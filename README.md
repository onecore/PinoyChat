PinoyChat: The Filipino Chatroom (Full Source Code)
---------------------
----------
![logo](https://lh4.ggpht.com/NSroSMlzfHe8U6oIAltCMD6mZXIdz8lcfkqRhIJQ_Wr_6CuWipcSGsqMF9LtC1M_Xg=w300-rw) 
----------

![License](https://img.shields.io/badge/License-MIT%20License-orange.svg?style=flat-square) 

![Powered](https://img.shields.io/badge/Fueled-CoreSEC%20Softwares-orange.svg?style=flat-square)

![Website](https://img.shields.io/badge/Head Developer-Mark%20A.%20Pequeras-orange.svg?style=flat-square)

![langs](https://img.shields.io/badge/C/C++-%20Python/Cython-orange.svg?style=flat-square)


----------
**Developers**
> - Mark Anthony Remetir Pequeras
> - Antionio Birol 
> - Maria Shiela Magistrado

**Special Thanks**
> - JetBrain's PyCharm Professional IDE (Offered Open Source License for this Project)

----------
**Compatibility**
> - Android (from 4.0 and Above)
> - iOS (iPhone, iPad, iPod)
> - Desktops (Linux, Mac and Windows)

----------
**License**
>MIT License 


**Latest Version (Master)**
> - 2.0.1

|   Version      |   Release Date   |
| -------------  |   -------------  |
|   1.3.1        |    Oct 4, 2014   |
|   1.4.0        |    Oct 9, 2014   |
|   1.4.1        |    Oct 13, 2014  |
|   1.5.0        |    Oct 21, 2014  |
|   1.5.1        |    Oct 21, 2014  |
|   1.5.2        |    Nov 1, 2014   |
|   1.5.3        |    Nov 11, 2014  |
|   1.5.4        |    Nov 12, 2014  |
|   1.5.5        |    Nov 19, 2014  |
|   2.0.0        |    Jan 27, 2015  |
|   2.0.1        |    Feb 08, 2015  |
|   2.0.3        |    March 19, 2016  |

----------
**Upcoming Version:**
> - 2.0.2 (Under Development)