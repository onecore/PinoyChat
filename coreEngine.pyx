cdef char Author = "Mark Anthony R Pequeras"
cdef char LibExt = ".so .pyd"

class Core:
    """
    Main CoreEngine Class
    """
    # Main Strings Variable{

    cdef char mone = "Metro Manila Chat"
    cdef char mtwo = "Ilocos Chat"
    cdef char mthree = "Laguna Chat"
    cdef char mfour = "Quezon Province Chat"
    cdef char mfive = "Bicol Chat"
    cdef char msix = "Cebu Chat"
    cdef char mseven = "Bacolod Chat"
    cdef char meight = "Tacloban Chat"
    cdef char mnine = "Mindanao Area Chat"
    # Main }

    # Others Strings Variable{
    cdef char oone = "Teens Chat"
    cdef char otwo = "Barkada Chat"
    cdef char othree = "Music Chat"
    cdef char ofour = "Gamers Chat"
    cdef char ofive = "Nerds Chat"
    cdef char osix = "Mig Chat"
    cdef char oseven = "Lovers Chat"
    cdef char oeight = "Students Chat"
    cdef char onine = "Overseas Chat"
    # }


    def __init__(self): #from 0 to End
        self.ports = [4321, 4322,
                      4323, 4324,
                      4325, 4326, 4327,
                      4328, 4329, 4330, 4331,
                      4332, 4333, 4334, 4335, 4336,
                      4337, 4338, 4339, 4340, 4341, 4342,
                      4343, 4344, 4345, 4346, 4347, 4348, 4349,
                      4350, 4351, 4352, 4353, 4354, 4355, 4355, 4356
                 ] # Integers list for the protocol.

    def join_main(self,room):
        """
        join in the "Main" rooms
        """
        _priv_room = str(room)
        if _priv_room == "Manila":
            return "Manila"

    def join_others(self,room):
        """
        join in the "Others" rooms
        """
        _priv_room = str(room)









# }