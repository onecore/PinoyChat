__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__version__ = 1.0
__distributor__ = ['CoreSEC Softwares, CA']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'


# { Milestones
# } Milestones


# { Imports
# } Imports


# { Souce
# } Source  