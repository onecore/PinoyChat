# -*- coding: utf-8 -*-

import version as Version  # Version contains some strings resource
from kivy.core.window import Window

__marp__ = """

  sSSs    sSSs_sSSs     .S_sSSs      sSSs    sSSs    sSSs    sSSs
 d%%SP   d%%SP~YS%%b   .SS~YS%%b    d%%SP   d%%SP   d%%SP   d%%SPs
d%S'    d%S'     `S%b  S%S   `S%b  d%S'    d%S'    d%S'    d%S'
S%S     S%S       S%S  S%S    S%S  S%S     S%|     S%S     S%S
S&S     S&S       S&S  S%S    d*S  S&S     S&S     S&S     S&S
S&S     S&S       S&S  S&S   .S*S  S&S_Ss  Y&Ss    S&S_Ss  S&S
S&S     S&S       S&S  S&S_sdSSS   S&S~SP  `S&&S   S&S~SP  S&S
S&S     S&S       S&S  S&S~YSY%b   S&S       `S*S  S&S     S&S
S*b     S*b       d*S  S*S   `S%b  S*b        l*S  S*b     S*b
S*S.    S*S.     .S*S  S*S    S%S  S*S.      .S*P  S*S.    S*S.
 SSSbs   SSSbs_sdSSS   S*S    S&S   SSSbs  sSS*S    SSSbs   SSSbs
  YSSP    YSSP~YSSY    S*S    SSS    YSSP  YSS'      YSSP    YSSP
                       SP
                       Y       (C) 2014, CORESEC SOFTWARE, Canada
                               (C) 2014, Mark Anthony R. Pequeras

"""

DEVELOPMENT_MODE = True  # 1/0-True/False

DEBUG = False # Force to Output everything. (False on Release)

__author__ = 'Mark Anthony Remetir Pequeras'

__developers__ = [__author__,
                  'Antonio Birol',
                  'Maria Shiela Magistrado',
                  'Arel Jao Guina',
                  'jay Paterno']

__build__ = 'Mobile'

__language__ = ['Python', 'C', 'Cython', 'Kivy']

__version__ = Version.Version    # Link to every part of versioning inside the App.

__branch__ = "develop"

__release__ = "03/19/2016" # Version update Date

__publisher__ = ['CoreSEC Softwares, CA']

__license__ = Version.License

__website__ = 'http://pinoychat.coresec.ca'

__compat__ = ['iOS','iPad','Android >= 4',
              'Android Tablet 4.x+','RaspberryPi',
              'MacOSX','Linux','Windows +XP']

__dependencies__ = ['Python 2.7','Kivy 1.8.0','Twisted/ZOPE','coreEngine','docutils', 'plyer']

__technologies__ = ['Kivy Language','C Language','Cython Language','Python Language']

__social__ = {
            "facebook":"http://www.fb.com/pinoychatapp",
            "twitter":"http://www.twitter.com/mpequeras",
            "website":"http://pinoychat.coresec.ca",
            }

__con__ = '0x'
__updates__ = Version.WhatsNew

from Engine import Functions
from kivy.support import install_twisted_reactor
install_twisted_reactor()
from twisted.internet import reactor, protocol
from twisted.protocols.basic import LineOnlyReceiver
from kivy.core.audio import SoundLoader
from kivy.uix.button import Button
from kivy.garden.navigationdrawer import NavigationDrawer
from kivy.cache import Cache
from kivy.base import EventLoop
from kivy.uix.accordion import Accordion,AccordionItem
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.properties import ObjectProperty, StringProperty
from kivy.parser import parse_color
from kivy.uix.switch import Switch
from kivy.uix.label import Label
from kivy.uix.rst import RstDocument
from kivy.app import App
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.modalview import ModalView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.tabbedpanel import TabbedPanel,TabbedPanelHeader
from kivy.uix.popup import Popup
from kivy.uix.scatter import Scatter
from kivy.clock import Clock
from kivy.uix.spinner import Spinner
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder
from kivy.loader import Loader
from kivy.uix.image import Image
from kivy.metrics import dp,sp, pt
from Engine import Truncate

import binascii
import ConfigParser
import urllib
import coreEngine
#import ParseEngine < Use this if you are in Mac, speeds up the app up to 10x
import webbrowser


global rootEngine # I am Global <3
global PMFEEDS
global showLoading
global NewsLoaded
global ShowAds
global ShowAds_Pattern

ShowAds = 1
ShowAds_Pattern = 10
NewsLoaded = 0
showLoading = 1


ConfigParse = ConfigParser.RawConfigParser()
if DEVELOPMENT_MODE:
    ConfigParse.read("/Users/Markie/Desktop/Development/PinoyChat/Data/cParser.cfg")

else:
    ConfigParse.read("Data/cParser.cfg")


def _SaveMe():
    """
    Helper function for saving config values
    """

    with open("/Users/Markie/Desktop/Development/PinoyChat/Data/cParser.cfg" if DEVELOPMENT_MODE else "Data/cParser.cfg", "wb") as config_file:
        ConfigParse.write(config_file)

PinoyChat_Inc = lambda X: X + 0.1                           # INC by 0.1
PinoyChat_Dec = lambda Y: Y - 0.1                           # DEC by 0.1
PinoyChat_SOGet = lambda MA,M: ConfigParse.get(MA,M)        # Section and Option Required
PinoyChat_SOVSet = lambda MA,L,M: ConfigParse.set(MA,L,M)   # Section, Option and Value Required
PinoyChat_Save = lambda MM: _SaveMe() if MM else MM         # Option Saver in Lambda
PinoyChat_XY = lambda X,Y: {"center_x":X,"center_y":Y}      # Axis of Y and X Setter
cut_max = PinoyChat_SOGet("PinoyChatCon","cut_level") # Bold Config

Loader.loading_image = "Data/Resource/pc.png"  # custom loader image

Builder.load_string('''
<ScrollableLabel>:

    Label:
        id: PCHATFEEDENGINE
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text


<PM>:
    Label:
        id: PMOBJ
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
        markup: True
        font_size: root.font_size

<PCMIO>:
    Label:
        id: PCIO
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
        markup: True
        font_size: root.font_size

<AGREEMENT>:
    Label:
        id: PMOBJ
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
        markup: True
        font_size: root.font_size

<CMDHelp>:
    Label:
        id: CMDHelps
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
        markup: True
        font_size: root.font_size

<NEWS>: 
    Label:
        id: NEWSOBJ
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
        markup: True
        font_size: root.font_size

<ScrollableAM>:
    Label:
        id: AM
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text

''')



# because for ads.

if DEVELOPMENT_MODE == False:
    from kivy.utils import platform
    from jnius import autoclass

    if platform=="android":
        PythonActivity=autoclass("org.renpy.android.PythonActivity")
        AdBuddiz=autoclass("com.purplebrain.adbuddiz.sdk.AdBuddiz")
# because for ads.

rootEngine = ObjectProperty() # Object <<
rst = ObjectProperty()
oView = ObjectProperty()

def PinoyChatDebug(out,inspector=1,error=0):
    """
    I/O Function for Debugging (with userdefined messages)
    """

    import inspect
    InFunction = inspect.stack()[1][3]
    print "PINOYCHAT DEBUG MESSAGE: "+ str(out) + " in Function "+str(InFunction)+"()"


class PinoyChatError(Exception):
    pass

class EchoClient(LineOnlyReceiver):
    def connectionMade(self):
        self.factory.app.on_connection(self.transport, self.sendLine)
        if DEBUG:
            PinoyChatDebug("Connection Accepted by Server.")

    def lineReceived(self, msg):
        response = self.factory.app.handle_message(msg)
        if response:
            self.sendLine(response)



class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def __init__(self, app):
        self.app = app

    def clientConnectionLost(self, conn, reason):
        #self.app.print_message("connection lost")
        self.app.connection = False
        self.app.send = None

    def clientConnectionFailed(self, conn, reason):
        self.app.print_message("Connection failed...")


class ScrollableLabel(ScrollView):
    """
    Scrollview with Label inside (as the self.cs_msgFeeds connected to)
    """
    text = StringProperty('')   # method from Label()

class ScrollableAM(ScrollView):
    text = StringProperty('')

class AGREEMENT(ScrollView):
    text = StringProperty('')   # method from Label()
    font_size = StringProperty('23sp')

class CMDHelp(ScrollView):
    text = StringProperty('')   # method from Label()
    font_size = StringProperty('23sp')

class PM(ScrollView):
    text = StringProperty('')
    font_size = StringProperty('23sp')

class PCMIO(ScrollView):
    innerId = "pcio"
    text = StringProperty('')
    font_size = StringProperty('18sp')

class NEWS(ScrollView):
    text = StringProperty('')
    font_size = StringProperty('23sp')

class pinoyChatClientApp(App):
    """
    Main class of the PinoyChat
    """
    global curr_h
    global username
    global username_new
    global send_username_bool
    global connected
    global show_join
    global change_nick
    global anti_flood
    global flood_warning
    global sounds # bool for sounds
    global users_init # used on Enter
    global oView
    global print_message_init
    global sound_path
    global initsend
    global is_Bold,is_Italic,is_Normal # for Appearance Options
    global DataServer   # Server with the default "8080" as port
    global Users_Storage # Users Variable hack
    Users_Storage = {}

    global BoolUserOption   # Connected to UserOption() fN
    BoolUserOption = "Closed"

    global AdminBool    # is Admin 1/0
    AdminBool = False

    global cacheEngine  # Variable for Caching message (Developed to avoid BLACK bug)
    cacheEngine = ""

    global guest
    guest = 0

    global ModBool
    ModBool = False     # is Moderator 1/0

    global DisableMessageBool   # Disable send_message() fN
    DisableMessageBool = False

    global Message
    global MessageStorage
    MessageStorage = {}

    # Chat Appearance {
    global Feed_Bold, Feed_Italic, Feed_Normal
    global Feed_UColor, Feed_MColor, Feed_Font, Feed_Size
    global Feed_Image, Feed_BackgroundOrColor, Feed_Color
    global banned
    global Room
    global CurrentChat
    global Loggedin
    Room = None
    Loggedin = False
    banned = 0
    CurrentChat = ""

    bold_Disk = PinoyChat_SOGet("PinoyChatCon","message_bold") # Bold Config
    italicDisk = PinoyChat_SOGet("PinoyChatCon","message_italic")   # Italic Config
    userColorDisk = PinoyChat_SOGet("PinoyChatCon","username_color")   # Username Color
    msgColorDisk = PinoyChat_SOGet("PinoyChatCon","message_color")  # Message Color
    soundDisk = PinoyChat_SOGet("PinoyChatCon","sound")     # Sound Config
    sizeDisk = PinoyChat_SOGet("PinoyChatCon","message_size")   # Message Font Size
    fontDisk = PinoyChat_SOGet("PinoyChatCon","message_font")   # Message Font Name

    colorOrImageDisk = PinoyChat_SOGet("PinoyChatCon","backgroundcolor_or")   # Is Color or Image
    imageDisk = PinoyChat_SOGet("PinoyChatCon","background_image")  # Background Image
    colorDisk = PinoyChat_SOGet("PinoyChatCon","background_color")  # Background Color


    Feed_Bold,Feed_Italic,Feed_Normal = int(bold_Disk),int(italicDisk),True
    # "#003366","#2E2E2E","" U,M,F <~~ DEFAULTS U and M Colours
    Feed_UColor, Feed_MColor, Feed_Font, Feed_Size = str(userColorDisk),str(msgColorDisk),fontDisk, sizeDisk
    Feed_BackgroundOrColor = colorOrImageDisk # contains "color" or "image" Str
    Feed_Image = imageDisk
    Feed_Color = colorDisk

    sounds = int(soundDisk)

    # }

    DataServer = str(Functions.Core.PChatParser(True))
    curr_h = str(Functions.Core.PChatParserNext(True))


    is_Bold,is_Italic,is_Normal = 0,0,1 # Appearance Settings
    initsend = 0    # 1/0 runs once, on send_message() fN
    flood_warning = 3
    anti_flood = 3 # allow 3 multi-lined messages.
    flood_punish = 30 # seconds
    change_nick = 2
    show_join = 0
    sound_path = "Data/Tone/pinoychat.wav"
    scr_list = ['Exit'] #screens list
    scr_current = 'None' #current screen
    connection = None # none type
    users = None      # cache variable for saving users list
    cache_msg = None  # variable for caching message
    csOut_Temp = """
    f0809bb15089b523ae49fdeceaae4ceb
    """
    csOut = ""        # static variable as logger

    username = ""

    def __init__(self, **kwargs):
        """PinoyChat Initialization / Some protos"""
        super(pinoyChatClientApp,self).__init__(**kwargs)
        from kivy.uix.behaviors import ButtonBehavior
        from kivy.uix.image import AsyncImage


        class AvatarView(ButtonBehavior,AsyncImage):
            """Avatar Implementation"""
            pass

        self.SideBarLay = FloatLayout()
        self.SideAvatar = AvatarView()
        self.SideCameraICO = Button(text=u"\uf030")
        self.SideCameraLICO = Button(text=u"\uf1c5")
        self.SideOffICO = Button(text=u"\uf08b")
        self.SideOff = Button(text="Sign Out")
        self.SideMessage = Button(text="Messages")
        self.SideEditAv = Button(text="View Profile")
        self.SideEdit = Button(text="Edit Account")
        self.SideName = Button(text="Hi Guest!")

        #// Not Logged Widgets
        self.Side_infon = Label(text="PinoyChat News")
        self.Side_infona = Label(text=u"\uf12f")
        self.Side_Feeds = RstDocument()
        self.Side_website = Button(text="PinoyChat Website")
        self.Side_Like = Button(text="Share Us!")
        self.Side_rate = Button(text="Rate PinoyChat!")
        self.Side_download = Button(text="Download Desktop")
        self.Side_info = Label(text="PinoyChat © CoreSEC Softwares, CA")
        self.Side_sec = Image(source="Data/profile.png")

    if DEVELOPMENT_MODE: # This runs when DevelMode is True / 1
        from Engine import Functions
        Get = Functions.Core.PChatMarkPequeras(True)
        #username = Get #""

    username_new = ""

    soundswav = ""

    csideversion = ""

    send_username_bool = 0

    connected = 0

    users_init = ""

    print_message_init = ""

    Message = None

    def ConfigEngine(self):
        Parser = ConfigParser.RawConfigParser()

    def ProgressBarObj(self):
        pass

    def on_start(self):
        """
        This Function runs every App Start (Initializer of PinoyChat)
        """
        EventLoop.window.bind(on_keyboard=self.hook_keyboard)
        if DEVELOPMENT_MODE == False:
            AdBuddiz.setPublisherKey("bc3c7f60-425a-436e-a1db-57ed488b8ffe");
            AdBuddiz.cacheAds(PythonActivity.mActivity); # cached Ads.

        def _IfNew():
            View = ModalView(auto_dismiss=False)
            View.size_hint = .8,.7
            View.title_color = parse_color("#F0E68C")
            Agreement_And_License = """
     [b][color=#696969]PINOYCHAT RULES[/color][/b]
     You are Allowed:
     - Chat Freely!
     - Privately Message a User
     - Hire or Advertise
     - Share Location
     - Share Contact
     - Use of Multiple Usernames
     - Linking a URL

     Strictly Prohibited:
     - Spamming in Chat
     - Bashing User(s)
     - No Personal Attack or Harassment
     - No Solicitation
     - Do not Monopolize Conversation
     - Linking URL (Except Social Links)

     [b][color=#696969]License[/color][/b][size=10]
        The MIT License (MIT) | Open-Source
        Copyright (c) 2014 CoreSEC Softwares, CA
        Copyright (c) 2014 Mark Anthony Pequeras
        Copyright (c) 2014 Antonio Birol

        Permission is hereby granted, free of charge,
        to any person obtaining a copy of this software
        and associated documentation files (the "Software"),
        to deal in the Software without restriction,
        including without limitation the rights
        to use, copy, modify, merge, publish, distribute,
        sublicense, and/or sell copies of the Software,
        and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice
        shall be included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS",
        WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
        INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
        FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
        TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
        THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.[/size]


     [b][color=#696969]Publisher[/color][/b]
     - CoreSEC Softwares, CA

     [b][color=#696969]Contact[/color][/b]
     Website: http://www.pinoychat.coresec.ca
     Contact Email: publisher@coresec.ca
            """
            _howTo = """

    [b][color=#696969]About this Version[/color][/b]

     [b][color=#696969][size=13]Version[/size][/color][/b]
     [size=15]{v}[/size]

     [b][color=#696969][size=13]Release Date[/size][/color][/b]
     [size=15]{rd}[/size]

     [b][color=#696969][size=13]Updates[/size][/color][/b]
     [size=15]{u}[/size]








     [/size]

            """.format(v=__version__,
                       rd=__release__,
                       u=__updates__)

            FLOUT = FloatLayout()

            BG = Button(background_normal="Data/introData/bgo.png")
            BG.pos_hint = {"center_x":.5,"center_y":.5}

            AgreeButton = Button()
            AgreeButton.text = "Next"
            AgreeButton.pos_hint = PinoyChat_XY(.5,.05)
            AgreeButton.size_hint = 1,.1

            AgreementLBL = AGREEMENT()
            AgreementLBL.font_size = "14sp"


            AgreementLBL.text = Agreement_And_License
            AgreementLBL.size_hint = 1 , .87
            AgreementLBL.pos_hint = {"center_x":.5,"center_y":.53}

            def AgreeCallBack_Close():
                "Close the Modal and Save the Option new = 0"
                PinoyChat_SOVSet("PinoyChatCon","new","0")
                PinoyChat_Save(True)
                View.dismiss()

            FinalCloseButton = Button()
            FinalCloseButton.on_press = AgreeCallBack_Close
            FinalCloseButton.pos_hint = PinoyChat_XY(.5,.05)
            FinalCloseButton.size_hint = 1 , .1
            FinalCloseButton.text = "Close Now"


            def AgreeCallBack():
                "Next Button and Close Now will be next"
                if "License" in AgreementLBL.text: # Final Agreement
                    AgreementLBL.text = _howTo
                    FLOUT.add_widget(FinalCloseButton)



            AgreeButton.on_press = AgreeCallBack

            FLOUT.add_widget(BG)
            FLOUT.add_widget(AgreementLBL)
            FLOUT.add_widget(AgreeButton)
            View.add_widget(FLOUT)
            View.open()

        if True: # Initializer for everthing on Start up (:
            if PinoyChat_SOGet("PinoyChatCon","new") == "1":
                _IfNew()    # Run notice if user is new.

            else:
                pass


        # Start loading background Color/Image / Private Var Functions below
        IsColorOrImage = PinoyChat_SOGet("PinoyChatCon","backgroundcolor_or")
        _Disk_Color = PinoyChat_SOGet("PinoyChatCon","background_color")
        _Disk_Image = PinoyChat_SOGet("PinoyChatCon","background_image")

        if IsColorOrImage == "color":
            self.cs_BG.background_normal = ""
            self.cs_BG.background_down = self.cs_BG.background_normal
            self.cs_BG.background_color = parse_color(_Disk_Color)

        elif IsColorOrImage == "image":
            self.cs_BG.background_normal = _Disk_Image
            self.cs_BG.background_down = _Disk_Image

        else:
            pass # This is Default and will be passed

    def show_pchat_ad(self):
        """
        Show Ads Function, just call me.
        """
        AdBuddiz.showAd(PythonActivity.mActivity) #lets show the ad ;)

    def hook_keyboard(self, window, key, *largs):
        """
        Hooke any Keyboard keys and ask for Callbacks
        """
        if key in [27,1001]:
           # do what you want, return True for stopping the propagation
           return True

    def on_pause(self):
        """
        When App Minimized this Function called
        """
        return True

    def on_resume(self):
        """
        When App resume from on_pause() fN
        """
        return True

    def open_settings(self):
        """
        KV Settings
        """
        pass # disable Menu button

    def send_username(self):
        """
        Send Username packets to the coreEngine Server
        """
        global username
        global send_username_bool

        if len(str(username)) == 0:
            self.ask_for_username()


        else:
            if send_username_bool == 0:
                priv_packforserver = username
                #reactor.connectTCP(str(curr_h), 4321, EchoFactory(self))
                self.send(priv_packforserver) #send to server.
                send_username_bool = 1


    def get_users_list(self):
        """
        fN for Getting Users list from the Server
        """
        global curr_h
        try:
            if DEBUG:
                PinoyChatDebug(out="Sending Request Packet to Server")
            self.send("list")
        except:
            if DEBUG:
                PinoyChatDebug("Error Connecting to Server..")
            self.pinoyChat_exception(title="Connection Error",errmsg="Please Try again later.")


        #reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))

    def build(self): # Initialize build func()
        """
        Builder class of PinoyChat App
        """
        global username
        global rootEngine # < careful its globa :3


        self.title = "PinoyChat App {v} - CoreSEC Softwares, CA ".format(v=Version.Version) # window title for Desktops (mac,wind,linux)
        self.Sidebar = NavigationDrawer()


        rootEngine = self.screenManagerx() #self.pinoyChat()
        rootEngine.add_widget(self.scr_option()) # Option Screen
        rootEngine.add_widget(self.scr_chat())  #  # Chat Area Screen
        rootEngine.add_widget(self.ScreenLogin()) # Login Screen
        #rootEngine.add_widget(self.ScreenRegister())
        rootEngine.add_widget(self.scr_intro()) # Introduction Screen
        self.connect_to_server() #start connection to server. manila server (MAIN)
        self.Sidebar.add_widget(self.SidebarContents())
        self.Sidebar.add_widget(rootEngine)
        rootEngine.current = 'screenIntro' #'screenChat' # Current Screen
        #return rootEngine
        return self.Sidebar

    def BannedObj(self):
        pass


    def DefaultSettingsLoaderObj(self):
        """
        Default Settings will be Set and Save using this Function.
        """
        global Feed_Bold, Feed_Italic, Feed_Normal
        global Feed_UColor, Feed_MColor, Feed_Font, Feed_Size
        global Feed_Image, Feed_BackgroundOrColor, Feed_Color
        global username
        #username = ""
        PinoyChat_SOVSet("PinoyChatCon","message_bold","0")
        Feed_Bold = 0
        PinoyChat_SOVSet("PinoyChatCon","message_italic","0")
        Feed_Italic = 0
        PinoyChat_SOVSet("PinoyChatCon","message_size","23")
        Feed_Size = "23"
        PinoyChat_SOVSet("PinoyChatCon","username_color","#003366")
        Feed_UColor = "#003366"
        PinoyChat_SOVSet("PinoyChatCon","message_color","#2E2E2E")
        Feed_MColor = "#2E2E2E"
        PinoyChat_SOVSet("PinoyChatCon","message_font","NONE")
        Feed_Font = ""
        PinoyChat_SOVSet("PinoyChatCon","background_image","Data/Colours/26.png")
        Feed_Image = "Data/Colours/26.png"
        PinoyChat_SOVSet("PinoyChatCon","backgroundcolor_or","image")
        Feed_BackgroundOrColor = "image"
        # Now Save
        PinoyChat_Save(True) # This will write everything on Config File
        self.Warning("Default Settings Loaded!")

    # Just call me when you need string Values ;)
    class AccountValuesLoader(object):

        def __init__(self,username):
            """
            Contains User Information and a PM tunnel.
            """
            from Engine import User
            self.username = username

            if len(self.username) < 3:
                return None

            Struct = User.Core().GetUsersInformation(u=self.username) # Packed String-Tuples

            EvalStruct = eval(Struct) # Evaluated String into Tuples.

            # Gender, Rank, Age, Aboutme, Location (Viewables.)
            self.Username = str(EvalStruct[0])   # no
            self.Password = str(EvalStruct[1])   # no
            self.Avatar = str(EvalStruct[2])     # yes
            self.Gender = str(EvalStruct[3])     # yes
            self.Rank = str(EvalStruct[4])       # yes
            self.Age = str(EvalStruct[5])        # yes
            self.AboutMe = str(EvalStruct[6])    # yes
            self.Location = str(EvalStruct[7])   # yes
            self.Status = str(EvalStruct[8])     # On/Off
            self.Admin = str(EvalStruct[9])      # no
            self.Moderator = str(EvalStruct[10]) # no


    def EditAccountObj(self):
        """
        Modal for Editing Account Values.
        """
        from kivy.uix.image import AsyncImage
        global username
        global NewAvatar
        NewAvatar = ""

        #Gender[X], Rank[X], Age[X], Aboutme[], Location[X]
        global _Gender, _Rank, _Age, _Aboutme, _Location
        _Gender, _Rank, _Age, _Aboutme, _Location = None,None,None,None,None
        M = ModalView()
        MainAcc = Accordion()
        RemoteValues = pinoyChatClientApp().AccountValuesLoader(username=username)

        def SingleExecute(usrx,querx,valx):
            """
            Remote Executor
            """
            from Engine.User import Core
            SEngine = Core()
            return SEngine.UpdateUser(usrx,querx,valx)

        n = "Data/Colours/6.jpg"
        nd = "Data/Colours/22.jpg"
        nn = "Data/Colours/25.jpg"

        M.background = n

        First = AccordionItem()
        First.title = "Account"
        First.orientation = "horizontal"
        First.background_normal = nd
        First.background_selected = nn


        Second = AccordionItem()
        Second.title = "Avatar Settings"
        Second.background_normal = nd
        Second.background_selected = nn

        Third = AccordionItem()
        Third.title = "My Information"
        Third.background_normal = nd
        Third.background_selected = nn

        OneL = FloatLayout()    # Account
        TwoL = FloatLayout()    # Avatar
        ThrL = FloatLayout()    # Information

        # ACCOUNT SETTINGS
        uinfo = Label(text="Registered as [b]"+username+"[/b]")
        #uinfo.font_name = "Data/fonts/ambrosia.ttf"
        uinfo.pos_hint = PinoyChat_XY(.5,.75)
        uinfo.markup = True
        uinfo.font_size = "23sp"

        cpass = Label(text="Change Password")
        cpass.pos_hint = PinoyChat_XY(.5,.47)
        cpass.font_size = "23sp"

        seper = Button()
        seper.background_normal = "Data/Account/trans.png"
        seper.background_down = seper.background_normal
        seper.size_hint = 1,.2
        seper.pos_hint = PinoyChat_XY(.5,.5)

        CurPass = TextInput()       # Original Password
        CurPass.hint_text = "Your Current Password"
        CurPass.size_hint = .8,.1
        CurPass.pos_hint = PinoyChat_XY(.5,.35)
        CurPass.password = True
        CurPass.multiline = False

        NewPass = TextInput()       # Original Password
        NewPass.hint_text = "New Password"
        NewPass.size_hint = .8,.1
        NewPass.multiline = False
        NewPass.pos_hint = PinoyChat_XY(.5,.23)

        def ChangePassword():
            Rv = RemoteValues.Password
            if str(CurPass.text) == str(Rv):
                if str(NewPass.text) == str(Rv):
                    self.Warning("Nothing to Update...")

                else:
                    if str(NewPass.text) != str(Rv):
                        if len(NewPass.text) <= 4 or len(NewPass.text) >= 30:
                            self.Warning("Please check your new Password.")
                            return False

                        try:
                            SingleExecute(username,"Password",NewPass.text)
                        finally:
                            self.Warning("Password changed successfully!")
                            M.dismiss()

            else:
                self.Warning("Password doesn't match!")
                CurPass.text = ""
        _ChangeNow = Button(text="Change now")       # Original Password
        _ChangeNow.size_hint = 1,.12
        _ChangeNow.pos_hint = PinoyChat_XY(.5,.06)
        _ChangeNow.background_normal = "Data/Colours/1.jpg"
        _ChangeNow.background_down = _ChangeNow.background_normal
        _ChangeNow.on_press = ChangePassword

        OneL.add_widget(uinfo)
        OneL.add_widget(cpass)
        OneL.add_widget(seper)
        OneL.add_widget(CurPass)
        OneL.add_widget(NewPass)
        OneL.add_widget(_ChangeNow)
        # ACCOUNT SETTINGS

        # AVATAR SETTINGS
        AS_RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
        _UsrImgObj = str(AS_RunGetter.Avatar)


        SAvatarView = AsyncImage()
        SAvatarView.source = _UsrImgObj #Avatar

        try:
            SAvatarView.reload()
        except:
            pass

        SAvatarView.pos_hint = PinoyChat_XY(.5,.7)
        SAvatarView.size_hint = .5,.5
        SAvatarView.allow_stretch = True
        SAvatarView.keep_ratio = False

        def AvaReloader():
            AS_RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
            _UsrImgObj = str(AS_RunGetter.Avatar)

            SAvatarView.source = _UsrImgObj
            self.SideAvatar.source = _UsrImgObj

        SaveAvatar = Button(text="Reload All")
        SaveAvatar.background_normal = "Data/Colours/17.jpg"
        SaveAvatar.background_down = SaveAvatar.background_normal
        SaveAvatar.size_hint = 1,.2
        SaveAvatar.pos_hint = PinoyChat_XY(.5,.1)
        SaveAvatar.on_press = AvaReloader

        def UpdateEngine(usrx,querx,valx):
            """
            Account DB Update dupe, remotely.
            """

            from Engine.User import Core
            SEngine = Core()
            return SEngine.UpdateUser(usrx,querx,valx)


        def PChatAvatarPchat():
            """
            Function for showing default avatars in Tiled
            """
            global NewAvatar

            l = GridLayout()
            l.cols = 3
            content = 12
            b = 0
            g = 0

            def ShowSelected(x): # function "x"
                global NewAvatar
                try:
                    selected = x.background_normal
                    SAvatarView.source = selected
                    NewAvatar = str(selected)
                    UpdateEngine(username,"Avatar",selected)
                    self.Warning("Saved, Please Update your Avatar!")
                    m.dismiss()

                except:
                    self.Warning("CritException: Critical looping in users avatar memleak")

            # Loop for every avatar images
            while b != 6 and g != 6:
                #Loops into range of 6 for b and g (gender)
                b += 1
                g += 1

                #Male Avatars
                l.add_widget(Button(on_press=ShowSelected,background_normal="Data/Account/Avatars/b"+str(b)+".jpg",background_down="Data/Account/Select.png"))

                #Female Avatars
                l.add_widget(Button(on_press=ShowSelected,background_normal="Data/Account/Avatars/g"+str(g)+".jpg",background_down="Data/Account/Select.png"))

                if b == 6 and g == 6:
                    break   #Let's exit before your device gets burned XD

            m = ModalView()
            m.size_hint = 1,.8
            m.add_widget(l)
            m.open()


        def UploadAndUpdate(u,p,binaryp):
            """
            Function for transfering custom avatars to coreEngine (PinoyChat Engine)
            """
            #from Engine.UEngine import PCWebLoader
            pcurl = "http://pctunnel.coresec.ca:5000/upload"
            filename = binaryp
            from Engine.UEngine import MultipartPostHandler
            import urllib2, cookielib

            cookies = cookielib.CookieJar()
            opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies),
                                            MultipartPostHandler)
            if u and p:

                params = { "pchatid" : str(u), "pchatpwd" : str(p),
                           "upload" : open(binaryp, "rb") }
                opener.open(pcurl, params)


        def PChatAvatarLocal():
            from kivy.utils import platform
            from os import path

            #GetType anon function for retriving File Extensions
            GetType = lambda spath: path.splitext(str(spath))[1][1:] if spath else None


            BG = Button()
            BG.background_normal = "Data/Colours/22.jpg"
            BG.background_down = BG.background_normal
            BG.size_hint = 1,1
            BG.pos_hint = PinoyChat_XY(.5,.5)

            OS = ""

            if platform == "android":
                OS = platform

            else:
                OS = "desk-"+str(platform)

            l = FloatLayout()

            LAView = AsyncImage()
            LAView.source = _UsrImgObj #Avatar

            try:
                LAView.reload()
            except:
                pass

            LAView.pos_hint = PinoyChat_XY(.5,.6)
            LAView.size_hint = .6,.6
            LAView.allow_stretch = True
            LAView.keep_ratio = False

            def TakePic(): #// In Option
                """
                Functions for capturing an image, uses Plyer as image object loader
                """
                from plyer import camera
                from Engine.User import Core, MultiPartForm

                if OS == "android": # Only designed to work for android

                    def finished(e): #receive e as the image location
                        "This function runs when CaptureNow() function finished the proc."

                        path = e #update the label to the image location

                        try:

                            try:
                                UploadAndUpdate(u=username,p=AS_RunGetter.Password,binaryp=path)
                                LAView.source = str(e)
                                LAView.reload()

                                GetRem = pinoyChatClientApp().AccountValuesLoader(username=username)
                                RemV = GetRem.Avatar

                            finally:
                                m.dismiss()
                                self.Warning("Avatar Updated!")

                                Cache.remove('kv.image')
                                Cache.remove('kv.texture')
                                self.cs_Logo.source = str(RemV)
                                SAvatarView.source = str(RemV)
                                self.SideAvatar.source = str(RemV)


                        except:
                            self.Warning("Ooops, Unable to Update avatar!")


                    def  CaptureNow():
                        "Only works with android"
                        import random
                        GenInt = lambda ints: random.randrange(int(ints),543)
                        FinalName = "/storage/sdcard0/pinoychat-"+str(GenInt(99))+str(GenInt(200))+str(GenInt(143))+".jpg" # file name gen / good to prevent dupes
                        camera.take_picture(str(FinalName), finished) #Take a picture and save at this location. After will call done() callback

                    CaptureNow()    # Auto start the Camera.

                else:
                    self.Warning("This option only supports Mobile")



            def LoadPic():  # Works for both mobile and desktop platforms
                """
                Image Obj loader
                """
                from kivy.uix.filechooser import FileChooserIconView, FileChooserListView

                BG = Button()
                BG.background_normal = "Data/Colours/17.jpg"
                BG.background_down = BG.background_normal
                BG.size_hint = 1,1
                BG.pos_hint = PinoyChat_XY(.5,.5)

                ilabel = Label(text="Please provide an awesome picture of yours!")
                ilabel.pos_hint = PinoyChat_XY(.5,.95)

                m = ModalView()
                m.size_hint = 1,.8

                l = FloatLayout()

                iView = FileChooserListView()

                if OS == "android":
                    iView.path = "/storage/sdcard0/"

                else:
                    pass

                iView.pos_hint = PinoyChat_XY(.5,.5)
                iView.size_hint = .98,.8

                def loader():
                    """
                    File loader function (From SDA)
                    """
                    from Engine.User import Core
                    from kivy.logger import Logger
                    import imghdr

                    cpath, cselection = iView.path, iView.selection

                    try:

                        files = cselection[0] # first.
                        allowedExt = ["jpg","jpeg","png"]   # list of available img files.

                        if str(GetType(files)) in allowedExt:
                            print GetType(files)

                            try:
                                LAView.source = files
                                UploadAndUpdate(u=username,p=AS_RunGetter.Password,binaryp=files)
                                GetRem1 = pinoyChatClientApp().AccountValuesLoader(username=username)
                                RemV1 = GetRem1.Avatar
                            finally:
                                m.dismiss()
                                Cache.remove('kv.image')
                                Cache.remove('kv.texture')
                                self.cs_Logo.source = str(RemV1)
                                self.Warning("Avatar Updated!")
                                AvaReloader()

                        else:
                            raise # go to Exception

                    except:
                        self.Warning("Please provide an Image file.")



                btn = Button(text='Load and Update')
                btn.background_normal = "Data/Colours/7.jpg"
                btn.background_down = btn.background_normal
                btn.on_press = loader
                btn.size_hint = 1,.1
                btn.pos_hint = PinoyChat_XY(.5,.05)

                l.add_widget(BG)
                l.add_widget(iView)
                l.add_widget(ilabel)
                l.add_widget(btn)

                m.add_widget(l)
                m.open()

            take = Button(text="Take a Picture (Smile)")
            take.background_normal = "Data/Colours/16.jpg"
            take.background_down = take.background_normal
            take.size_hint = 1,.13
            take.pos_hint = PinoyChat_XY(.5,.2)
            take.on_press = TakePic

            load = Button(text="Load Image ...")
            load.background_normal = "Data/Colours/16.jpg"
            load.background_down = load.background_normal
            load.size_hint = 1,.13
            load.pos_hint = PinoyChat_XY(.5,.06)
            load.on_press = LoadPic

            l.add_widget(BG)
            l.add_widget(LAView)
            l.add_widget(take)
            l.add_widget(load)

            m = ModalView()
            m.add_widget(l)
            m.size_hint = .8,.5
            m.open()


        def PChatAvatarWeb():

            fl = FloatLayout()

            BG = Button()
            BG.background_normal = "Data/Colours/22.jpg"
            BG.background_down = BG.background_normal
            BG.size_hint = 1,1
            BG.pos_hint = PinoyChat_XY(.5,.5)

            ViewObjLink = _UsrImgObj
            View = AsyncImage()
            View.source = ViewObjLink

            try:
                View.reload()
            except:
                pass

            View.pos_hint = PinoyChat_XY(.5,.7)
            View.size_hint = .7,.4
            View.allow_stretch = True
            View.keep_ratio = False

            info = Label(text="Please provide a URL with an image extension")
            info.pos_hint = PinoyChat_XY(.5,.9+.15)

            info2 = Label(text="Supported image files (JPEG,JPG and PNG)")
            info2.pos_hint = PinoyChat_XY(.5,.4)

            tInput = TextInput(hint_text="Image URL: http://ex.com/img.jpg")
            tInput.size_hint = .9,.1
            tInput.pos_hint = PinoyChat_XY(.5,.3)

            def ProcessRemoteImg():
                """
                Image processing function for getting
                """
                import requests
                global ViewObjLink

                ImageURL = ""

                if "http://" not in tInput.text:
                    ImageURL = "http://"+tInput.text
                    tInput.text = ImageURL

                else:
                    ImageURL = tInput.text

                def checkAvail():
                    global ViewObjLink
                    if len(ImageURL) <= 4:
                        self.Warning("Please check the Link!")

                    else:
                        try:
                            req = requests.get(str(ImageURL))
                            ViewObjLink = ImageURL

                            if req.status_code == 200:
                                header = req.headers['content-type']

                                if "image/" in header:  #Header if image

                                    if ".jpg" == tInput.text[-4:] or ".png" == tInput.text[-4:]:
                                        View.source = ViewObjLink

                                        try:
                                            UpdateEngine(username,"Avatar",ViewObjLink)
                                            m.dismiss()

                                        except:
                                            self.Warning("")

                                    else:
                                        self.Warning("Direct image required.")

                                else:
                                    self.Warning("URL not pointing into image")

                            else:
                                self.Warning("The Website isn't functional")
                        except:
                            self.Warning("Please check the Link!")
                checkAvail()

            IandSave = Button()
            IandSave.background_normal = "Data/Colours/7.jpg"
            IandSave.background_down = IandSave.background_normal
            IandSave.size_hint = 1,.2
            IandSave.pos_hint = PinoyChat_XY(.5,.1)
            IandSave.text = "Import and Save"
            IandSave.on_press = ProcessRemoteImg


            fl.add_widget(BG)
            fl.add_widget(View)
            fl.add_widget(info)
            fl.add_widget(info2)
            fl.add_widget(tInput)
            fl.add_widget(IandSave)

            m = ModalView()
            m.add_widget(fl)
            m.size_hint = .8,.8
            m.open()

        def ProcessAvatar(spinner,value):
            """
            Function for processing chosen action(value)
            """
            action = ['From Avatars', 'From Storage', 'Import from Web']

            if value == action[0]:
                # PChat Avatars
                PChatAvatarPchat()

            elif value == action[1]:
                # Local
                PChatAvatarLocal()

            elif value == action[2]:
                #Via Web
                PChatAvatarWeb()



        SpinAction = Spinner(
            text="Edit Avatar",
            values=('From Avatars', 'From Storage', 'Import from Web'),
            size_hint=(.5, None),
            size=(300, 44),
            pos_hint={'center_x': .5, 'center_y': .4})
        SpinAction.background_normal = "Data/Colours/8.jpg"
        SpinAction.background_down = SpinAction.background_normal
        SpinAction.bind(text=ProcessAvatar)

        TwoL.add_widget(SAvatarView)
        TwoL.add_widget(SpinAction)
        TwoL.add_widget(SaveAvatar)
        # AVATAR SETTINGS


        # INFO SETTINGS
        # Information below are the ones chosen to be editable.
        # Gender[X], Rank[X], Age[X], Aboutme[], Location[X] (Viewables.)

        BG2 = Button()
        BG2.background_normal = "Data/Colours/22.jpg"
        BG2.background_down = BG2.background_normal
        BG2.size_hint = 1,.7
        BG2.pos_hint = PinoyChat_XY(.5,.45)

        InfoTitle = Label(text="Rank: [b]"+RemoteValues.Rank+"[/b]")    # constant.
        InfoTitle.pos_hint = PinoyChat_XY(.5,.9)
        InfoTitle.markup = True

        AgeLabel = Label(text=username+"'s Age: [b]"+RemoteValues.Age+"[/b]")
        AgeLabel.pos_hint = PinoyChat_XY(.3,.7)
        AgeLabel.markup = True
        AgeLabel.size_hint_x = .8

        def AgeObj():
            """
            Edit Age Values
            """
            global _Age

            _m = ModalView(size_hint=(.4,.24))

            _l = FloatLayout()
            _l.orientation = "vertical"
            RMAge = RemoteValues.Age
            _Age = []
            AgeTotal = 0
            try:
                Int1 = int(RMAge[0])
                Int2 = int(RMAge[1])
                _Age = [Int1,Int2]
                AgeTotal = Int1 + Int2

            except:
                _Age = [0,0]

            _VAge1 = Spinner(
                    # default value shown
                    text=str(_Age[0]),
                    # available values
                    values=('1', '2','3','4','5','6','7','8','9','0'),
                    size_hint=(.4, .5),
                    size=(100, 44),
                    pos_hint={'center_x': .28, 'center_y': .6})

            _VAge2 = Spinner(
                    # default value shown
                    text=str(_Age[1]),
                    # available values
                    values=('1', '2','3','4','5','6','7','8','9','0'),
                    size_hint=(.4, .5),
                    size=(100, 44),
                    pos_hint={'center_x': .71, 'center_y': .6})

            def saveAge():
                global _Age
                _Age = str(_VAge1.text) + str(_VAge2.text)

                if str(_Age) != str(RMAge):
                    EditAge.text = "Edited"

                else:
                    EditAge.text = "Edit"

                try:
                    SingleExecute(username,"Age",_Age)

                finally:
                    _m.dismiss()

            _save = Button(text="Save Age")
            _save.background_normal = "Data/Colours/15.jpg"
            _save.background_down = _save.background_down
            _save.size_hint = .97,.3
            _save.pos_hint = PinoyChat_XY(.5,.1)
            _save.on_press = saveAge
            _ti = TextInput()
            _ti.text = RemoteValues.Age
            _ti.size_hint = 1,.12
            _ti.pos_hint = PinoyChat_XY(.5,.5)

            _l.add_widget(_VAge1)
            _l.add_widget(_VAge2)
            _l.add_widget(_save)

            _m.add_widget(_l)
            _m.open()


        EditAge = Button(text="Edit")
        EditAge.background_normal = "Data/Colours/15.jpg"
        EditAge.background_down = EditAge.background_normal
        EditAge.size_hint = .3,.1
        EditAge.pos_hint = PinoyChat_XY(.7,.7)
        EditAge.on_press = AgeObj

        GLabel = Label(text=" Gender: [b]"+RemoteValues.Gender+"[/b]")
        GLabel.pos_hint = PinoyChat_XY(.3,.58)
        GLabel.markup = True
        GLabel.size_hint_x = .8

        def GenderObj():
            """
            Edit Gender value
            """
            global _Gender
            _m = ModalView(size_hint=(.4,.24))

            _l = FloatLayout()
            _l.orientation = "vertical"

            _Gender = RemoteValues.Gender

            _Gen = Spinner(
                    # default value shown
                    text=str(_Gender),
                    # available values
                    values=('Male', 'Female'),
                    size_hint=(.8, .5),
                    size=(100, 44),
                    pos_hint={'center_x': .5, 'center_y': .6})

            def saveGen():
                global _Gender

                if str(_Gen.text) != str(_Gender):
                    EditGen.text = "Edited"

                else:
                    EditGen.text = "Edit"
                _Gender = _Gen.text
                try:
                    SingleExecute(username,"Gender",_Gen.text)

                finally:
                    _m.dismiss()

            _save = Button(text="Save Gender")
            _save.background_normal = "Data/Colours/15.jpg"
            _save.background_down = _save.background_down
            _save.size_hint = .97,.3
            _save.pos_hint = PinoyChat_XY(.5,.1)
            _save.on_press = saveGen

            _ti = TextInput()
            _ti.text = RemoteValues.Age
            _ti.size_hint = 1,.12
            _ti.pos_hint = PinoyChat_XY(.5,.5)

            _l.add_widget(_Gen)
            _l.add_widget(_save)

            _m.add_widget(_l)
            _m.open()

        EditGen = Button(text="Edit")
        EditGen.background_normal = "Data/Colours/15.jpg"
        EditGen.background_down = EditGen.background_normal
        EditGen.size_hint = .3,.1
        EditGen.pos_hint = PinoyChat_XY(.7,.58)
        EditGen.on_press = GenderObj

        LocLabel = Label(text=" Location: [b]....[/b]")
        LocLabel.pos_hint = PinoyChat_XY(.28,.46)
        LocLabel.markup = True
        LocLabel.size_hint_x = .8

        def LocationObj():
            """
            Editing Location Values
            """
            global _Location
            _m = ModalView(size_hint=(.6,.24))

            _l = FloatLayout()
            _l.orientation = "vertical"

            _Location = RemoteValues.Location

            _Loc = TextInput(text=str(_Location),size_hint=(.8, .5),
                        pos_hint={'center_x': .5, 'center_y': .6})

            def saveLoc():
                global _Location

                if len(_Loc.text) == 0 or len(_Loc.text) >= 11:
                    self.Warning("Oops, must be too short/long!")
                    return False

                if str(_Loc.text) != str(_Location):
                    EditLoc.text = "Edited"
                    _m.dismiss()

                elif str(_Loc.text) == str(_Location):
                    EditLoc.text = "Edit"

                _Location = _Loc.text
                try:
                    SingleExecute(username,"Location",_Loc.text)

                finally:
                    _m.dismiss()

            _save = Button(text="Save Location")
            _save.background_normal = "Data/Colours/15.jpg"
            _save.background_down = _save.background_down
            _save.size_hint = .97,.3
            _save.pos_hint = PinoyChat_XY(.5,.1)
            _save.on_press = saveLoc

            _ti = TextInput()
            _ti.text = RemoteValues.Age
            _ti.size_hint = 1,.12
            _ti.pos_hint = PinoyChat_XY(.5,.5)

            _l.add_widget(_Loc)
            _l.add_widget(_save)

            _m.add_widget(_l)
            _m.open()

        EditLoc = Button(text="Edit")
        EditLoc.background_normal = "Data/Colours/15.jpg"
        EditLoc.background_down = EditLoc.background_normal
        EditLoc.size_hint = .3,.1
        EditLoc.pos_hint = PinoyChat_XY(.7,.46)
        EditLoc.on_press = LocationObj

        AmeLabel = Label(text=" About Me: [b]....[/b]")
        AmeLabel.pos_hint = PinoyChat_XY(.28,.34)
        AmeLabel.markup = True
        AmeLabel.size_hint_x = .8

        def AbmeObj():
            """
            Function for editing About me Values
            """
            global _Aboutme
            _m = ModalView(size_hint=(.7,.4),auto_dismiss=False)

            _l = FloatLayout()
            _l.orientation = "vertical"

            _Aboutme = RemoteValues.AboutMe

            _Am = TextInput(text=str(_Aboutme),size_hint=(.9, .5),
                        pos_hint={'center_x': .5, 'center_y': .6})

            def saveAm():
                global _Aboutme

                if len(_Am.text) >= 100:
                    self.Warning("The Maximum length is 99!")
                    return False

                if len(_Am.text) <= 4:
                    self.Warning("The Minimum characters is 4!")
                    return False

                if str(_Am.text) != str(_Aboutme):
                    EditAme.text = "Edited"

                elif str(_Am.text) == str(_Aboutme):
                    EditAme.text = "Edit"

                _Aboutme = _Am.text
                try:
                    SingleExecute(username,"AboutMe",_Am.text)

                finally:
                    _m.dismiss()

            _save = Button(text="Save About Me")
            _save.background_normal = "Data/Colours/15.jpg"
            _save.background_down = _save.background_down
            _save.size_hint = .97,.3
            _save.pos_hint = PinoyChat_XY(.5,.1)
            _save.on_press = saveAm

            _ti = TextInput()
            _ti.text = RemoteValues.Location
            _ti.size_hint = 1,.12
            _ti.pos_hint = PinoyChat_XY(.5,.5)

            _l.add_widget(_Am)
            _l.add_widget(_save)

            _m.add_widget(_l)
            _m.auto_dismiss=1
            _m.open()


        EditAme = Button(text="Edit")
        EditAme.background_normal = "Data/Colours/15.jpg"
        EditAme.background_down = EditAme.background_normal
        EditAme.size_hint = .3,.1
        EditAme.pos_hint = PinoyChat_XY(.7,.34)
        EditAme.on_press = AbmeObj


        def SaveToServer():
            global username
            from Engine.User import Core
            A = None
            L = None
            G = None
            AM = None
            _Age,_Location, _Gender, _Aboutme


            def QueryParser():
                """
                Parser for generating Query for update on remote.
                """
                Inject = ""
                Count = 0

                #Algorithm:  0x0x0x0x0x0
                #            x0x0x0x0x0x
                if _Gender:
                    Inject += "Gender='"+_Gender+"',"

                if _Age:
                    Inject += "Age='"+_Age+"',"


                if _Aboutme:
                    Inject += "AboutMe='"+_Aboutme+"',"

                if _Location:
                    Inject += "Location='"+_Location+"',"

                Raw = "UPDATE Accounts SET, {custom} WHERE Username='{us}';".format(custom=Inject,
                                                                                 us=username)
                if Inject:
                    UpdateEngine = Core()
                    UpdateEngine.UpdateUser(query=Raw) #Execute.

                else:
                    pass

        def showSelf():
            self.UserPage(instance=None,value=username)
            M.dismiss()

        ChangeNow = Button(text="View your profile")       # Save All.
        ChangeNow.size_hint = 1,.12
        ChangeNow.pos_hint = PinoyChat_XY(.5,.06)
        ChangeNow.background_normal = "Data/Colours/1.jpg"
        ChangeNow.background_down = ChangeNow.background_normal
        ChangeNow.on_press = showSelf

        ThrL.add_widget(BG2)
        ThrL.add_widget(InfoTitle)
        ThrL.add_widget(AgeLabel)
        ThrL.add_widget(EditAge)
        ThrL.add_widget(GLabel)
        ThrL.add_widget(EditGen)
        ThrL.add_widget(LocLabel)
        ThrL.add_widget(EditLoc)
        ThrL.add_widget(AmeLabel)
        ThrL.add_widget(EditAme)
        ThrL.add_widget(ChangeNow)

        # INFO SETTINGS

        First.add_widget(OneL) # OneLLayout into First AccordContent.
        Second.add_widget(TwoL)
        Third.add_widget(ThrL)

        MainAcc.orientation = "vertical"
        MainAcc.add_widget(First)
        MainAcc.add_widget(Second)
        MainAcc.add_widget(Third)

        M.add_widget(MainAcc)
        M.size_hint = 1,.8
        M.open()

    def ToScreen(self,scr):
        """
        Room warper functions
        """
        rootEngine.current = scr

    def ShowIntroInfo(self):
        pass

    def InstantUploadAndUpdate(self,u,p,binaryp):
        """
        Function for transfering custom avatars to coreEngine (PinoyChat Engine)
        """
        #from Engine.UEngine import PCWebLoader
        pcurl = "http://pctunnel.coresec.ca:5000/upload"
        filename = binaryp
        from Engine.UEngine import MultipartPostHandler
        import urllib2, cookielib

        cookies = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies),
                                        MultipartPostHandler)
        if u and p:

            params = { "pchatid" : str(u), "pchatpwd" : str(p),
                       "upload" : open(binaryp, "rb") }
            opener.open(pcurl, params)


    def InstantPChatAvatarLocal(self,onlyLoad=False):
        from kivy.utils import platform
        from kivy.uix.image import AsyncImage
        from os import path

        #GetType anon function for retriving File Extensions
        GetType = lambda spath: path.splitext(str(spath))[1][1:] if spath else None


        BG = Button()
        BG.background_normal = "Data/Colours/22.jpg"
        BG.background_down = BG.background_normal
        BG.size_hint = 1,1
        BG.pos_hint = PinoyChat_XY(.5,.5)

        OS = ""

        if platform == "android":
            OS = platform

        else:
            OS = "desk-"+str(platform)

        l = FloatLayout()

        LAView = AsyncImage()
        AS_RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
        _UsrImgObj = str(AS_RunGetter.Avatar)
        LAView.source = _UsrImgObj #Avatar

        try:
            LAView.reload()
        except:
            pass

        LAView.pos_hint = PinoyChat_XY(.5,.6)
        LAView.size_hint = .8,.6
        LAView.allow_stretch = True
        LAView.keep_ratio = False

        def TakePic():
            """
            Functions for capturing an image, uses Plyer as image object loader
            """
            from plyer import camera
            from Engine.User import Core, MultiPartForm

            if OS == "android": # Only designed to work for android

                def finished(e): #receive e as the image location
                    "This function runs when CaptureNow() function finished the proc."

                    path = e #update the label to the image location

                    try:

                        try:
                            self.InstantUploadAndUpdate(u=username,p=AS_RunGetter.Password,binaryp=path)
                            LAView.source = str(e)
                            LAView.reload()
                            GetRemCam = pinoyChatClientApp().AccountValuesLoader(username=username)
                            CamRemV = GetRemCam.Avatar
                        finally:
                            m.dismiss()
                            Cache.remove('kv.image')
                            Cache.remove('kv.texture')
                            self.cs_Logo.source = str(CamRemV)

                            self.Warning("Avatar Updated!")


                    except:
                        self.Warning("Ooops, Unable to Update avatar!")


                def  CaptureNow():
                    "Only works with android"
                    import random
                    GenInt = lambda ints: random.randrange(int(ints),543)
                    FinalName = "/storage/sdcard0/pinoychat-"+str(GenInt(99))+str(GenInt(200))+str(GenInt(143))+".jpg" # file name gen / good to prevent dupes
                    camera.take_picture(str(FinalName), finished) #Take a picture and save at this location. After will call done() callback

                CaptureNow()    # Auto start the Camera.

            else:
                self.Warning("This option only supports Mobile")



        def LoadPic():  # Works for both mobile and desktop platforms
            """
            Image Obj loader
            """
            from kivy.uix.filechooser import FileChooserIconView, FileChooserListView

            BG = Button()
            BG.background_normal = "Data/Colours/17.jpg"
            BG.background_down = BG.background_normal
            BG.size_hint = 1,1
            BG.pos_hint = PinoyChat_XY(.5,.5)

            ilabel = Label(text="Please provide an awesome picture of yours!")
            ilabel.pos_hint = PinoyChat_XY(.5,.95)

            m = ModalView()
            m.size_hint = 1,.8

            l = FloatLayout()

            iView = FileChooserListView()

            if OS == "android":
                iView.path = "/storage/sdcard0/"

            else:
                pass

            iView.pos_hint = PinoyChat_XY(.5,.5)
            iView.size_hint = .98,.8

            def loader():
                """
                File loader function (From SDA)
                """
                from Engine.User import Core
                from kivy.logger import Logger
                import imghdr

                cpath, cselection = iView.path, iView.selection
                _Load = ""
                try:

                    files = cselection[0] # first.
                    allowedExt = ["jpg","jpeg","png","PNG","JPG"]   # list of available img files.

                    if str(GetType(files)) in allowedExt:
                        _Load = ""

                        try:
                            LAView.source = files
                            self.InstantUploadAndUpdate(u=username,p=AS_RunGetter.Password,binaryp=files)
                            GetRem = pinoyChatClientApp().AccountValuesLoader(username=username)
                            RemV = GetRem.Avatar

                        finally:
                            Cache.remove('kv.image')
                            Cache.remove('kv.texture')
                            self.cs_Logo.source = str(RemV)

                            self.Warning("Avatar Updated!")

                            m.dismiss()

                    else:
                        raise # go to Exception

                except:
                    self.Warning("Please provide an Image file.")



            btn = Button(text='Load and Update')
            btn.background_normal = "Data/Colours/7.jpg"
            btn.background_down = btn.background_normal
            btn.on_press = loader
            btn.size_hint = 1,.1
            btn.pos_hint = PinoyChat_XY(.5,.05)

            l.add_widget(BG)
            l.add_widget(iView)
            l.add_widget(ilabel)
            l.add_widget(btn)

            m.add_widget(l)
            m.open()

        take = Button(text="Take a Picture (Smile)")
        take.background_normal = "Data/Colours/16.jpg"
        take.background_down = take.background_normal
        take.size_hint = 1,.13
        take.pos_hint = PinoyChat_XY(.5,.2)
        take.on_press = TakePic

        load = Button(text="Load Image ...")
        load.background_normal = "Data/Colours/16.jpg"
        load.background_down = load.background_normal
        load.size_hint = 1,.13
        load.pos_hint = PinoyChat_XY(.5,.06)
        load.on_press = LoadPic

        l.add_widget(BG)
        l.add_widget(LAView)
        l.add_widget(take)
        l.add_widget(load)

        m = ModalView()
        m.add_widget(l)
        m.size_hint = .8,.5

        if onlyLoad:
            LoadPic()
        else:
            m.open()



    def ProcessAvatar(spinner,value):
        """
        Function for processing chosen action(value)
        """
        action = ['From Avatars', 'From Storage', 'Import from Web']

        if value == action[0]:
            # PChat Avatars
            PChatAvatarPchat()

        elif value == action[1]:
            # Local
            PChatAvatarLocal()

        elif value == action[2]:
            #Via Web
            PChatAvatarWeb()



        SpinAction = Spinner(
            text="Edit Avatar",
            values=('From Avatars', 'From Storage', 'Import from Web'),
            size_hint=(.5, None),
            size=(300, 44),
            pos_hint={'center_x': .5, 'center_y': .4})
        SpinAction.background_normal = "Data/Colours/8.jpg"
        SpinAction.background_down = SpinAction.background_normal
        SpinAction.bind(text=ProcessAvatar)

        TwoL.add_widget(SAvatarView)
        TwoL.add_widget(SpinAction)
        TwoL.add_widget(SaveAvatar)
    # AVATAR SETTINGS



    def Warning(self,message,close=0):
        """
        Method (ModalView) with a simple pop-up and message.
        """
        _M = ModalView(auto_dismiss=True)
        _L = FloatLayout()
        _BG = Button()
        _BG.background_normal = "Data/Colours/13.jpg"
        _BG.background_down = _BG.background_normal
        _BG.pos_hint = PinoyChat_XY(.5,.5)

        _Mess = str(message)
        _Label = Label()
        _Label.text = _Mess
        _Label.color = parse_color("#475577")
        _Label.pos_hint = PinoyChat_XY(.5,.5) # Center

        _L.add_widget(_BG)
        _L.add_widget(_Label)
        _M.add_widget(_L)
        _M.size_hint = 1,.08
        _M.pos_hint = PinoyChat_XY(.5,.9)
        if close == 1:
            _M.dismiss()
        _M.open()

    def Logged(self):
        print self.cs_Send

    def EnterMain(self):


        global curr_h
        global username
        global Room
        global initsend
        initsend = 0
        #self.cs_Send.font_name = "DroidSans"
        self.cs_Send.font_size = '19sp'
        self.cs_Send.size_hint = 1,.16
        self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
        self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


        if len(str(username)) == 0:
            self.ToScreen("Login")
            Modality.dismiss()

        else:

            self.MessageGetFunc(type="Main",room="Manila")
            self.chat_overlay.text = ""+coreEngine.Core.mone
            reactor.connectTCP(str(curr_h), 4321, EchoFactory(self))
            self.to_chat_Window()
            Modality.dismiss()
            Room = "Manila"

    def JoinTransfer(self):
        return self.EnterMain()

    def ScreenLogin(self, call=0,sign=False,Username=None,Password=None):
        """
        Screen with Login Functions and UI
        """
        from Engine import User, Resource
        global username, guest

        class RegisterNow(object):
            """
            Class that Groups everything in Registration Page.
            """
            @staticmethod
            def Main():
                """
                static method, returns the whole registration pop,
                Concept of this Registration pop, will Ask for some useful information,
                then Other options for your account will be available in your control panel, e.g Age, etc...
                """
                colorsData = "Data/Colours/"
                Layout = FloatLayout()
                global username
                global Avatar, Username, Password, Gender
                Avatar, Username, Password, Gender = None,None,None,"b" # B/G girls/boys
                Count = 0

                def ProcessAll():
                    """
                    Function to Process the Information to Server
                    """
                    global username
                    # Checks required, unlike Sign-in uses decorator.
                    strUsername = UBox.text
                    spaces_removed = strUsername.replace(" ","")
                    final_username = spaces_removed.translate("~!@#$%^&*,./:;'`()_+|[]?<>=-\"")

                    if len(final_username) < 4 or len(final_username) >= 9:
                        # Username must be greater than 4 or equal or less than 9
                        UBox.text = ""
                        self.Warning("Check your username!")
                        return False

                    elif len(PBox.text) < 4 or len(PBox.text) >= 16:
                        PBox.text = ""
                        self.Warning("Check your password!")

                        return False

                    # End

                    else:   # Passed
                        InformationParser = User.Core(registration=True)
                        Proc = InformationParser.UserRegistration(u=final_username,
                                                           p=PBox.text,
                                                           a=ChooseAvatar.background_normal,
                                                           g=VGender.text)

                        # Passed
                        if "Thank you" in str(Proc):
                            try:
                                username = final_username
                                MView.dismiss()
                                pinoyChatClientApp().ToScreen("screenIntro")

                            finally:
                                pinoyChatClientApp().Warning(str(Proc))

                        # Not Passed
                        else:
                            pinoyChatClientApp().Warning(str(Proc))


                def CloseReg():
                    """
                    Close Function for MView()
                    """
                    MView.dismiss()

                def AvatarSetter(GenderInit):
                    """
                    Trigger when Gender has changed.
                    """
                    ChooseAvatar.background_normal = "Data/Account/Avatars/" +GenderInit+ "1.jpg"
                    ChooseAvatar.background_down = ChooseAvatar.background_normal

                def GenderChoose(spinner,text):
                    """
                    Trigger when Gender changed, then call AvatarSetter(Gender)
                    """
                    global Gender

                    if text == "Male":
                        AvatarSetter("b")
                        Gender = "b"

                    elif text == "Female":
                        AvatarSetter("g")
                        Gender = "g"

                def AvatarChoose():
                    """
                    Do the Auto Change Avatar on Click of Image.
                    """
                    global Avatar, Gender

                    IntegerCount = int(ChooseAvatar.background_normal[-5]) # 1-6

                    if IntegerCount == 6:
                        ChooseAvatar.background_normal = "Data/Account/Avatars/"+Gender+"1.jpg"
                        ChooseAvatar.background_down = ChooseAvatar.background_normal

                    else:
                        Incremented = IntegerCount + 1  # Increment image name by 1
                        ChooseAvatar.background_normal = "Data/Account/Avatars/" + Gender + str(Incremented)+".jpg"
                        ChooseAvatar.background_down = ChooseAvatar.background_normal


                # Widgets {
                MainBG = Button()
                MainBG.background_normal = colorsData+"13.jpg"
                MainBG.background_down = MainBG.background_normal
                MainBG.pos_hint = PinoyChat_XY(.5,.5)

                CloseButton = Button() # Option Button
                CloseButton.background_normal = "Data/IOData/close.png"
                CloseButton.background_down = "Data/IOData/close.png"
                CloseButton.size_hint = .20,.12
                CloseButton.pos_hint = {'center_x': .89, 'center_y': 0.93}
                CloseButton.on_release = CloseReg

                CALogo = Button()
                CALogo.background_normal = "Data/Account/createac.jpg"
                CALogo.background_down = CALogo.background_normal
                CALogo.pos_hint = PinoyChat_XY(.5,.8)
                CALogo.size_hint = .9,.25

                GenderLabel = Label(text="GENDER")
                GenderLabel.bold = True
                GenderLabel.pos_hint = PinoyChat_XY(.5,.65)
                GenderLabel.font_size = "19sp"
                GenderLabel.color = parse_color("#475577")
                GenderLabel.font_name = "Data/fonts/ambrosia.ttf"

                VGender = Spinner(
                    # default value shown
                    text="Male",
                    # available values
                    values=('Male', 'Female'),
                    size_hint=(.5, None),
                    size=(100, 44),
                    pos_hint={'center_x': .5, 'center_y': .6})
                VGender.background_normal = "Data/Colours/1.jpg"
                VGender.font_name = "Data/fonts/ambrosia.ttf"
                VGender.bind(text=GenderChoose)

                AvatarLabel = Label(text="CHOOSE AVATAR")
                AvatarLabel.bold = True
                AvatarLabel.pos_hint = PinoyChat_XY(.5,.5)
                AvatarLabel.font_size = "19sp"
                AvatarLabel.color = parse_color("#475577")
                AvatarLabel.font_name = "Data/fonts/ambrosia.ttf"

                ChooseAvatar = Button()
                ChooseAvatar.size_hint = .45,.23
                ChooseAvatar.background_normal = "Data/Account/Avatars/b1.jpg"
                ChooseAvatar.background_down = "Data/Account/Avatars/b1.jpg"
                ChooseAvatar.pos_hint = PinoyChat_XY(.5,.37)
                ChooseAvatar.on_release = AvatarChoose

                UsrLabel = Label(text="USERNAME")
                UsrLabel.bold = True
                UsrLabel.pos_hint = PinoyChat_XY(.2,.18)
                UsrLabel.font_size = "19sp"
                UsrLabel.color = parse_color("#475577")
                UsrLabel.font_name = "Data/fonts/ambrosia.ttf"

                PwdLabel = Label(text="PASSWORD")
                PwdLabel.bold = True
                PwdLabel.pos_hint = PinoyChat_XY(.2,.11)
                PwdLabel.font_size = "19sp"
                PwdLabel.color = parse_color("#475577")
                PwdLabel.font_name = "Data/fonts/ambrosia.ttf"

                Under = Button()
                Under.background_normal = "Data/Colours/23.jpg"
                Under.background_down = Under.background_normal
                Under.size_hint = .9,.17
                Under.pos_hint = PinoyChat_XY(.5,.15)

                UBox = TextInput()
                UBox.size_hint = .5,.06
                UBox.pos_hint = PinoyChat_XY(.6,.18)
                UBox.hint_text = "Your username"
                UBox.multiline = False
                UBox.foreground_color = parse_color("#475577")
                UBox.background_normal = "Data/Colours/23.jpg"

                PBox = TextInput()
                PBox.size_hint = .5,.06
                #PBox.password = True
                PBox.pos_hint = PinoyChat_XY(.6,.11)
                PBox.foreground_color = parse_color("#475577")
                PBox.hint_text = "Your password"
                PBox.background_normal = "Data/Colours/23.jpg"



                GoButton = Button(text="Sign Up!")
                GoButton.background_normal = "Data/Colours/1.jpg"
                GoButton.background_down = "Data/Colours/1.jpg"
                GoButton.size_hint = 1,.1
                GoButton.pos_hint = PinoyChat_XY(.5,.001)
                GoButton.on_release = ProcessAll
                # Widgets }

                Layout.add_widget(MainBG)
                Layout.add_widget(CALogo)
                Layout.add_widget(CloseButton)
                Layout.add_widget(GenderLabel)
                Layout.add_widget(VGender)
                Layout.add_widget(AvatarLabel)
                Layout.add_widget(ChooseAvatar)
                Layout.add_widget(Under)
                Layout.add_widget(UsrLabel)
                Layout.add_widget(PwdLabel)
                Layout.add_widget(UBox)
                Layout.add_widget(PBox)
                Layout.add_widget(GoButton)

                MView = ModalView(auto_dismiss=False)
                MView.size_hint = .96,.9
                MView.add_widget(Layout)
                return MView.open()

        class SignInNow(pinoyChatClientApp): #Inherits @ pccapp class
            """
            Modalview for Signin Prompt.
            """
            global username
            global Loggedin
            global guest
            global initsend

            def __init__(self,guest=False,direct=False):
                self.isGuest = guest
                self.Direct = direct
                self.DirectU = Username
                self.DirectP = Password


            def Main(self):
                global username
                global guest
                global initsend
                global Loggedin

                Layout = FloatLayout()
                BG = Button()
                BG.background_normal = "Data/Colours/23.jpg"
                BG.background_down = BG.background_normal
                BG.pos_hint = PinoyChat_XY(.5,.5)

                Logo = Button()
                Logo.background_normal = "Data/Resource/logoglp.png"
                Logo.background_down = Logo.background_normal
                Logo.size_hint = .55,.45
                Logo.pos_hint = PinoyChat_XY(.5,.99+.18)

                Banner = Button()
                Banner.background_normal = "Data/Colours/23.jpg"
                Banner.pos_hint = PinoyChat_XY(.5,.5)
                Banner.size_hint = 1,.5

                bLabel = Label(text="USER ACCOUNT")
                bLabel.bold = True
                bLabel.pos_hint = PinoyChat_XY(.5,.9)
                bLabel.font_size = "23sp"
                bLabel.color = parse_color("#475577")
                bLabel.font_name = "Data/fonts/ambrosia.ttf"

                def ServerReturned(errormsg):
                    """
                    Checks Everything, (Decorator.)
                    """
                    global guest
                    global username
                    global initsend
                    print initsend, "InitSend Stats.."
                    global Room
                    global Loggedin

                    if "Welcome back" not in errormsg:
                        username = ""
                        pinoyChatClientApp().Warning(str(errormsg))
                        Loggedin = False

                    else: # Authenticated.

                        username = UBox.text
                        Loggedin = True

                        #self.to_rooms_Window()

                def CheckPoint(procfunc):
                    """
                    Checks Everything, (Decorator.)
                    """

                    def ShowErr(mess):
                        def DelayerFunc(dt):
                            Layout.remove_widget(_B)

                        _B = Label(text=mess)
                        _B.font_name = "Data/fonts/ambrosia.ttf"
                        _B.pos_hint = PinoyChat_XY(.5,.1-.18)
                        _B.color = parse_color("#339966")
                        Layout.add_widget(_B)
                        Clock.schedule_once(DelayerFunc,4)

                    # Decorate starts
                    def check():
                        _GuestValue = self.isGuest
                        _Username = _Nickname  = UBox.text
                        _Password = str(PBox.text)

                        strUsername = str(_Nickname)
                        spaces_removed = strUsername.replace(" ","")
                        final_username = spaces_removed.translate(None,"~!@#$%^&*,./:;'`()_+|\"")

                        if _GuestValue:     # If Guest

                            if len(final_username) >= 9 or len(final_username) < 4:           # < 4 or > 8 not allowed.
                                pinoyChatClientApp().Warning("Please provide atleast 4-8 Characters.")
                                UBox.text = ""
                                return False

                            else:
                                procfunc(guestname=final_username)

                        elif not _GuestValue:   # Registered

                            if len(final_username) >= 9 or len(final_username) < 4:
                                #ShowErr("Username must have atleast 4-8 Characters")
                                pinoyChatClientApp().Warning("Username must have atleast 4-8 Characters")
                                UBox.text = ""
                                return False

                            elif len(_Password) < 4 and len(_Password) >= 16:
                                #ShowErr("Password must have atleast 4-16 Characters")
                                pinoyChatClientApp().Warning("Password must have atleast 4-16 Characters")
                                PBox.text = ""
                                return False

                            else:
                                procfunc(U=final_username, P=_Password)

                    return check


                @CheckPoint
                def ProcessLogin(guestname=None,U=None,P=None):
                    """
                    Function that Process everything to make your connection
                    to server.
                    """
                    global username
                    global guest

                    Usern = Nick = UBox.text
                    Pass = PBox.text

                    UBox.text = self.DirectU # inject
                    PBox.text = self.DirectP # inject

                    if not self.isGuest:
                        # If user has an Account
                        Engine = User.Core(userlogin=True)
                        return ServerReturned(str(Engine.UserProcessCred(u=str(U),
                                               p=str(P))))
                        # The Final Process are inside the ServerReturned function

                    elif self.isGuest:

                        if len("pc-"+username) >= 5 or len("pc-"+username) >= 9:
                            self.Warning("A Name must have atleast 4-8 Characters")
                            return False

                        else:
                            Rem_Spaces = str(UBox.text).replace(" ","")
                            FinalGName = str(Rem_Spaces).translate(None,"~!@#$%^&*,./:;'`()_+|[]?<>=-\"")

                            if len("pc-"+FinalGName) >= 5 or len("pc-"+FinalGNamee) <= 9:

                                Rem_Spaces = str(UBox.text).replace(" ","")
                                FinalGName = str(Rem_Spaces).translate(None,"~!@#$%^&*,./:;'`()_+|[]?<>=-\"")
                                username = "pc-"+str(FinalGName)
                                guest = 1
                                self.ToScreen("screenIntro")
                                _MView.dismiss()

                            else:
                                self.Warning("A Name must have atleast 4-8 Characters")

                def SignGuestOrMember(instance):
                    """
                    Access to UBox Object
                    """
                    Hint = instance.hint_text
                    Text = instance.text
                    if "Nickname" in Hint:
                        self.isGuest = True

                UBox = TextInput()
                UBox.size_hint = .55,.13
                UBox.pos_hint = PinoyChat_XY(.6,.76)
                UBox.hint_text = " Username"
                UBox.multiline = False
                UBox.foreground_color = parse_color("#475577")
                UBox.background_normal = "Data/Colours/23.jpg"
                UBox.focus = True

                def ProcWarper(value):
                    return ProcessLogin()

                PBox = TextInput()
                PBox.size_hint = .55,.13
                PBox.password = True
                PBox.pos_hint = PinoyChat_XY(.6,.58)
                PBox.multiline = False
                PBox.foreground_color = parse_color("#475577")
                PBox.hint_text = " Password"
                PBox.background_normal = "Data/Colours/23.jpg"
                PBox.bind(on_text_validate=ProcWarper)

                UsrLabel = Label(text="Username")
                UsrLabel.bold = True
                UsrLabel.pos_hint = PinoyChat_XY(.18,.76)
                UsrLabel.font_size = "19sp"
                UsrLabel.color = parse_color("#475577")
                UsrLabel.font_name = "Data/fonts/ambrosia.ttf"

                PwdLabel = Label(text="Password")
                PwdLabel.bold = True
                PwdLabel.pos_hint = PinoyChat_XY(.18,.58)
                PwdLabel.font_size = "19sp"
                PwdLabel.color = parse_color("#475577")
                PwdLabel.font_name = "Data/fonts/ambrosia.ttf"

                LoginButton = Button(text="LOGIN")
                LoginButton.pos_hint = PinoyChat_XY(.5,.29)
                LoginButton.background_normal = "Data/Colours/7.jpg"
                LoginButton.background_down = LoginButton.background_normal
                LoginButton.size_hint = 1,.2
                LoginButton.on_press = ProcessLogin

                _MView = ModalView()

                def OpenRegMain():
                     _MView.dismiss()
                     RegisterNow.Main()

                RegButton = Button(text="Create Account")
                RegButton.pos_hint = PinoyChat_XY(.5,.1)
                RegButton.background_normal = "Data/Colours/25.jpg"
                RegButton.background_down = LoginButton.background_normal
                RegButton.size_hint = 1,.2
                RegButton.on_release = lambda: OpenRegMain()

                def ReconstructGuest():
                    """
                    Change all appearance for Guest Login
                    """
                    Layout.add_widget(BG)
                    Layout.add_widget(Logo)
                    Layout.add_widget(UBox)
                    UBox.hint_text = "Nickname: guest"
                    UsrLabel.text = "Nickname"
                    UsrLabel.pos_hint = PinoyChat_XY(.18,.7)
                    UBox.pos_hint = PinoyChat_XY(.6,.7)

                    Layout.add_widget(LoginButton)
                    LoginButton.text = "LOGIN (AS GUEST)"
                    Layout.add_widget(RegButton)
                    Layout.add_widget(UsrLabel)


                if self.isGuest:
                    ReconstructGuest()

                else:
                    Layout.add_widget(BG)
                    Layout.add_widget(Logo)
                    Layout.add_widget(UBox)
                    Layout.add_widget(PBox)
                    Layout.add_widget(LoginButton)
                    Layout.add_widget(RegButton)
                    Layout.add_widget(UsrLabel)
                    Layout.add_widget(PwdLabel)
                    #Layout.add_widget(Banner)
                    #Layout.add_widget(bLabel)

                _MView.add_widget(Layout)
                _MView.size_hint = .9,.4
                _MView.pos_hint = PinoyChat_XY(.5,.6)
                #return _MView.open()

                if self.Direct: # 2.0.2
                    UBox.text = self.DirectU
                    PBox.text = self.DirectP
                    return ProcWarper(True)


        if call == 1:       # just a simple hack to access this inner class
            SignInNow().Main()

        elif call == 2:
            RegisterNow().Main() # just a simple hack to access this inner class

        elif sign:
            SignInNow(direct=True).Main()

        scr = Screen(name = "Login")
        Main = FloatLayout()
        BG = Button()
        BG.background_normal = Resource.imageData["Account"]+"bg.jpg"
        BG.background_down = BG.background_normal

        Logo = Button()
        Logo.background_normal = Resource.imageData["Resource"]+"logoglp.png"
        Logo.background_down = Logo.background_normal
        Logo.size_hint = .9,.4
        Logo.pos_hint = PinoyChat_XY(.5,.6)

        Signin = Button()
        Signin.background_normal = "Data/Account/colors.png"
        Signin.background_down = Signin.background_normal
        Signin.size_hint = .6,.1
        Signin.pos_hint = PinoyChat_XY(.3,.3)
        Signin.on_release = lambda: SignInNow(guest=False).Main()

        def callGuestLogin():
            construct = SignInNow(guest=True)
            construct.Main()

        Guest = Button()
        Guest.background_normal = "Data/Account/guest.png"
        Guest.background_down = Guest.background_normal
        Guest.size_hint = .4,.1
        Guest.pos_hint = PinoyChat_XY(.8,.3)
        Guest.on_release = callGuestLogin

        SignLabel = Label(text="Sign-In your account!")
        SignLabel.font_size = "15sp"
        SignLabel.font_name = "Data/fonts/ambrosia.ttf"
        SignLabel.color = parse_color("#696969")
        SignLabel.pos_hint = PinoyChat_XY(.3,.25)

        GuestLabel = Label(text="   Chat as Guest.")
        GuestLabel.font_size = "15sp"
        GuestLabel.font_name = "Data/fonts/ambrosia.ttf"
        GuestLabel.color = parse_color("#696969")
        GuestLabel.pos_hint = PinoyChat_XY(.8,.25)

        OrLabel = Label(text="or")
        OrLabel.font_size = "18sp"
        OrLabel.bold = True
        OrLabel.font_name = "Data/fonts/ambrosia.ttf"
        OrLabel.color = parse_color("#696969")
        OrLabel.pos_hint = PinoyChat_XY(.59,.3)

        JoinLabel = Label(text="Make your Account now, it's free.")
        JoinLabel.font_size = "18sp"
        JoinLabel.bold = True
        JoinLabel.font_name = "Data/fonts/ambrosia.ttf"
        JoinLabel.color = parse_color("#696969")
        JoinLabel.pos_hint = PinoyChat_XY(.5,.05)

        Trans = Button()
        Trans.background_normal = "Data/Account/trans.png"
        Trans.background_down = Trans.background_normal
        Trans.size_hint = 1,.08
        Trans.pos_hint = PinoyChat_XY(.47,.38)

        TTrans = Button()
        TTrans.background_normal = "Data/Account/ttrans.png"
        TTrans.background_down = TTrans.background_normal
        TTrans.size_hint = 1,.08
        TTrans.pos_hint = PinoyChat_XY(.47,.18)

        Join = Button()
        Join.background_normal = "Data/Account/join.png"
        Join.background_down = Join.background_normal
        Join.size_hint = .63,.1
        Join.pos_hint = PinoyChat_XY(.5,.1)
        Join.on_press = lambda: RegisterNow().Main()

        Main.add_widget(BG)
        Main.add_widget(Logo)
        Main.add_widget(Signin)
        Main.add_widget(Guest)
        Main.add_widget(SignLabel)
        Main.add_widget(GuestLabel)
        Main.add_widget(OrLabel)
        Main.add_widget(Trans)
        Main.add_widget(Join)
        Main.add_widget(JoinLabel)
        Main.add_widget(TTrans)

        scr.add_widget(Main)

        return scr

    def SidebarContents(self,user=False,logged=False):
        """
        Sidebar View
        """
        from kivy.uix.image import AsyncImage
        global username
        global Loggedin

        BG = Button()
        BG.background_normal = "Data/Colours/2.jpg"
        BG.background_down = BG.background_normal


        self.SideName.font_name = "Data/fonts/ambrosia.ttf"
        self.SideName.background_normal = "Data/Colours/13.jpg"
        self.SideName.color = parse_color("#424242")
        self.SideName.size_hint = 1.1,.07
        self.SideName.pos_hint = PinoyChat_XY(.5,.97)
        self.SideName.font_size = 25
        #self.SideName.background_color = 0,0,0,0
        self.SideName.border = 1,4,1,4
        #self.SideName.opacity =
        self.SideName.background_down = self.SideName.background_normal

        self.SideEdit.font_name = "Data/fonts/ambrosia.ttf"
        self.SideEdit.size_hint = 1.1,.07
        self.SideEdit.pos_hint = PinoyChat_XY(.5,.588)
        self.SideEdit.font_size = 25
        self.SideEdit.border = 1,4,1,4
        self.SideEdit.opacity = .7
        self.SideEdit.on_release = self.EditAccountObj



        self.SideEditAv.font_name = "Data/fonts/ambrosia.ttf"
        self.SideEditAv.size_hint = 1.1,.07
        self.SideEditAv.pos_hint = PinoyChat_XY(.5,.655)
        self.SideEditAv.font_size = 25
        self.SideEditAv.border = 1,4,1,4
        self.SideEditAv.opacity = .7

        self.SideMessage.font_name = "Data/fonts/ambrosia.ttf"
        self.SideMessage.size_hint = 1.1,.07
        self.SideMessage.pos_hint = PinoyChat_XY(.5,.522)
        self.SideMessage.font_size = 25
        self.SideMessage.border = 1,4,1,4
        self.SideMessage.opacity = .7

        def PCLogOut(log=False):
            "Log Out fN"
            self.back_to_intro()

        self.SideOff.background_normal = "Data/Colours/17.jpg"
        self.SideOff.font_name = "Data/fonts/ambrosia.ttf"
        self.SideOff.size_hint = 1.1,.07
        self.SideOff.pos_hint = PinoyChat_XY(.5,.454)
        self.SideOff.font_size = 25
        self.SideOff.border = 1,4,1,4
        self.SideOff.opacity = .8
        self.SideOff.on_release = PCLogOut


        self.SideOffICO.font_name = "Data/Resource/ico/icon2.ttf"
        self.SideOffICO.size_hint = .27,.08
        self.SideOffICO.opacity = .6
        self.SideOffICO.background_color = 0,0,0,0
        self.SideOffICO.font_size = 24
        self.SideOffICO.pos_hint = PinoyChat_XY(.75,.456)


        self.SideAvatar.size_hint = 1,.25
        self.SideAvatar.pos_hint = PinoyChat_XY(.5,.813)
        self.SideAvatar.allow_stretch = True
        self.SideAvatar.keep_ratio = False
        self.blocker = Button(size_hint=(1,1))


        self.SideCameraICO.font_name = "Data/Resource/ico/icon2.ttf"
        self.SideCameraICO.size_hint = .27,.08
        self.SideCameraICO.opacity = .6
        #self.SideCameraICO.border = 1,4,1,4
        self.SideCameraICO.font_size = 36
        self.SideCameraICO.pos_hint = PinoyChat_XY(.114,.895)

        self.SideCameraLICO.font_name = "Data/Resource/ico/icon2.ttf"
        self.SideCameraLICO.size_hint = .27,.08
        self.SideCameraLICO.opacity = .6
        #self.SideCameraLICO.border = 1,4,1,4
        self.SideCameraLICO.font_size = 36
        self.SideCameraLICO.pos_hint = PinoyChat_XY(.114,.818)

        self.SideBarLay.add_widget(BG)


        # // Not Logged Side Panel
        def link_download():
            _url_share = """http://pinoychat.coresec.ca/downloads"""
            try:
                webbrowser.open(_url_share)
            except:
                self.pinoyChat_exception(errmsg="Sorry, something went wrong...")

        def link_rate():
            _url_share = """https://play.google.com/store/apps/details?id=org.coresec.pinoychat"""
            try:
                webbrowser.open(_url_share)
            except:
                self.pinoyChat_exception(errmsg="Sorry, something went wrong...")

        def link_share():
            _url_share = """https://www.facebook.com/sharer/sharer.php?u=https://play.google.com/store/apps/details?id=org.coresec.pinoychat"""
            try:
                webbrowser.open(_url_share)
            except:
                self.pinoyChat_exception(errmsg="Sorry, something went wrong...")

        def link_website():
            _url_share = """http://pinoychat.coresec.ca/"""
            try:
                webbrowser.open(_url_share)
            except:
                self.pinoyChat_exception(errmsg="Sorry, something went wrong...")

        self.Side_sec.size_hint = .5,1
        self.Side_sec.allow_stretch = True
        self.Side_sec.mipmap = True
        self.Side_sec.pos_hint = PinoyChat_XY(.5,.08)


        #self.Side_info.font_name = "Data/fonts/ambrosia.ttf"
        self.Side_info.size_hint = 1.1,.07
        self.Side_info.color = parse_color("#000000")
        self.Side_info.pos_hint = PinoyChat_XY(.5,.03)
        self.Side_info.font_size = 12
        self.Side_info.border = 1,4,1,4
        self.Side_info.opacity = .7
        self.Side_info.on_release = self.EditAccountObj


        self.Side_download.font_name = "Data/fonts/ambrosia.ttf"
        self.Side_download.size_hint = 1.1,.07
        self.Side_download.pos_hint = PinoyChat_XY(.5,.9)
        self.Side_download.font_size = 25
        self.Side_download.border = 1,4,1,4
        self.Side_download.opacity = .7
        self.Side_download.on_release = link_download


        self.Side_rate.font_name = "Data/fonts/ambrosia.ttf"
        self.Side_rate.size_hint = 1.1,.07
        self.Side_rate.pos_hint = PinoyChat_XY(.5,.83)
        self.Side_rate.font_size = 25
        self.Side_rate.border = 1,4,1,4
        self.Side_rate.opacity = .7
        self.Side_rate.on_release = link_rate


        self.Side_Like.font_name = "Data/fonts/ambrosia.ttf"
        self.Side_Like.size_hint = 1.1,.07
        self.Side_Like.pos_hint = PinoyChat_XY(.5,.69)
        self.Side_Like.font_size = 25
        self.Side_Like.border = 1,4,1,4
        self.Side_Like.opacity = .7
        self.Side_Like.on_release = link_share


        self.Side_website.font_name = "Data/fonts/ambrosia.ttf"
        self.Side_website.size_hint = 1.1,.07
        self.Side_website.pos_hint = PinoyChat_XY(.5,.76)
        self.Side_website.font_size = 25
        self.Side_website.border = 1,4,1,4
        self.Side_website.opacity = .7
        self.Side_website.on_release = link_website



        self.Side_infon.font_name = "Data/fonts/ambrosia.ttf"
        self.Side_infon.size_hint = 1.1,.07
        self.Side_infon.color = parse_color("#000000")
        self.Side_infon.pos_hint = PinoyChat_XY(.4,.6)
        self.Side_infon.font_size = 25
        self.Side_infon.border = 1,4,1,4
        self.Side_infon.opacity = .7
        self.Side_infon.on_release = self.EditAccountObj

        self.Side_infona.font_name = "Data/Resource/ico/icon.ttf"
        self.Side_infona.size_hint = 1.1,.07
        self.Side_infona.color = parse_color("#000000")
        self.Side_infona.pos_hint = PinoyChat_XY(.8,.6)
        self.Side_infona.font_size = 25
        self.Side_infona.border = 1,4,1,4
        self.Side_infona.opacity = .7
        self.Side_infona.on_release = self.EditAccountObj

        def LoadNews():
            t = None
            s = None
            try:
                p = urllib.urlopen("http://pinoychat.coresec.ca/news/News.pc")
                s = p.read()
            except:
                s = """
Unable to Load News
                """
            return s


        self.Side_Feeds.pos_hint = PinoyChat_XY(.5,.38)
        self.Side_Feeds.size_hint = 1,.4
        self.Side_Feeds.text = LoadNews()

        self.SideBarLay.add_widget(self.Side_download)
        self.SideBarLay.add_widget(self.Side_sec)
        self.SideBarLay.add_widget(self.Side_Like)
        self.SideBarLay.add_widget(self.Side_website)
        self.SideBarLay.add_widget(self.Side_rate)
        self.SideBarLay.add_widget(self.Side_infon)
        self.SideBarLay.add_widget(self.Side_Feeds)
        self.SideBarLay.add_widget(self.Side_infona)
        self.SideBarLay.add_widget(self.Side_info)


        # // Not Logged Side Panel
        def IntroPanel(): # Not Logged In.
            print "No Log"

        def PanelLogged(uname): # Logged In.
            who = str(uname)
            return False


        if username and logged:
            PanelLogged(username)

        else:
            IntroPanel()


        return self.SideBarLay

    # Main Intro (changed 2.0.2)
    def scr_intro(self):
        """
        Intro Page fN (SCREEN)
        """
        from kivy.garden.navigationdrawer import NavigationDrawer
        from kivy.animation import AnimationTransition, Animation
        from kivy.uix.behaviors import ButtonBehavior
        import webbrowser

        global Loggedin
        global showLoading
        global DataServer
        global NewsLoaded
        global slides
        global username
        Window.softinput_mode = 'pan'

        FLAYOUT = FloatLayout()

        scr = Screen(name = 'screenIntro')

        def CameraLoad(): # in left panel

            def UploadAndUpdate(u,p,binaryp):
                """
                Function for transfering custom avatars to coreEngine (PinoyChat Engine)
                """
                #from Engine.UEngine import PCWebLoader
                pcurl = "http://pctunnel.coresec.ca:5000/upload"
                filename = binaryp
                from Engine.UEngine import MultipartPostHandler
                import urllib2, cookielib

                cookies = cookielib.CookieJar()
                opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies),
                                                MultipartPostHandler)
                if u and p:

                    params = { "pchatid" : str(u), "pchatpwd" : str(p),
                               "upload" : open(binaryp, "rb") }
                    opener.open(pcurl, params)


            def TakePic(): # in lef pane
                """
                Functions for capturing an image, uses Plyer as image object loader
                """
                from plyer import camera
                from Engine.User import Core, MultiPartForm
                from kivy.utils import platform
                global username
                OS = platform


                if OS == "android": # Only designed to work for android

                    def finished(e): #receive e as the image location
                        "This function runs when CaptureNow() function finished the proc."

                        path = e #update the label to the image location

                        try:

                            try:
                                AS_RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
                                UploadAndUpdate(u=username,p=AS_RunGetter.Password,binaryp=path)

                            finally:
                                GetRem1 = pinoyChatClientApp().AccountValuesLoader(username=username)
                                RemV = GetRem1.Avatar
                                self.Warning("Avatar Updated!")
                                Cache.remove('kv.image')
                                Cache.remove('kv.texture')
                                self.SideAvatar.source = str(RemV)


                        except:
                            self.Warning("Ooops, Unable to Update avatar!")


                    def  CaptureNow():
                        "Only works with android"
                        import random
                        GenInt = lambda ints: random.randrange(int(ints),543)
                        FinalName = "/storage/sdcard0/pinoychat-"+str(GenInt(99))+str(GenInt(200))+str(GenInt(143))+".jpg" # file name gen / good to prevent dupes
                        camera.take_picture(str(FinalName), finished)

                    CaptureNow()    # Auto start the Camera.

                else:
                    self.Warning("This option only supports Mobile")
            TakePic()

        def Restruct_Panel():
            global username


            def avatar_camera(x):
                CameraLoad()

            def showSelf(x):
                self.UserPage(instance=None,value=username)

            def showmail(x):
                self.PinoyMail()
            try:
                self.SideBarLay.remove_widget(self.Side_infon)
                self.SideBarLay.remove_widget(self.Side_Feeds)
                self.SideBarLay.remove_widget(self.Side_website)
                self.SideBarLay.remove_widget(self.Side_Like)
                self.SideBarLay.remove_widget(self.Side_rate)
                self.SideBarLay.remove_widget(self.Side_download)
                self.SideBarLay.remove_widget(self.Side_info)
                self.SideBarLay.remove_widget(self.Side_infona)
                self.SideBarLay.remove_widget(self.Side_sec)



            finally:
                self.SideBarLay.add_widget(self.SideAvatar)
                self.SideBarLay.add_widget(self.SideName)
                self.SideBarLay.add_widget(self.SideEdit)
                self.SideBarLay.add_widget(self.SideEditAv)
                self.SideBarLay.add_widget(self.SideCameraICO)
                # self.SideBarLay.add_widget(self.SideCameraLICO)
                self.SideBarLay.add_widget(self.SideMessage)
                self.SideBarLay.add_widget(self.SideOff)
                self.SideBarLay.add_widget(self.SideOffICO)

            # Sets uname
            self.SideName.text = username

            # Sets Avatar
            try:
                RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
                self.SideAvatar.source = RunGetter.Avatar
            except:
                self.SideAvatar.source = "Data/Account/Avatars/guest.jpg"

            # open mini Prof
            self.SideEditAv.bind(on_press = showSelf)

            # open msg modal
            self.SideMessage.bind(on_press = showmail)

            # open cam
            self.SideCameraICO.bind(on_press = avatar_camera)

        def Focus():
            pass

        def DeFocus():
            pass

        def enter_main():
            """
            Added in 1.4.0
            Join Chat (Button from Intro)
            """
            global curr_h
            global username
            global Room
            from Engine import User
            global initsend
            initsend = 0

            if len(username) >= 4:
                if True:
                    curr_port = 4321
                    self.chat_overlay.text = ""+coreEngine.Core.mone
                    reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                    self.MessageGetFunc(type="Main",room="Manila")
                    Room = "Manila"
                self.to_chat_Window()

            else:
                self.ToScreen("Login")

        def OpenSide():
            self.Sidebar.toggle_state()

        BG = Button(background_normal="Data/Colours/2.jpg") # Background
        BG.background_down = BG.background_normal

        def shareOnFB():
            """
            Share on Facebook function
            """
            #_url_share = """https://m.facebook.com/dialog/feed?app_id=891081214237643&caption=PinoyChat is the New Filipino Application for Chatting within group (Chatrooms), Download #PinoyChat Now! Available at Google Playtore and Apple Appstore&link=https://play.google.com/store/apps/details?id=org.coresec.pinoychat&redirect_uri=https://play.google.com/store/apps/details?id=org.coresec.pinoychat """
            _url_share = "https://m.facebook.com/pinoychatapp"
            try:
                webbrowser.open(_url_share)
            except:
                self.pinoyChat_exception(errmsg="Sorry, something went wrong...")

        def followTwitter():
            """
            Open Twitter function
            """
            _url_share = "https://twitter.com/intent/follow?screen_name=mpequeras"
            try:
                webbrowser.open(_url_share)
            except:
                self.pinoyChat_exception(errmsg="Sorry, something went wrong...")


        class PCIBinder(ButtonBehavior,Image):
            """Combined Button and Image"""
            pass

        icons = ["Data/Resource/ico/icon.ttf","Data/Resource/ico/icon2.ttf","Data/fonts/ambrosia.ttf"]
        icon = {"foundation":icons[0],
                "fontawesome":icons[1]}
        
        fbres = u"\uf082"
        twres = u"\uf081"
        geres = u"\uf138"
        toppos = .94
        FButton = Button(text=fbres)
        FButton.font_name = icons[1]
        FButton.size_hint = .2,.2
        FButton.font_size = 75
        FButton.pos_hint = PinoyChat_XY(.1,toppos)
        FButton.background_color = 0,0,0,0
        FButton.on_release = shareOnFB

        TButton = Button(text=twres)
        TButton.font_name = icons[1]
        TButton.size_hint = .2,.2
        TButton.font_size = 75
        TButton.pos_hint = PinoyChat_XY(.26,toppos)
        TButton.background_color = 0,0,0,0
        TButton.on_release = followTwitter

        GButton = Button(text=geres)
        GButton.font_name = icons[1]
        GButton.size_hint = .13,.13
        GButton.font_size = 55
        GButton.pos_hint = PinoyChat_XY(.9,toppos)
        GButton.background_color = 0,0,0,0

        if len(username) > 3:
            GButton.on_release = OpenSide

        else:
            pass
        GButton.on_release = OpenSide

        logo = PCIBinder(source="Data/Resource/newlogo.png")
        logo.keep_ratio = False
        logo.allow_stretch = True
        logo.pos_hint = PinoyChat_XY(.5,.6)
        logo.size_hint = .63,.35
        logo.mipmap = True
        logo.on_press = OpenSide

        transTop = Button(background_normal="Data/Account/ttrans.png")
        transTop.background_down = transTop.background_normal
        transTop.size_hint = .98,.12
        transTop.pos_hint = PinoyChat_XY(.5,.77)
        transTop.opacity = .8

        transDown = Button()
        transDown = Button(background_normal="Data/Account/trans.png")
        transDown.background_down = transDown.background_normal
        transDown.size_hint = .8,.12
        transDown.pos_hint = PinoyChat_XY(.48,.38)
        transDown.opacity = .8

        ubg = u"\uf13e"
        UBGButton = Button(text="Username")
        UBGButton.font_name = icons[2]
        UBGButton.size_hint = .3,.09
        UBGButton.font_size = 29
        UBGButton.color = parse_color("#848484")
        UBGButton.pos_hint = PinoyChat_XY(.15,.32)
        UBGButton.background_color = 255,255,255,1

        PBGButton = Button(text="Password")
        PBGButton.font_name = icons[2]
        PBGButton.size_hint = .3,.09
        PBGButton.font_size = 29
        PBGButton.color = parse_color("#848484")
        PBGButton.pos_hint = PinoyChat_XY(.15,.25)
        PBGButton.background_color = 255,255,255,1

        IOBGButton = Button()
        IOBGButton = Button(background_normal="Data/Colours/24.jpg")
        IOBGButton.background_down = IOBGButton.background_normal
        IOBGButton.size_hint = 1,.163
        IOBGButton.pos_hint = PinoyChat_XY(.5,.286)
        #IOBGButton.opacity =

        UInput = TextInput()
        PInput = TextInput()


        UsrIco = Button(text=u"\uf007")
        UsrIco.font_name = icons[1]
        UsrIco.size_hint = .2,.2
        UsrIco.font_size = 35
        UsrIco.pos_hint = PinoyChat_XY(.36,.328)
        UsrIco.background_color = 0,0,0,0

        PIco = Button(text=u"\uf13e")
        PIco.font_name = icons[1]
        PIco.size_hint = .2,.2
        PIco.font_size = 35
        PIco.pos_hint = PinoyChat_XY(.36,.256)
        PIco.background_color = 0,0,0,0

        NAMLabel = Label(text="Not a Member Yet?")
        NAMLabel.font_name = icons[2]
        NAMLabel.pos_hint = PinoyChat_XY(.5,.15)
        NAMLabel.font_size = 25


        def Reg():
            pinoyChatClientApp().ScreenLogin(call=2)

        def Log(*args):
            # ret (
            global Loggedin, username
            Window.allow_vkeyboard = False
            Window.single_vkeyboard = True
            Window.docked_vkeyboard = False

            pinoyChatClientApp().ScreenLogin(sign=True,Username=UInput.text,Password=PInput.text)
            if Loggedin:
                pinoyChatClientApp().Warning("Welcome back "+str(username)+"!")
                rootEngine.current = 'screenChat'
                #pinoyChatClientApp().SidebarContents(username,logged=True)
                Restruct_Panel()
                self.to_rooms_Window()
            else:
                pass

            PInput.text = ""
            PInput.hint_text = ""
            UInput.focus = False
            PInput.focus = False

        cButton = Button(text="Create your Account!")
        cButton.font_name = icons[2]
        cButton.size_hint = .7,.07
        cButton.pos_hint = PinoyChat_XY(.5,.09)
        cButton.font_size = 25
        cButton.border = 1,4,1,4
        cButton.opacity = .7
        cButton.background_down = "Data/Colours/22.jpg"
        cButton.on_press = Reg


        CIcon = Button(text=u"\uf234")
        CIcon.font_name = icons[1]
        CIcon.size_hint = .13,.13
        CIcon.font_size = 24
        CIcon.pos_hint = PinoyChat_XY(.8,.09)
        CIcon.background_color = 0,0,0,0
        CIcon.opacity = .8

        def UText(*args):
            """Username Glue"""
            object,text = args # unpack

            if len(object.text) >= 4:
                UsrIco.color = parse_color("#1bbc9b")
            if len(object.text) <= 3:
                UsrIco.color = parse_color("#ffffff")

        def PText(*args):
            """Password Glue"""
            object,text = args # unpack

            if len(object.text) >= 4:
                PIco.color = parse_color("#1bbc9b")
            if len(object.text) <= 3:
                PIco.color = parse_color("#ffffff")

        def LogFocus(instance,value):
            if value:
                UInput.pos_hint = PinoyChat_XY(.69,.936) #i
                PInput.pos_hint = PinoyChat_XY(.69,.881) #i

                UBGButton.pos_hint = PinoyChat_XY(.15,.946) #b
                PBGButton.pos_hint = PinoyChat_XY(.15,.872) #b

                IOBGButton.pos_hint = PinoyChat_XY(.5,.91) #bg

                UsrIco.pos_hint = PinoyChat_XY(.36,.94) #ico
                PIco.pos_hint = PinoyChat_XY(.36,.886) #ico

                #// Some BGS etc.

            else:

                UInput.pos_hint = PinoyChat_XY(.69,.321)
                PInput.pos_hint = PinoyChat_XY(.69,.251)

                UBGButton.pos_hint = PinoyChat_XY(.15,.32) #b
                PBGButton.pos_hint = PinoyChat_XY(.15,.25) #b

                IOBGButton.pos_hint = PinoyChat_XY(.5,.286) #bg

                UsrIco.pos_hint = PinoyChat_XY(.36,.328) #ico
                PIco.pos_hint = PinoyChat_XY(.36,.256) #ico

        def UValidate(x):
            PInput.text = ""
            PInput.focus = True

        def PValidate(x):
            from kivy.utils import platform

            UInput.focus = False
            PInput.focus = False

            Log(x)

        UInput.size_hint = .55,.07
        UInput.pos_hint = PinoyChat_XY(.69,.321)
        UInput.background_color = 0,0,0,0
        UInput.hint_text = "username"
        UInput.font_size = '23sp'
        UInput.foreground_color = parse_color("#657572")
        UInput.bind(text=UText)
        UInput.multiline = False
        UInput.bind(on_text_validate=UValidate)
        UInput.bubble = False
        #UInput.bind(focus=LogFocus)

        PInput.size_hint = .55,.07
        PInput.hint_text = "password"
        PInput.pos_hint = PinoyChat_XY(.69,.251)
        PInput.background_color = 0,0,0,0
        PInput.font_size = '23sp'
        PInput.foreground_color = parse_color("#657572")
        PInput.password = True
        PInput.bind(text=PText)
        #PInput.bind(on_text_validate=Log)
        #PInput.focus = False
        #PInput.bind(on__enter=PValidate)
        #PInput.bind(focus=LogFocus)
        PInput.multiline = False
        PInput.bubble = False

        blog = Button()
        blog.font_name = "Data/Resource/ico/icon2.ttf"
        blog.text = u"\uf054"
        #blog.color = parse_color("#848484")
        blog.font_size = 20
        blog.background_normal = "Data/Colours/22.jpg"
        blog.background_down = "Data/IOData/send.png"
        blog.size_hint = .1,.159
        blog.opacity = .9
        blog.pos_hint = PinoyChat_XY(.96,0.286)  #{'center_x': .5, 'center_y': 0.08}
        blog.on_press = Log

        FLAYOUT.add_widget(BG)
        FLAYOUT.add_widget(transTop)
        FLAYOUT.add_widget(transDown)
        FLAYOUT.add_widget(logo)
        FLAYOUT.add_widget(FButton)
        FLAYOUT.add_widget(TButton)
        FLAYOUT.add_widget(GButton)
        FLAYOUT.add_widget(IOBGButton)
        FLAYOUT.add_widget(UBGButton)
        FLAYOUT.add_widget(PBGButton)
        FLAYOUT.add_widget(UsrIco)
        FLAYOUT.add_widget(PIco)
        FLAYOUT.add_widget(NAMLabel)
        FLAYOUT.add_widget(cButton)
        FLAYOUT.add_widget(CIcon)
        FLAYOUT.add_widget(UInput)
        FLAYOUT.add_widget(PInput)
        FLAYOUT.add_widget(blog)
        scr.add_widget(FLAYOUT)
        return scr



    def scr_chat(self):
        """
        Chat Screen Class
        """
        scr = Screen(name = 'screenChat')
        csecEngine = self.pinoyChat()
        scr.add_widget(csecEngine)
        return scr


    def scr_option(self):
        """
        Option Screen Class
        """
        scr = Screen(name = 'screenOpt')
        return scr

    def screenManagerx(self):
        """
        Main class for handling Screens
        """
        scr = ScreenManager()
        scr.transition = NoTransition(clearcolor=[0, 0, 0, 1])
        return scr


    def AllocateAdminObj(self,name):
        """
        Function for retrieving Admins (Dict type)
        """
        global DataServer

        dicts = DataServer + "Dict.pchat"

        get_dict = urllib.urlopen(dicts)
        get_dict_content = get_dict.read()


    def PCEmoticonEngine(self,Message=None):
        """
        Process and Parse Emoticons (If detected)
        """
        fontFile = "Data/fonts/font21.ttf"
        emoSize = "40"
        fontSyntax = ["[font={ffile}][size={es}]".format(ffile = fontFile,es=emoSize),"[/size][/font]"]

        Emoticons = {
            # PinoyChat Key Mappings
            ":)":"c",
            ":D":"b",
            "XD":"y",
            ":P":"x",
            ":*":"0",
            ":\")":"d",
            ";)":"d",
            "(spray)":"a",
            "(laugh)":"b",
            "(smile)":"c",
            "(blush)":"d",
            "(inlove)":"e",
            "(bomb)":"f",
            "(woot)":"g",
            "(meow)":"h",
            "(ninja)":"i",
            "(huh)":"j",
            "(mad)":"k",
            "(fire)":"l",
            "(sick)":"m",
            "(angry)":"n",
            "(boss)":"o",
            "(bleed)":"p",
            "(really)":"q",
            "(hurt)":"r",
            "(snob)":"s",
            "(cry)":"t",
            "(wow)":"u",
            "(hate)":"v",
            "(ows)":"w",
            "(dead)":"x",
            "(hihi)":"y",
            "(ilikeyou)":"z",

            # nums
            "(ego)":"1",
            "(sleep)":"2",
            "(greedy)":"3",
            "(adik)":"4",
            "(slash)":"5",
            "(brutal)":"6",
            "(dizzy)":"7",
            "(relax)":"8",
            "(devil)":"9",
            "(kiss)":"0",

            #symbs
            "(blind)":"[",
            "(stupid)":"]",
            "(fine)":"'",
            "(badboy)":";",
            }
        Which = Message

        if self.SanitizeMessage(Message) == None:
            Which = Message

        else:
            Which = self.SanitizeMessage(Message)

        LMemStore = mString = Which

        RepWhat = []

        InMessage = lambda s: True if Emoticons.has_key(s) else False

        for Maps in Emoticons.keys():
            # Capture all Registered Mappings
            if Maps in mString:
                RepWhat.append(Maps)

        Curr = LMemStore
        # IDK why Loops could'nt handle this one :(

        if RepWhat:
            if len(RepWhat) == 1:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1]))

            elif len(RepWhat) == 2:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1]))

            elif len(RepWhat) == 3:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1]))

            elif len(RepWhat) == 4:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1]))

            elif len(RepWhat) == 5:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1])) \
                    .replace(RepWhat[4],str(fontSyntax[0])+str(Emoticons[RepWhat[4]])+str(fontSyntax[1]))

            elif len(RepWhat) == 6:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1])) \
                    .replace(RepWhat[4],str(fontSyntax[0])+str(Emoticons[RepWhat[4]])+str(fontSyntax[1])).replace(RepWhat[5],str(fontSyntax[0])+str(Emoticons[RepWhat[5]])+str(fontSyntax[1]))


            elif len(RepWhat) == 7:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1])) \
                    .replace(RepWhat[4],str(fontSyntax[0])+str(Emoticons[RepWhat[4]])+str(fontSyntax[1])).replace(RepWhat[5],str(fontSyntax[0])+str(Emoticons[RepWhat[5]])+str(fontSyntax[1])) \
                    .replace(RepWhat[6],str(fontSyntax[0])+str(Emoticons[RepWhat[6]])+str(fontSyntax[1]))


            elif len(RepWhat) == 8:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1])) \
                    .replace(RepWhat[4],str(fontSyntax[0])+str(Emoticons[RepWhat[4]])+str(fontSyntax[1])).replace(RepWhat[5],str(fontSyntax[0])+str(Emoticons[RepWhat[5]])+str(fontSyntax[1])) \
                    .replace(RepWhat[6],str(fontSyntax[0])+str(Emoticons[RepWhat[6]])+str(fontSyntax[1])).replace(RepWhat[7],str(fontSyntax[0])+str(Emoticons[RepWhat[7]])+str(fontSyntax[1]))

            elif len(RepWhat) == 9:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1])) \
                    .replace(RepWhat[4],str(fontSyntax[0])+str(Emoticons[RepWhat[4]])+str(fontSyntax[1])).replace(RepWhat[5],str(fontSyntax[0])+str(Emoticons[RepWhat[5]])+str(fontSyntax[1])) \
                    .replace(RepWhat[6],str(fontSyntax[0])+str(Emoticons[RepWhat[6]])+str(fontSyntax[1])).replace(RepWhat[7],str(fontSyntax[0])+str(Emoticons[RepWhat[7]])+str(fontSyntax[1])) \
                    .replace(RepWhat[8],str(fontSyntax[0])+str(Emoticons[RepWhat[8]])+str(fontSyntax[1]))

            elif len(RepWhat) == 10:
                Curr = LMemStore.replace(RepWhat[0],str(fontSyntax[0])+str(Emoticons[RepWhat[0]])+str(fontSyntax[1])).replace(RepWhat[1],str(fontSyntax[0])+str(Emoticons[RepWhat[1]])+str(fontSyntax[1])) \
                    .replace(RepWhat[2],str(fontSyntax[0])+str(Emoticons[RepWhat[2]])+str(fontSyntax[1])).replace(RepWhat[3],str(fontSyntax[0])+str(Emoticons[RepWhat[3]])+str(fontSyntax[1])) \
                    .replace(RepWhat[4],str(fontSyntax[0])+str(Emoticons[RepWhat[4]])+str(fontSyntax[1])).replace(RepWhat[5],str(fontSyntax[0])+str(Emoticons[RepWhat[5]])+str(fontSyntax[1])) \
                    .replace(RepWhat[6],str(fontSyntax[0])+str(Emoticons[RepWhat[6]])+str(fontSyntax[1])).replace(RepWhat[7],str(fontSyntax[0])+str(Emoticons[RepWhat[7]])+str(fontSyntax[1])) \
                    .replace(RepWhat[8],str(fontSyntax[0])+str(Emoticons[RepWhat[8]])+str(fontSyntax[1])).replace(RepWhat[9],str(fontSyntax[0])+str(Emoticons[RepWhat[9]])+str(fontSyntax[1]))


            else:
                pass

        return Curr




    def AdminPanel(self,AorM):
        """
        Show Admin Login ModalView if 'pca'
        """
        global DataServer, username
        global AdminBool
        global ModBool

        adormod = AorM # a or m string

        def ValidateCred():
            """
            Validate the Credentials if True global A/M -> True
            """
            global username
            global AdminBool, ModBool

            xusername = uInput.text
            password = pInput.text

            if adormod == "a":
                inits = urllib.urlopen(DataServer+"a.pchat")
                read_init = inits.read()
                Dict_Object = eval(read_init)
                method_tester = dict(Dict_Object)
                us_good = 0
                ps_good = 0

            elif adormod == "m":
                inits = urllib.urlopen(DataServer+"m.pchat")
                read_init = inits.read()
                Dict_Object = eval(read_init)
                method_tester = dict(Dict_Object)
                us_good = 0
                ps_good = 0

            try:
                if adormod == "a":
                    if Dict_Object[xusername] and Dict_Object[xusername] == password:
                        AdminBool = True
                        ModBool = False
                        username = xusername
                        m.dismiss()


                elif adormod == "m":
                   if Dict_Object[xusername] and Dict_Object[xusername] == password:
                        ModBool = True
                        AdminBool = False
                        username = xusername
                        m.dismiss()


            except KeyError:
                Message.text = "Wrong Credentials.."



        m = ModalView(auto_dismiss=True)
        m.size_hint = .7,.5
        m.pos_hint = {"center_x":.5,
                      "center_y":.5}
        FL = FloatLayout()

        logo = Button(background_normal="Data/IOData/logo.png")
        logo.background_down = logo.background_normal
        logo.size_hint = .4,.29
        logo.pos_hint = {"center_x":.5,
                      "center_y":.7}
        logo.on_press = ValidateCred


        uInput = TextInput()
        uInput.size_hint = .9,.1
        uInput.pos_hint = {"center_x":.5,
                      "center_y":.4}

        uInput.hint_text = "Username: "

        pInput = TextInput()
        pInput.size_hint = .9,.1
        pInput.pos_hint = {"center_x":.5,
                      "center_y":.26}
        pInput.hint_text = "Passcode: "
        pInput.password = True

        Message = Label(text="Official's Panel")
        Message.pos_hint = {"center_x":.5,
                      "center_y":.1}

        FL.add_widget(uInput)
        FL.add_widget(pInput)
        FL.add_widget(Message)
        FL.add_widget(logo)
        m.add_widget(FL)
        m.open()


    def ChangeColorFunc(self,f):
        "A Function for Changing Colors"
        from kivy.uix.colorpicker import ColorPicker
        global Feed_Bold, Feed_Italic, Feed_Normal
        global Feed_UColor, Feed_MColor, Feed_Font

        function = f

        def on_color(instance,value):
            """
            On_Color Event
            """
            global Feed_Bold, Feed_Italic, Feed_Normal
            global Feed_UColor, Feed_MColor, Feed_Font
            chose = str(instance.hex_color) # picked color in Hex ex. #000000

            if function == "UserColor":
                Feed_UColor = chose

            elif function == "MessageColor":
                Feed_MColor  = chose

        m = ModalView(auto_dismiss=True)
        UILayOut = FloatLayout()

        l = Label(text="Tap Below to Close",color=parse_color("#424242"),font_size="23sp")
        l.pos_hint = {"center_x":.5,"center_y":.05}
        w = ColorPicker()
        w.pos_hint = {"center_x":.5,"center_y":.6}
        w.bind(color=on_color)

        bg = Button(background_normal="Data/optionsData/bg.jpg",background_down="Data/optionsData/bg.jpg")
        bg.pos_hint = {"center_x":.5,"center_y":.5}
        UILayOut.add_widget(bg)
        UILayOut.add_widget(w)
        UILayOut.add_widget(l)
        m.size_hint = 1,.8
        m.add_widget(UILayOut)
        m.open()

    def showOpt(self):
        """
        Show Options Modal (fN)
        """
        global CurrentChat,MessageStorage
        global username, sounds, sound_path,curr_h
        global Feed_Bold, Feed_Italic, Feed_Normal
        global Feed_UColor, Feed_MColor, Feed_Font, Feed_Size
        global AdminBool,ModBool, guest, initsend

        from kivy.uix.image import AsyncImage


        _priv_sound_bool = 1 #private sound boolean config


        Modal = ModalView()
        FL = FloatLayout()
        Modal.background = "Data/Colours/13.jpg"

        Accord = Accordion()
        #Accord.min_space = 58
        Accord.size_hint = 1,.9
        Accord.orientation = "vertical"

        # Items
        acctab_one = AccordionItem(title="Username",background_normal="Data/Colours/6.jpg",background_selected="Data/Colours/17.jpg")
        #acctab_one.collapse = True
        acctab_two = AccordionItem(title="Sounds",background_normal="Data/Colours/6.jpg",background_selected="Data/Colours/17.jpg")
        #acctab_two.collapse = False
        acctab_three = AccordionItem(title="Appearance",background_normal="Data/Colours/6.jpg",background_selected="Data/Colours/17.jpg")
        #acctab_three.collapse = False
        acctab_four = AccordionItem(title="Miscellaneous",background_normal="Data/Colours/6.jpg",background_selected="Data/Colours/17.jpg")
        #acctab_four.collapse = False
        # Items


        one_FL = FloatLayout()
        _usrbox = Label(size_hint=(.3,.2),
                            pos_hint={"center_x":.5,"center_y":.7})
        _usrbox.font_name = "Data/fonts/ambrosia.ttf"
        _usrbox.font_size = "29sp"
        _usrbox.color = parse_color("#424242")

        def call_reg():
            """
            Function to call and move to the place of Reg objects.
            """
            global username, guest
            if _creacc.text == "Join Us!": # reg condition
                Modal.dismiss()

                try:
                    self.ToScreen("Login")
                finally:
                    self.ScreenLogin(call=2)    # simple hack to access the inner class.

            elif _creacc.text == "Account Options": # logged-in already.
                if "pc-" in username:
                    self.Warning("Only available for Registered Users")
                    Modal.dismiss()
                else:
                    self.EditAccountObj()
                    Modal.dismiss()



        def config_name():
            """
            Function for Changing username
            - Log-out button now starts (2.0.0)
            """
            global AdminBool,ModBool, username, initsend
            global MessageStorage
            global CurrentChat
            global guest

            if _setuser.text == "Log Out":  # Logout
                Modal.dismiss()
                self.Warning(message="Don't forget to come back {n}!".format(n=username))
                username = ""
                AdminBool, ModBool = 0, 0

                PMCache = dict(MessageStorage)  # cache messages (PM)
                PMCache.clear()                 # remove contents

                CurrentChat = ""
                
                if guest:
                    guest = 0

                try:
                    self.Main.remove_widget(self.control)   # when admin control is on, turn off.

                except:
                    pass
                initsend = 0
                self.back_to_intro()


            elif _setuser.text == "Sign In!": # Sign In
                Modal.dismiss()

                try:
                    self.ToScreen("Login")
                finally:
                    self.ScreenLogin(call=1)    # simple hack to access the inner class.

        _setuser = Button(background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",
                          size_hint=(.4,.2),pos_hint={"center_x":.24,"center_y":.2})
        _setuser.on_release = config_name

        # Seperator
        _sep = Button(background_normal="Data/Account/trans.png",background_down="Data/Account/trans.png",
                          size_hint=(1,.23),pos_hint={"center_x":.5,"center_y":.34})

        _creacc = Button(background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",
                          size_hint=(.4,.2),pos_hint={"center_x":.75,"center_y":.2})

        _creacc.on_press = call_reg

        def GetPChatAvatar(username):
            """
            Function for getting avatar via DB (CoreEngine)
            """
            try:
                RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
                return RunGetter.Avatar

            except:
                pass
        _Avatar = AsyncImage(size_hint=(.34,.55), pos_hint={"center_x":.5,"center_y":.7})
        _Avatar.source = str(GetPChatAvatar(username))

        try:
            _Avatar.reload()
        except:
            pass

        _Avatar.allow_stretch = True
        _Avatar.keep_ratio = False


        if True:

            if len(username) == 0:  # Not Logged In
                try:
                    one_FL.remove_widget(_Avatar)   # try to remove if avatar exist
                except:
                    pass

                _usrbox.text = "Welcome to PinoyChat"
                _usrbox.color = parse_color("#424242")
                _setuser.text="Sign In!"
                _creacc.text = "Join Us!"


            if len(username) > 0:   # Logged In
                acctab_one.title = str(username)
                one_FL.add_widget(_Avatar)
                #_usrbox.text = str(username)
                _setuser.text = "Log Out"
                _creacc.text = "Account Options"


        one_FL.add_widget(_usrbox)
        one_FL.add_widget(_setuser)
        one_FL.add_widget(_creacc)
        one_FL.add_widget(_sep)

        two_FL = FloatLayout()
        _label_sounds = Label()
        _label_sounds.text = "Tone Configuration"
        _label_sounds.font_size = "25sp"
        _label_sounds.color = parse_color("#424242")
        _label_sounds.font_name = "Data/fonts/ambrosia.ttf"
        _label_sounds.pos_hint = {"center_x":.5,"center_y":.8}

        _label_psounds = Label()
        _label_psounds.text = "Tone: \"{path}\"".format(path=sound_path)
        _label_psounds.font_size = "18sp"
        _label_psounds.color = parse_color("#424242")
        #_label_psounds.font_name = "Data/fonts/ambrosia.ttf"
        _label_psounds.pos_hint = {"center_x":.5,"center_y":.2}

        def _swithFunc(instance,value):
            """
            Function for changing global (sounds) value
            """
            global sounds

            if value:
                sounds = 1
            else:
                sounds = 0

        _switch = Switch()

        if sounds == 1:
            _switch.active = True

        else:
            _switch.active = False

        _switch.bind(active=_swithFunc)
        _switch.pos_hint = {"center_x":.5,"center_y":.49}


        two_FL.add_widget(_switch)
        two_FL.add_widget(_label_sounds)
        two_FL.add_widget(_label_psounds)

        three_FL = BoxLayout(orientation="vertical",size_hint=(.7,1))


        def StopGuest(func):
            """
            This will Stop guest users on editing the colors
            """
            global guest

            def Check():
                if guest:
                    self.Warning("Only available for Registered Users")
                else:
                    return func()
            return Check

        @StopGuest
        def ChangeColorModalObj():
            """
            Change Color fN
            """
            global Feed_Bold, Feed_Italic, Feed_Normal
            global Feed_UColor, Feed_MColor, Feed_Font, Feed_Size
            global Feed_Color, Feed_Image, Feed_BackgroundOrColor

            Pops = ModalView(auto_dismiss=True)
            Pops.size_hint = .9,.5

            grid = GridLayout(cols=2,row=4,size_hint=(.9,.7))


            # { Fuctions
            def UserColorChangeObj():
                """
                Change Username Color
                """
                self.ChangeColorFunc("UserColor")

            def MessageColorChangeObj():
                """
                Change Mssage Color
                """
                self.ChangeColorFunc("MessageColor")

            def BoldChangeObj():
                """
                Set Bold style (E/Disabled)
                """
                global Feed_Bold
                if Feed_Bold:
                    Feed_Bold = 0
                    Bold_Button.text = "Disabled"
                else:
                    Feed_Bold = 1
                    Bold_Button.text = "Enabled"


            def ItalicChangeObj():
                """
                Set Italic style (E/Disabled)
                """
                global Feed_Italic
                if Feed_Italic:
                    Feed_Italic = 0
                    Italic_Button.text = "Disabled"
                else:
                    Feed_Italic = 1
                    Italic_Button.text = "Enabled"


            UserClr_Button = Button(text="User Color")
            UserClr_Button.background_color = parse_color(Feed_UColor)
            UserClr_Button.on_press = UserColorChangeObj


            MessageClr_Button = Button(text="Message Color")
            MessageClr_Button.background_color = parse_color(Feed_MColor)
            MessageClr_Button.on_press = MessageColorChangeObj

            Bold_Button = Button(text="Bold Switch")
            Bold_Button.background_color = parse_color("#013950")
            Bold_Button.on_press = BoldChangeObj
            Bold_Button.text = "Enabled" if Feed_Bold == "1" or Feed_Bold == 1 else "Disabled"

            Italic_Button = Button(text="Italic Swith")
            Italic_Button.background_color = parse_color("#013950")
            Italic_Button.on_press = ItalicChangeObj
            Italic_Button.text = "Enabled" if Feed_Italic == "1" or Feed_Italic == 1 else "Disabled"

            UserClr_Label = Label(text="Username Color")
            MessageClr_Label = Label(text="Message Color")
            Bold_Label = Label(text="Bold Message")
            Italic_Label = Label(text="Italic Message")

            grid.add_widget(UserClr_Label)
            grid.add_widget(UserClr_Button)

            grid.add_widget(MessageClr_Label)
            grid.add_widget(MessageClr_Button)

            grid.add_widget(Bold_Label)
            grid.add_widget(Bold_Button)

            grid.add_widget(Italic_Label)
            grid.add_widget(Italic_Button)
            Pops.add_widget(grid)
            Pops.open()

        def ChangeBGColorFunc():
            """
            Change Chat Background Color
            - 2.0.1 Color removed, BGs changed.
            """
            global Feed_Color, Feed_Image, Feed_BackgroundOrColor
            from kivy.uix.colorpicker import ColorPicker

            global on_color_init
            on_color_init = 0


            m = ModalView(auto_dismiss=True)
            UILayOut = FloatLayout()

            ChooseBG = Spinner(
                text='Default',
                # just for positioning in our example'
                size_hint=(.94, .15),
                #size=(100, 44),
                pos_hint={'center_x': .5, 'center_y': .16})
            ChooseBG.bold = True
            ChooseBG.background_normal = "Data/Colours/8.jpg"
            ChooseBG.background_down = ChooseBG.background_normal

            BGS = {
                "Default":"Data/Colours/26.png",
                "Leaf" :"Data/Colours/"+str(1)+".jpg",
                "Teal" :"Data/Colours/"+str(2)+".jpg",
                "Sky Blue" :"Data/Colours/"+str(3)+".jpg",
                "Blue Dark" :"Data/Colours/"+str(4)+".jpg",
                "Violet" :"Data/Colours/"+str(5)+".jpg",
                "Ash Blue" :"Data/Colours/"+str(6)+".jpg",
                "Green Lighter" :"Data/Colours/"+str(7)+".jpg",
                "Flat Teal" :"Data/Colours/"+str(8)+".jpg",
                "Flat Blue" :"Data/Colours/"+str(9)+".jpg",
                "Just Blue" :"Data/Colours/"+str(10)+".jpg",
                "Violet Sweet" :"Data/Colours/"+str(11)+".jpg",
                "Coal Blue" :"Data/Colours/"+str(12)+".jpg",
                "Yellow Light" :"Data/Colours/"+str(13)+".jpg",
                "Yellow Sweet" :"Data/Colours/"+str(14)+".jpg",
                "Orange Sweet" :"Data/Colours/"+str(15)+".jpg",
                "Orange Dark" :"Data/Colours/"+str(16)+".jpg",
                "Flat Orange" :"Data/Colours/"+str(17)+".jpg",
                "Lipstick Red" :"Data/Colours/"+str(18)+".jpg",
                "Red Sour" :"Data/Colours/"+str(19)+".jpg",
                "Red Dark" :"Data/Colours/"+str(20)+".jpg",
                "Smoke Brown" :"Data/Colours/"+str(21)+".jpg",
                "Clay Grey" :"Data/Colours/"+str(22)+".jpg",
                "Simply White" :"Data/Colours/"+str(23)+".jpg",
                "Ash White" :"Data/Colours/"+str(24)+".jpg",
                "Brown Dark" :"Data/Colours/"+str(25)+".jpg",

            }

            Images = []

            for BG in BGS.keys():
                Images.append(BG)

            def ShowView(spin,text):
                """
                Callbacks to reload the image View
                """
                global Feed_Image
                Spin, Text = spin,text
                BGView.source = BGS[Text]
                # Auto set to config
                Feed_Image = BGView.source
                self.cs_BG.background_normal = Feed_Image
                self.cs_BG.background_down = Feed_Image


            ChooseBG.values = Images
            ChooseBG.bind(text=ShowView)

            l = Label(text="Tap here to Close",color=parse_color("#ffffff"),font_size="23sp")
            l.size_hint = 1,.12
            l.background_normal = "Data/Colours/8.jpg"
            l.pos_hint = {"center_x":.5,"center_y":.01-0.1}

            BGView = Image(source=Feed_Image)
            BGView.pos_hint = PinoyChat_XY(.5,.6)
            BGView.size_hint = .98,.7

            def DefaultBGFunc():
                """
                Restore Default BG (Main PChat BG)
                """
                global Feed_Color, Feed_Image, Feed_BackgroundOrColor
                self.cs_BG.background_down = "Data/optionsData/bg.jpg"
                self.cs_BG.background_normal = self.cs_BG.background_down
                Feed_Image = self.cs_BG.background_down
                m.dismiss()


            bg = Button(background_normal="Data/Colours/13.jpg",background_down="Data/Colours/13.jpg")
            bg.pos_hint = {"center_x":.5,"center_y":.5}


            UILayOut.add_widget(bg)
            UILayOut.add_widget(l)
            UILayOut.add_widget(BGView)
            UILayOut.add_widget(ChooseBG)
            m.size_hint = 1,.6
            m.add_widget(UILayOut)
            m.open()

        def ChangeFontFunc():
            """
            Change Chat Fonts (only .ttf) fN
            """
            global Feed_Font

            from kivy.uix.slider import Slider
            global Feed_Size

            m = ModalView(auto_dismiss=True)

            UILayout = FloatLayout()
            UILayout.orientation = "vertical"

            PreviewString = ""

            if len(username) < 3:
                PreviewString = "PinoyChat is Heart." # Set this if Username is Empty

            else:
                PreviewString = username


            Preview = TextInput(text=PreviewString,
                            color=parse_color("#FF6600"),
                            pos_hint={"center_x":.5,"center_y":.7},
                            multiline=False,
                            size_hint=(1,.2))

            Preview.foreground_color = parse_color("#ffffff")
            Preview.background_color = parse_color("#333300")

            title = Button(text="Font Preview Below",
                           pos_hint={"center_x":.5,"center_y":.9},
                           size_hint=(1,.2))

            title.background_normal = "Data/optionsData/bbg.png"
            title.background_down = "Data/optionsData/bbg.png"

            title.color = parse_color("#ffffff")

            def UpdatePreview(instance,value):
                Preview.font_size = str(int(value)) + "sp"

            bg = Button(background_normal="Data/optionsData/bg.jpg",
                        background_down="Data/optionsData/bg.jpg",
                        pos_hint={"center_x":.5,"center_y":.5})

            s = Slider(orientation="horizontal",size_hint=(.8,.2),
                                   pos_hint={"center_x":.5,"center_y":.54},
                                   min=17,
                                   max=43,
                                   value=22)

            s.bind(value=UpdatePreview)

            def ChangeFontFunc(spinner, text):
                """
                Inner Function and called on Final
                """
                global Feed_Font

                FontPattern = str(text)
                FontPath = "Data/fonts/"
                FontName = FontPath+FontPattern.replace(" ","").replace("F","f")+".ttf" # FontFile path
                Preview.font_name = FontName

            ComboBox1 = Spinner(
                text='Fonts I',
                # available values
                values=('Font 1', 'Font 2', 'Font 3', 'Font 4', 'Font 5'),
                # just for positioning in our example
                size_hint=(.24, .17),
                #size=(100, 44),
                pos_hint={'center_x': .14, 'center_y': .4})

            ComboBox1.bind(text=ChangeFontFunc)

            ComboBox2 = Spinner(
                text='Fonts II',
                # available values
                values=('Font 6', 'Font 7', 'Font 8', 'Font 9', 'Font 10'),
                # just for positioning in our example
                size_hint=(.24, .17),
                #size=(100, 44),
                pos_hint={'center_x': .38, 'center_y': .4})
            ComboBox2.bind(text=ChangeFontFunc)

            ComboBox3 = Spinner(
                text='Fonts III',
                # available values
                values=('Font 11', 'Font 12', 'Font 13', 'Font 14', 'Font 15'),
                # just for positioning in our example
                size_hint=(.24, .17),
                #size=(100, 44),
                pos_hint={'center_x': .62, 'center_y': .4})
            ComboBox3.bind(text=ChangeFontFunc)

            ComboBox4 = Spinner(
                text='Fonts IV',
                # available values
                values=('Font 16', 'Font 17', 'Font 18', 'Font 19', 'Font 20','Font 21'),
                # just for positioning in our example
                size_hint=(.24, .17),
                #size=(100, 44),
                pos_hint={'center_x': .86, 'center_y': .4})
            ComboBox4.bind(text=ChangeFontFunc)

            def SaveFunc():
                """
                Save New Font Config
                """
                global Feed_Font
                self.EngineConnector.font_name = Preview.font_name
                Feed_Font = Preview.font_name
                m.dismiss()

            SaveButton = Button(text="Save Font",
                                background_normal="Data/optionsData/plank.png",
                                background_down="Data/optionsData/plank.png",
                                size_hint = (.7,.16),
                                color=parse_color("#333300"),
                                pos_hint={"center_x":.5,"center_y":.15})

            SaveButton.on_press = SaveFunc


            UILayout.add_widget(bg)
            UILayout.add_widget(title)
            UILayout.add_widget(Preview)
            UILayout.add_widget(s)
            UILayout.add_widget(ComboBox1)
            UILayout.add_widget(ComboBox2)
            UILayout.add_widget(ComboBox3)
            UILayout.add_widget(ComboBox4)
            UILayout.add_widget(SaveButton)
            m.size_hint = .9,.6
            m.add_widget(UILayout)
            m.open()

        def ChangeFontSizeFunc():
            """
            Function for Changing FontSize
            """
            from kivy.uix.slider import Slider
            global Feed_Size
            m = ModalView(auto_dismiss=True)
            UILayout = FloatLayout()
            l = Label(text=str(Feed_Size)+" Slide it!",pos_hint={"center_x":.6,"center_y":.5})
            preview = Label(color=parse_color("#FF9900"),text="A+",pos_hint={"center_x":.6,"center_y":.7})

            def UpdateLabel(instance,value):
                """
                Event Function + and - Font Size live Preview
                """
                global Feed_Size
                l.text = str(int(value)) + " Size in SP"
                Feed_Size = int(value) # Floating Point is ugly so Feed_Size is INT
                preview.font_size = int(value)
                self.EngineConnector.font_size = int(Feed_Size)

            s = Slider(orientation="vertical",size_hint=(.4,.8),
                       pos_hint={"center_x":.2,"center_y":.5},
                       min=17,
                       max=43,
                       value=23)

            s.bind(value=UpdateLabel)

            UILayout.add_widget(s)
            UILayout.add_widget(l)
            UILayout.add_widget(preview)
            m.size_hint = .1,.7
            m.add_widget(UILayout)
            m.open()

        font = "Data/fonts/font20" + ".ttf"
        ChangeFont = Button(text="Chat Fonts",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",)
        ChangeFont.bold = 1
        ChangeFont.background_color = parse_color("#FFFFCC")

        ChangeFont.on_release = ChangeFontFunc
        ChangeFont.size_hint_x = .8
        ChangeFont.pos_hint = {"center_x": .5}


        ChangeTextColor = Button(text="Messaging",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",)
        ChangeTextColor.bold = 1
        ChangeTextColor.background_color = parse_color("#FFFFCC")
        ChangeTextColor.on_release = ChangeColorModalObj
        ChangeTextColor.pos_hint = {"center_x": .5}
        ChangeTextColor.size_hint_x = .8

        ChangeFontSize = Button(text="Message Size",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",)
        ChangeFontSize.background_color = parse_color("#FFFFCC")
        ChangeFontSize.bold = 1
        ChangeFontSize.on_release = ChangeFontSizeFunc
        ChangeFontSize.size_hint_x = .8
        ChangeFontSize.pos_hint = {"center_x": .5}

        ChangeBackground = Button(text="Backgrounds",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",)
        ChangeBackground.background_color = parse_color("#FFFFCC")
        ChangeBackground.bold = 1
        ChangeBackground.size_hint_x = .8
        ChangeBackground.pos_hint = {"center_x": .5}
        ChangeBackground.on_release = ChangeBGColorFunc

        three_FL.add_widget(ChangeFont)
        three_FL.add_widget(ChangeTextColor)
        three_FL.add_widget(ChangeFontSize)
        three_FL.add_widget(ChangeBackground)

        four_FL = GridLayout(cols=2, spacing=1) # ROOMS OPTIONS


        def check_updates():
            """
            Update Checker fN
            """
            global curr_h
            url = "http://"+str(curr_h)+":8181/coreEngine/devel/latest"
            RVersion = str(urllib.urlopen(url).read()).replace(".","")
            LVersion = __version__.replace(".","")

            if int(RVersion) > int(LVersion):
                ntitle = "New version: "+str(urllib.urlopen(url).read())
                self.pinoyChat_exception(errmsg="New Version Detected!  \nPlease Update PinoyChat!",title="New Updates!")

            elif int(RVersion) <= int(LVersion):
                ctitle = "Version: "+str(__version__)
                self.pinoyChat_exception(errmsg="Your PinoyChat is Updated!",title=ctitle)


        def showDev():
            """
            Show Developers Info and Credits fN
            """
            m = ModalView(auto_dismiss=True)
            FL = FloatLayout()


            coresec = Label(text="TECHNOLOGIES")
            coresec.color = parse_color("#006699")
            coresec.font_size = "23sp"
            coresec.font_name = "Data/fonts/ambrosia.ttf"
            coresec.pos_hint = {"center_x":.5,"center_y":.8}

            coresec_info = Label(text="Python Programming Language")
            coresec_info.bold = True
            coresec_info.pos_hint = {"center_x":.5,"center_y":.7}

            coresec_desc = Label(text="Written and Built using C/Python")
            coresec_desc.pos_hint = {"center_x":.5,"center_y":.65}

            k = Label(text="Kivy Framework")
            k.bold = True
            k.pos_hint = {"center_x":.5,"center_y":.55}

            k_desc = Label(text="Powerful Python Framework")
            k_desc.pos_hint = {"center_x":.5,"center_y":.5}

            t_b = Label(text="Twisted and Flask")
            t_b.bold = True
            t_b.pos_hint = {"center_x":.5,"center_y":.42}

            t_b_desc = Label(text="Networking and Web")
            t_b_desc.pos_hint = {"center_x":.5,"center_y":.37}


            p = Label(text="PUBLISHER")
            p.color = parse_color("#006699")
            p.font_size = "23sp"
            p.font_name = "Data/fonts/ambrosia.ttf"
            p.pos_hint = {"center_x":.5,"center_y":.26}
# >>

            last = Label(text='Licensed under MIT License')
            last.pos_hint = {"center_x":.5,"center_y":.09}
            last.color = parse_color("#696969")

            plast = Label(text="\"Mabuhay Pilipinas!\"")
            plast.pos_hint = PinoyChat_XY(.5,.15)
            plast.color = parse_color("#696969")

            p_d = Label(text="Copyright 2014 "+u"\u00A9"+" Mark Pequeras")
            p_d.pos_hint = {"center_x":.5,"center_y":.25}

            p_d.color = parse_color("#696969")

            logo = Button()
            logo.background_normal = "Data/Resource/splashwhite.jpg"
            logo.background_down = logo.background_normal
            logo.pos_hint = {"center_x":.53,"center_y":.56}
            logo.size_hint = .6,.7

            orange_bg = Button()
            orange_bg.background_normal = "Data/Resource/plainbg.jpg"
            orange_bg.background_down = orange_bg.background_normal
            orange_bg.pos_hint = PinoyChat_XY(.5,.5)
            orange_bg.size_hint = 1,1

            FL.add_widget(orange_bg)
            FL.add_widget(logo)
            FL.add_widget(p_d)
            FL.add_widget(last)
            FL.add_widget(plast)

            m.add_widget(FL)
            m.size_hint = .8,.4
            m.open()

        def Technologies():
            m = ModalView()
            lay = FloatLayout()

            bg = Button()
            bg.pos_hint = PinoyChat_XY(.5,.5)
            bg.background_normal = "Data/Colours/23.jpg"
            bg.background_down = bg.background_normal

            fullstr = """
[b]PinoyChat: The Filipino Chatroom[sup]{vers}[/sup][/b]


[b]Published by[/b]
CoreSEC Software, CA

[b]Developers[/b]
Mark A. R. Pequeras
Antonio Birol
Maria Shiela Magistrado
Arel Jao Guina
Jay Paterno

[b]Information[/b]
Website: [i]pinoychat.coresec.ca[/i]
License: [i]MIT License (Open-Source)[/i]

[b]Contact Us[/b]
[i]Business: business@coresec.ca[/i]
[i]Information: info@coresec.ca[/i]
[i]Bug/Reports: report@coresec.ca[/i]


""".format(vers=__version__)

            inf = Label()
            inf.opacity = .9
            inf.color = parse_color("#585858")
            inf.text = fullstr
            inf.markup = True
            inf.size_hint = .9,.5
            inf.pos_hint = PinoyChat_XY(.5,.55)

            logo = Button()
            logo.pos_hint = PinoyChat_XY(.8,.08)
            logo.background_normal = "Data/profile.png"
            logo.size_hint = .4,.2
            logo.background_down = logo.background_normal


            lay.add_widget(bg)
            lay.add_widget(logo)
            lay.add_widget(inf)

            m.add_widget(lay)
            m.size_hint = .9,.9
            m.auto_dismiss = 1
            m.open()

        def showContacts():
            """
            Show Contacts Modal
            """
            m = Popup(auto_dismiss=True)
            FL = FloatLayout()

            bug_report = Label(text="Website")
            bug_report.color = parse_color("#FF9933")
            bug_report.font_size = "23sp"
            bug_report.font_name = "Data/fonts/ambrosia.ttf"
            bug_report.pos_hint = {"center_x":.5,"center_y":.8}

            bug_contact = Label(text="www.marp.me/pinoychat")
            bug_contact.pos_hint = {"center_x":.5,"center_y":.73}

            devel = Label(text="Development")
            devel.color = parse_color("#FF9933")
            devel.font_size = "23sp"
            devel.font_name = "Data/fonts/ambrosia.ttf"
            devel.pos_hint = {"center_x":.5,"center_y":.6}

            develReq =  Label(text="Offers & Suggestions")
            develReq.pos_hint = {"center_x":.5,"center_y":.53}

            develCon =  Label(text="info@coresec.ca")
            develCon.pos_hint = {"center_x":.5,"center_y":.47}

            developer = Label(text="Developer")
            developer.color = parse_color("#FF9933")
            developer.font_size = "23sp"
            developer.font_name = "Data/fonts/ambrosia.ttf"
            developer.pos_hint = {"center_x":.5,"center_y":.3}

            developerCon =  Label(text="Inquiries, Info & Business")
            developerCon.pos_hint = {"center_x":.5,"center_y":.23}

            developerE1 =  Label(text="E-mail: mark@marp.me")
            developerE1.pos_hint = {"center_x":.5,"center_y":.16}

            developerE2 =  Label(text="http://www.marp.me/")
            developerE2.pos_hint = {"center_x":.5,"center_y":.08}

            FL.add_widget(bug_report)
            FL.add_widget(bug_contact)
            FL.add_widget(devel)
            FL.add_widget(develReq)
            FL.add_widget(develCon)
            FL.add_widget(developer)
            FL.add_widget(developerCon)
            FL.add_widget(developerE1)
            FL.add_widget(developerE2)


            m.add_widget(FL)
            m.size_hint = .6,.6
            m.title = "Contact Us "
            m.open()


        lastsoon_label = Label()
        lastsoon_label.text = "Version "
        lastsoon_label.font_size = "25sp"
        lastsoon_label.color = parse_color("#424242")
        lastsoon_label.font_name = "Data/fonts/ambrosia.ttf"


        ver_label = Label()
        ver_label.text = __version__
        ver_label.bold = True
        ver_label.font_size = "25sp"
        ver_label.color = parse_color("#424242")
        ver_label.font_name = "Data/fonts/ambrosia.ttf"


        dev_label = Label()
        dev_label.text = "About"
        dev_label.font_size = "25sp"
        dev_label.color = parse_color("#424242")
        dev_label.font_name = "Data/fonts/ambrosia.ttf"

        dev_button = Button(text="View",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",)
        dev_button.on_press = Technologies



        up_label = Label()
        up_label.text = "Updates"
        up_label.font_size = "25sp"
        up_label.color = parse_color("#424242")
        up_label.font_name = "Data/fonts/ambrosia.ttf"
        up_button = Button(text="Check?",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg")
        up_button.on_press = check_updates



        con_label = Label()
        con_label.text = "Contacts"
        con_label.font_size = "25sp"
        con_label.color = parse_color("#424242")
        con_label.font_name = "Data/fonts/ambrosia.ttf"

        con_button = Button(text="View",background_normal="Data/Colours/2.jpg",background_down="Data/Colours/7.jpg",)
        con_button.on_press = showContacts



        four_FL.add_widget(lastsoon_label)
        four_FL.add_widget(ver_label)

        four_FL.add_widget(up_label)
        four_FL.add_widget(up_button)

        #four_FL.add_widget(con_label)
        #four_FL.add_widget(con_button)

        four_FL.add_widget(dev_label)
        four_FL.add_widget(dev_button)


        acctab_one.add_widget(one_FL)
        acctab_two.add_widget(two_FL)
        acctab_three.add_widget(three_FL)
        acctab_four.add_widget(four_FL)


        Accord.add_widget(acctab_one)
        Accord.add_widget(acctab_two)
        Accord.add_widget(acctab_four)
        Accord.add_widget(acctab_three)
        Modal.add_widget(Accord)

        Modal.size_hint = .9,.6
        Modal.auto_dismiss = True

        def _SaveOnDisk(instance):
            """ Options Saver """
            global Feed_Bold,Feed_Italic, Feed_UColor
            global Feed_Color, Feed_Image, Feed_BackgroundOrColor
            # Save Bold Option
            _boldVal = Feed_Bold

            # Save Italic Option
            _italicVal = Feed_Italic

            # Save Sounds Option
            _soundVal = sounds

            # Save Username Color Option
            _userColor = Feed_UColor

            # Save Message Color Option
            _msgColor = Feed_MColor

            # Save Message Size
            _messageSizeVal = Feed_Size

            _fontNameVal = ""
            # Save Font Name Option
            if "Data" not in Feed_Font:
                _fontNameVal = "NONE"

            else:
                _fontNameVal = Feed_Font

            # ImageorColor BG
            _imageOrcolor = Feed_BackgroundOrColor

            # Background Image
            _background_image = Feed_Image

            # Background Color
            _background_color = Feed_Color

            PinoyChat_SOVSet("PinoyChatCon",
                             "sound",
                             _soundVal)

            PinoyChat_SOVSet("PinoyChatCon",
                             "message_bold",
                             _boldVal)

            PinoyChat_SOVSet("PinoyChatCon",
                             "message_italic",
                             _italicVal)

            PinoyChat_SOVSet("PinoyChatCon",
                             "username_color",
                             _userColor)

            PinoyChat_SOVSet("PinoyChatCon",
                             "message_color",
                             _msgColor)

            PinoyChat_SOVSet("PinoyChatCon",
                             "message_size",
                             _messageSizeVal)

            PinoyChat_SOVSet("PinoyChatCon",
                             "message_font",
                             _fontNameVal)

            PinoyChat_SOVSet("PinoyChatCon",
                             "backgroundcolor_or",
                             _imageOrcolor)

            PinoyChat_SOVSet("PinoyChatCon",
                             "background_color",
                             _background_color)

            PinoyChat_SOVSet("PinoyChatCon",
                             "background_image",
                             _background_image)

            PinoyChat_Save(True)


        Modal.bind(on_dismiss = _SaveOnDisk) # SaveOnDisk Function will be called after the window Closed.
        Modal.open()


    def printerx(self,x):
        """
        Just like a warper func :)
        """
        chatWindow = "screenChat"
        if x == "1":
            self.toChat_Window()

    def changer(self):
        """
        call once printerx() func triggered.
        """
        screenManagerx = ObjectProperty()
        self.rootEngine.current = 'screenInt'

    def to_chat_Window(self):
        """
        change screen to screenChat
        """
        global username, ShowAds

        screenManagerx = ObjectProperty()

        self.cs_Send.text = "JOIN CHAT!"
        self.cs_Send.size_hint = 1,.16
        self.cs_Send.pos_hint = {'center_x': .5, 'center_y': 0.08}
        self.cs_msgFeeds.scroll_y = 0

        try:
            try:
                RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
                Av = RunGetter.Avatar
                Cache.remove('kv.image')
                Cache.remove('kv.texture')
                self.cs_Logo.source = Av

            finally:
                rootEngine.current = 'screenChat'
        except:
            Cache.remove('kv.image')
            Cache.remove('kv.texture')
            self.cs_Logo.source = "Data/Account/Avatars/guest.jpg"
            #self.Warning("Avatar Problem: Doest Exist..")
        #if DEVELOPMENT_MODE == False:
        #   self.show_pchat_ad() # show Ads on every enter if DEVMODE = 1




    def send_quit(self):
        """
        Send Quit packet to coreEngine
        """
        global username
        usr = username
        Mess = str(username)+" has Left the conversation..\n"
        self.send(str(Mess))


    def pinoyChat_exception(self,errmsg,title="Oooops"):
        """
        Exception Decorator @pinoyChat_exception
        """
        err_message = str(errmsg)
        popup = Popup(title=title, content=Label(text=err_message),auto_dismiss=True)
        popup.size_hint = .7,.3
        popup.open()


    def to_rooms_Window(self):
        """
        change screen to Rooms list
        """
        global username, Room, initsend

        Modality = ModalView() #MV
        Modality.size_hint = .8,.9
        Modality.background = "Data/Colours/23.jpg"
        TP = TabbedPanel()
        TP.do_default_tab = False
        TP.spacing = 30

        TP.size_hint = .8,.9
        TP.tab_pos = 'top_mid'

        TP_Others = TabbedPanelHeader()
        TP_Others.background_normal = "Data/Colours/14.jpg"
        TP_Others.background_down = "Data/Colours/16.jpg"
        TP_Others.text = "Others"


        Lay = BoxLayout()
        Lay.orientation = 'vertical'
        Lay.spacing = 5

        Lay_others = BoxLayout()
        Lay_others.orientation = 'vertical'
        Lay_others.spacing = 5

        TP_Others.content = Lay_others
        TP.default_tab_content = Lay
        TP.background = "Data/bg.jpg"


        TP_Phil = TabbedPanelHeader()
        TP_Phil.background_normal ="Data/Colours/15.jpg"
        TP_Phil.background_down = "Data/Colours/16.jpg"
        TP_Phil.text = "Main"
        TP_Phil.content = Lay

        TP.background_color = 0,1,1,0

        TP.add_widget(TP_Phil)
        TP.add_widget(TP_Others)


        bnormal = "Data/Colours/24.jpg"
        bdown =  "Data/Colours/24.jpg"


        def ph_room_one():
            """
            Manila Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0
            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:

                self.MessageGetFunc(type="Main",room="Manila")
                self.chat_overlay.text = ""+coreEngine.Core.mone
                reactor.connectTCP(str(curr_h), 4321, EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Manila"


        b1 = Button(text=coreEngine.Core.mone)
        b1.background_normal = str(bnormal)
        b1.background_down = str(bdown)
        b1.color = parse_color("#424242")
        b1.on_press = ph_room_one
        b1.bold = True

        def ph_room_two():
            """
            Ilocos Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Ilocos")
                #self.cs_msgFeeds.text = ""
                curr_port = 4322
                self.chat_overlay.text = ""+coreEngine.Core.mtwo
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Ilocos"

        b2 = Button(text=coreEngine.Core.mtwo)
        b2.background_down = str(bdown)
        b2.background_normal = str(bnormal)
        b2.on_press = ph_room_two
        b2.color = parse_color("#424242")
        b2.bold = True


        def ph_room_three():
            """
            Laguna Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Laguna")
                curr_port = 4323
                self.chat_overlay.text = ""+coreEngine.Core.mthree
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Laguna"



        b3 = Button(text=coreEngine.Core.mthree)
        b3.background_down = str(bdown)
        b3.background_normal = str(bnormal)
        b3.on_press = ph_room_three
        b3.color = parse_color("#424242")
        b3.bold = True


        def ph_room_four():
            """
            Lucena Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Quezon")
                curr_port = 4324
                self.chat_overlay.text = ""+coreEngine.Core.mfour
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Quezon"

        b4 = Button(text=coreEngine.Core.mfour)
        b4.background_normal = str(bnormal)
        b4.background_down = str(bdown)
        b4.on_press = ph_room_four
        b4.color = parse_color("#424242")
        b4.bold = True


        def ph_room_five():
            """
            Bicol Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Bicol")
                curr_port = 4325
                self.chat_overlay.text = ""+coreEngine.Core.mfive
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Bicol"

        b5 = Button(text=coreEngine.Core.mfive)
        b5.background_normal = str(bnormal)
        b5.background_down = str(bdown)
        b5.on_press = ph_room_five
        b5.color = parse_color("#424242")
        b5.bold = True


        def ph_room_six():
            """
            Cebu Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Cebu")
                curr_port = 4326
                self.chat_overlay.text = ""+coreEngine.Core.msix
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Cebu"

        b6 = Button(text=coreEngine.Core.msix)
        b6.background_normal = str(bnormal)
        b6.background_down = str(bdown)
        b6.on_press = ph_room_six
        b6.color = parse_color("#424242")
        b6.bold = True


        def ph_room_seven():
            """
            Bacolod Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Bacolod")
                curr_port = 4327
                self.chat_overlay.text = ""+coreEngine.Core.mseven
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Bacolod"

        b7 = Button(text=coreEngine.Core.mseven)
        b7.background_normal = str(bnormal)
        b7.background_down = str(bdown)
        b7.on_press = ph_room_seven
        b7.color = parse_color("#424242")
        b7.bold = True


        def ph_room_eight():
            """
            Tacloban Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Tacloban")
                curr_port = 4328
                self.chat_overlay.text = ""+coreEngine.Core.meight
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Tacloban"

        b8 = Button(text=coreEngine.Core.meight)
        b8.background_normal = str(bnormal)
        b8.background_down = str(bdown)
        b8.on_press = ph_room_eight
        b8.color = parse_color("#424242")
        b8.bold = True


        def ph_room_nine():
            """
            Mindanao Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Main",room="Mindanao")
                curr_port = 4329
                self.chat_overlay.text = ""+coreEngine.Core.mnine
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Mindanao"

        b9 = Button(text=coreEngine.Core.mnine)
        b9.background_normal = str(bnormal)
        b9.background_down = str(bdown)
        b9.on_press = ph_room_nine
        b9.color = parse_color("#424242")
        b9.bold = True


# Rooms of Others starts here

        def ot_room_one():
            """
            Teens Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Teens-Other")
                curr_port = 5321
                self.chat_overlay.text = ""+coreEngine.Core.oone
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Teens-Other"

        b1o = Button(text=coreEngine.Core.oone)
        b1o.background_normal = str(bnormal)
        b1o.background_down = str(bdown)
        b1o.on_press = ot_room_one
        b1o.color = parse_color("#424242")
        b1o.bold = True


        def ot_room_two():
            """
            Barkada Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Barkada-Other")
                curr_port = 5322
                self.chat_overlay.text = ""+coreEngine.Core.otwo
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Barkada-Other"

        b2o = Button(text=coreEngine.Core.otwo)
        b2o.background_down = str(bdown)
        b2o.background_normal = str(bnormal)
        b2o.on_press = ot_room_two
        b2o.color = parse_color("#424242")
        b2o.bold = True


        def ot_room_three():
            """
            Music Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Music-Other")
                curr_port = 5323
                self.chat_overlay.text = ""+coreEngine.Core.othree
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Music-Other"

        b3o = Button(text=coreEngine.Core.othree)
        b3o.background_down = str(bdown)
        b3o.background_normal = str(bnormal)
        b3o.on_press = ot_room_three
        b3o.color = parse_color("#424242")
        b3o.bold = True


        def ot_room_four():
            """
            Gamers Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Gamers-Other")
                curr_port = 5324
                self.chat_overlay.text = ""+coreEngine.Core.ofour
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Gamers-Other"

        b4o = Button(text=coreEngine.Core.ofour)
        b4o.background_normal = str(bnormal)
        b4o.background_down = str(bdown)
        b4o.on_press = ot_room_four
        b4o.color = parse_color("#424242")
        b4o.bold = True


        def ot_room_five():
            """
            Nerds Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Nerds-Other")
                curr_port = 5325
                self.chat_overlay.text = ""+coreEngine.Core.ofive
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Nerds-Other"

        b5o = Button(text=coreEngine.Core.ofive)
        b5o.background_normal = str(bnormal)
        b5o.background_down = str(bdown)
        b5o.on_press = ot_room_five
        b5o.color = parse_color("#424242")
        b5o.bold = True


        def ot_room_six():
            """
            Migchat Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Migchat-Other")
                curr_port = 5326
                self.chat_overlay.text = ""+coreEngine.Core.osix
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Migchat-Other"

        b6o = Button(text=coreEngine.Core.osix)
        b6o.background_normal = str(bnormal)
        b6o.background_down = str(bdown)
        b6o.on_press = ot_room_six
        b6o.color = parse_color("#424242")
        b6o.bold = True


        def ot_room_seven():
            """
            Lovers Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Lovers-Other")
                curr_port = 5327
                self.chat_overlay.text = ""+coreEngine.Core.oseven
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Lovers-Other"

        b7o = Button(text=coreEngine.Core.oseven)
        b7o.background_normal = str(bnormal)
        b7o.background_down = str(bdown)
        b7o.on_press = ot_room_seven
        b7o.color = parse_color("#424242")
        b7o.bold = True


        def ot_room_eight():
            """
            Students Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Students-Other")
                curr_port = 5328
                self.chat_overlay.text = ""+coreEngine.Core.oeight
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Students-Other"

        b8o = Button(text=coreEngine.Core.oeight)
        b8o.background_normal = str(bnormal)
        b8o.background_down = str(bdown)
        b8o.on_press = ot_room_eight
        b8o.color = parse_color("#424242")
        b8o.bold = True

        def ot_room_nine():
            """
            Overseas Room
            """
            global curr_h
            global username
            global Room
            global initsend
            initsend = 0

            self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.size_hint = 1,.16
            self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
            self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1


            if len(str(username)) == 0:
                self.ToScreen("Login")
                Modality.dismiss()

            else:
                self.MessageGetFunc(type="Others",room="Overseas-Other")
                curr_port = 5329
                self.chat_overlay.text = ""+coreEngine.Core.onine
                reactor.connectTCP(str(curr_h), int(curr_port), EchoFactory(self))
                self.to_chat_Window()
                Modality.dismiss()
                Room = "Overseas-Other"

        b9o = Button(text=coreEngine.Core.onine)
        b9o.background_normal = str(bnormal)
        b9o.background_down = str(bdown)
        b9o.on_press = ot_room_nine
        b9o.color = parse_color("#424242")
        b9o.bold = True



        info_var = Label()
        info_var.text = "Rooms Listed below are Location based"
        info_var.color = parse_color("#484848")
        info_var.font_name = "Data/fonts/ambrosia.ttf"

        info_var_two = Label()
        info_var_two.text = "Rooms Listed below are Miscellaneous"
        info_var_two.color = parse_color("#484848")
        info_var_two.font_name = "Data/fonts/ambrosia.ttf"

        Lay_others.add_widget(info_var_two)
        Lay_others.add_widget(b1o)
        Lay_others.add_widget(b2o)
        Lay_others.add_widget(b3o)
        Lay_others.add_widget(b4o)
        Lay_others.add_widget(b5o)
        Lay_others.add_widget(b6o)
        Lay_others.add_widget(b7o)
        Lay_others.add_widget(b8o)
        Lay_others.add_widget(b9o)

        Lay.add_widget(info_var)
        Lay.add_widget(b1)
        Lay.add_widget(b2)
        Lay.add_widget(b3)
        Lay.add_widget(b4)
        Lay.add_widget(b5)
        Lay.add_widget(b6)
        Lay.add_widget(b7)
        Lay.add_widget(b8)
        Lay.add_widget(b9)
        Modality.add_widget(TP)
        Modality.open()

    def showCredits(self):
        """
        Modal view at Intro (CREDITS)
        """
        mods = ModalView(auto_dismiss=True)
        lay = FloatLayout()

        mainbg = Button()
        mainbg.background_normal = "Data/bg.jpg"
        mainbg.background_down = "Data/bg.jpg"
        mainbg.size_hint = 1,1
        mainbg.pos_hint = {"center_x":.5,"center_y":.5}
        label_string = Label()
        label_string.text = "CoreSEC Softwares Inc.\n"
        label_string.font_size = 20
        label_string.pos_hint = {"center_x":.5,"center_y":.65}
        label_string.size_hint = .1,.1
        label_string.color = parse_color("#585858")
        strs = "CoreSEC "+u"\u00a9"+" 2014, All Rights Reserved.\n"
        label_strings = Label()
        label_strings.text = strs
        label_strings.font_size = 14
        label_strings.pos_hint = {"center_x":.5,"center_y":.62}
        label_strings.size_hint = .1,.1
        label_strings.color = parse_color("#585858")

        more_info = "Type the following for Help"
        more_i = Label()
        more_i.color = parse_color("#404040")
        more_i.pos_hint = {"center_x":.5,"center_y":.58}
        more_i.font_size = 13
        more_i.bold = True
        more_i.text = str(more_info)

        cmd_strings = """
"Private" to show help for Private Messaging

"AddRoom" to show help for adding New rooms

"ChangeColor" to show help for Changing Nicks

"Report=NAME" Report a username.

"IP=NAME" The Interternet Protocol of a user

        """
        command_list = Label()
        command_list.color = parse_color("#ffffff")
        command_list.pos_hint = {"center_x":.5,"center_y":.42}
        command_list.font_size = 12
        command_list.text = str(cmd_strings)



        mod_logo = Button(background_normal="Data/csec.png",
                               background_down="Data/csec.png")
        lay.add_widget(mainbg)
        lay.add_widget(mod_logo)
        lay.add_widget(label_string)
        lay.add_widget(label_strings)
        lay.add_widget(more_i)
        lay.add_widget(command_list)
        mod_logo.size_hint = .7,.4
        mod_logo.pos_hint = {'center_x':.5,'center_y':.8}
        mods.add_widget(lay)
        mods.size_hint = .7,.7
        mods.open()

    def add_panel_intro(self):
        "Adds the left panel, No User"
        pass

    def back_to_intro(self):
        """
        Change screen to screenChat (Intro)
        """
        global username
        global initsend
        global ShowAds
        global CurrentChat
        global MessageStorage
        global SmileysLimit

        initsend = 0
        self.ShowINIT = 1
        PMCache = dict(MessageStorage)
        PMCache.clear()

        CurrentChat = ""

        try:
            self.send("exit")
        except:
            pass

        screenManagerx = ObjectProperty()
        self.ToScreen("screenIntro")
        ShowAds = 1

        self.cs_Send.font_name = "Data/fonts/DroidSans.ttf"
        self.cs_Send.font_size = '19sp'
        self.cs_Send.size_hint = 1,.16
        self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}
        self.cs_Send.on_press = self.send_message   # fixed bug 2.0.1
        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        self.cs_Logo.source = ""
        SmileysLimit = 0

        # 2.0.2  {
        username = ""
        # }


        def remove_panel():
            try:
                self.SideBarLay.remove_widget(self.SideAvatar)
                self.SideBarLay.remove_widget(self.SideName)
                self.SideBarLay.remove_widget(self.SideEdit)
                self.SideBarLay.remove_widget(self.SideEditAv)
                self.SideBarLay.remove_widget(self.SideCameraICO)
                #self.SideBarLay.remove_widget(self.SideCameraLICO)
                self.SideBarLay.remove_widget(self.SideMessage)
                self.SideBarLay.remove_widget(self.SideOff)
                self.SideBarLay.remove_widget(self.SideOffICO)
            finally:
                self.SideBarLay.add_widget(self.Side_infon)
                self.SideBarLay.add_widget(self.Side_Feeds)
                self.SideBarLay.add_widget(self.Side_website)
                self.SideBarLay.add_widget(self.Side_Like)
                self.SideBarLay.add_widget(self.Side_rate)
                self.SideBarLay.add_widget(self.Side_download)
                self.SideBarLay.add_widget(self.Side_info)
                self.SideBarLay.add_widget(self.Side_infona)
                self.SideBarLay.add_widget(self.Side_sec)

        remove_panel()
        self.add_panel_intro()

        return False

    def porter(self,prt):
        """
        Port Changer function :D
        """
        global curr_port
        int_port = int(prt)
        curr_port = int_port
        return curr_port

    def play_PMChat(self):
        """
        Private Messaging Tone Player
        """
        global soundswav, sounds, sound_path
        player = SoundLoader.load("Data/Tone/pinoychat.wav")

        if sounds:
            player.play()

        else: pass

    def play_chat(self):
        """
        Main Chat Tone Player
        """
        global soundswav, sounds, sound_path
        player = SoundLoader.load("Data/Tone/ppinoychat.wav")

        if sounds:
            player.play()
        else: pass


    def show_chat_opt(self):
        modal = ModalView()

        modal.open()


    def newInbox(self):
        """
        Implementation for Notification
        """
        pass


    def default_msgbox(self):
        """
        Move msgbox to default pos.
        """
        self.cs_msgBox.pos_hint = {'center_x': 0.49, 'center_y': 0.09}


    def ask_for_username(self): # Last function for 1.3.1 moved to 1.3.2
        """
        Function for Asking for Username
        """
        global username,users_init
        mod = ModalView(auto_dismiss=True)
        FL = FloatLayout()

        try:
            self.send("list")

        except:
            self.pinoyChat_exception(errmsg="Server is on Maintenance,\nPlease come back later.",title="Server is Offline..")
            return False

        def saveUsername():
            """
            Save Username
            """
            global username
            global users_init
            strUsername = str(username_string.text)
            spaces_removed = strUsername.replace(" ","")
            final_username = spaces_removed.translate(None,"~!@#$%^&*,./:;'()_+|\"")
            var = 0
            _split = list(users_init.split())

            if strUsername == "pchatpanel" or strUsername == "pinoychatadmin" or strUsername == "pca":
                self.AdminPanel(AorM="a")
                mod.dismiss()
                return False

            if strUsername == "mod" or strUsername == "pinoychatmod" or strUsername == "pcm":
                self.AdminPanel(AorM="m")
                mod.dismiss()
                return False

            if len(final_username) > 10 or len(final_username) < 4: # Not Allowed,    Throw Exception!
                title.pos_hint = {"center_x":0.63,"center_y":0.77}
                title.color = parse_color("#FE642E")
                title.text = ""

                if len(final_username) > 10:
                    title.text = "Well, that's long!"

                if len(final_username) < 4:
                    title.text = "That's too short!"

            elif final_username.title() in _split:        # capture dupe
                title.pos_hint = {"center_x":0.6,"center_y":0.77}
                title.color = parse_color("#FE642E")
                title.text = "Nickname Exists!"


            else:
                    username = str(final_username)
                    mod.dismiss()
                    username_string.focus = False

        def on_enter(value):
            saveUsername()

        username_string = TextInput()
        username_string.hint_text = "Your nickname: "
        username_string.font_size = "24sp"
        username_string.size_hint = .8,.2
        username_string.multiline = False
        username_string.padding_y = "13sp"
        username_string.padding_x = "13sp"
        username_string.font_name = "Data/fonts/ambrosia.ttf"
        username_string.pos_hint = {"center_x":0.5,"center_y":0.54}
        username_string.bind(on_text_validate=on_enter)


        title = Label()
        title.text = "Start Chatting!"
        title.pos_hint = {"center_x":0.6,"center_y":0.77}
        title.font_size = "24sp"
        title.font_name = "Data/fonts/ambrosia.ttf"

        BGImplem = Button(background_normal="Data/IOData/bg.jpg")
        BGImplem.size_hint = 2,2
        BGImplem.pos_hint = PinoyChat_XY(.5,.5)
        save_username_button = Button(text="Use Username")
        save_username_button.color = parse_color("#424242")
        save_username_button.bold = True
        save_username_button.size_hint = .8,.3
        save_username_button.pos_hint = {"center_x":0.5,"center_y":0.24}


        save_username_button.background_normal = "Data/optionsData/plank.png"
        save_username_button.background_down = "Data/optionsData/plank.png"
        save_username_button.on_press = saveUsername

        usrimage = Button()
        usrimage.background_normal = "Data/introData/username.png"
        usrimage.background_down = "Data/introData/username.png"
        usrimage.size_hint = .25,.3
        usrimage.pos_hint = {"center_x":.22,"center_y":.8}


        FL.add_widget(title)
        FL.add_widget(username_string)
        FL.add_widget(save_username_button)
        FL.add_widget(usrimage)

        mod.add_widget(FL)
        mod.size_hint = .8,.4
        mod.pos_hint = {"center_x":.5,"center_y":.7}
        mod.open()

    def users_online(self):
        global username, users_init, oView,print_message_init

        """
        Modal view for showing online users
        """
        def GetUsersNow(instance):
            global print_message_init

            if not print_message_init:
                try:
                    rst.text = """
You are not in Chat.

Please consider to JOIN the room first, before checking our Active users

**TIP**: To refresh the active users simply Close and Reopen this page"""
                finally:
                    pass


        modal = ModalView(auto_dismiss=True)

        try:
            self.send("list") # PACKET ASKING FOR USERSLIST.

        except: # Disconnected due IDLE
            self.pinoyChat_exception(title="PinoyChat Network",errmsg="Network Error")
            self.back_to_intro()

        FL = FloatLayout()
        BG = Button(background_normal="Data/Colours/6.jpg")
        BG.background_down = BG.background_normal
        BG.pos_hint = PinoyChat_XY(.5,.5)

        title = Label()
        title.font_name = "Data/fonts/ambrosia.ttf"
        title.text = "Active Users"
        title.font_size = "24sp"
        title.pos_hint = {"center_x":.45,"center_y":.9}

        rst = RstDocument()
        rst.size_hint = .9,.8
        rst.pos_hint = {"center_x":.5,"center_y":.45}
        rst.text = print_message_init


        change_colr = Button()  # USERS ICON
        change_colr.background_normal = "Data/IOData/usr.png"
        change_colr.background_down = "Data/IOData/usr.png"
        change_colr.size_hint = .36,.18
        change_colr.pos_hint = {"center_x":.83, "center_y":.9}

        FL.add_widget(BG)
        FL.add_widget(rst)
        FL.add_widget(title)
        FL.add_widget(change_colr)
        modal.add_widget(FL)
        modal.size_hint = .7,.9
        modal.bind(on_open=GetUsersNow)
        modal.open()

    def UserPage(self,instance,value,msg=""):
        """
        Contains User Information and a PM tunnel.
        """
        global username, print_message_init
        from kivy.uix.image import Image,AsyncImage
        from Engine import User

        #If the user is a Guest "pc-" as pattern.
        if "pc-" in str(value):
            self.UserOptions(instance=None,value=value)
            return False

        Struct = User.Core().GetUsersInformation(u=value) # Packed String-Tuples
        EvalStruct = eval(Struct) # Evaluated String into Tuples.

        #If app is less than v. 2.0.0 (as Exception)
        try:
            EvalStruct[0]   # if the user has no username via remote db
        except:
            self.UserOptions(instance=None,value=value)
            return False


        # Otherwise the user has Account Registered.

        # Local Vars
        Username = str(EvalStruct[0])   # no
        Password = str(EvalStruct[1])   # no
        Avatar = str(EvalStruct[2])     # yes
        Gender = str(EvalStruct[3])     # yes
        Rank = str(EvalStruct[4])       # yes
        Age = str(EvalStruct[5])        # yes
        AboutMe = str(EvalStruct[6])    # yes
        Location = str(EvalStruct[7])   # yes
        Status = str(EvalStruct[8])     # On/Off
        Admin = str(EvalStruct[9])      # no
        Moderator = str(EvalStruct[10]) # no

        BG = Button()
        BG.background_normal = "Data/Colours/13.jpg"
        BG.background_down = BG.background_normal
        BG.pos_hint = PinoyChat_XY(.5,.5)

        UsrImgObj = str(EvalStruct[2])

        AvatView = AsyncImage()
        AvatView.source = UsrImgObj #Avatar

        try:
            AvatView.reload()
        except:
            pass

        AvatView.size_hint = .6,.3
        AvatView.pos_hint = PinoyChat_XY(.5,.7)
        AvatView.keep_ratio = 1
        AvatView.allow_stretch = True
        AvatView.keep_ratio = False

        NameView = Button(text=str(value))
        NameView.background_normal = "Data/Colours/12.jpg"
        NameView.background_down = NameView.background_normal
        NameView.size_hint = 1,.12
        NameView.pos_hint = PinoyChat_XY(.5,.94)

        Sep = Button()
        Sep.background_normal = "Data/Account/trans.png"
        Sep.background_down = Sep.background_normal
        Sep.pos_hint = PinoyChat_XY(.5,.5)
        Sep.size_hint = .9,.08

        LRank = Button()
        LRank.markup = True
        LRank.background_normal = "Data/Colours/17.jpg"
        LRank.background_down = LRank.background_normal
        LRank.text = "Rank"
        LRank.color = parse_color("#ffffff")
        #LRank.font_name = "Data/fonts/ambrosia.ttf"
        LRank.size_hint = .3,.07
        #LRank.font_size = "20sp"
        LRank.pos_hint = PinoyChat_XY(.2,.45)

        LRank1 = Button()
        LRank1.background_normal = "Data/Colours/24.jpg"
        LRank1.background_down = LRank1.background_normal
        LRank1.text = Rank
        LRank1.color = parse_color("#484848")
        #LRank.font_name = "Data/fonts/ambrosia.ttf"
        LRank1.size_hint = .6,.07
        #LRank.font_size = "20sp"
        LRank1.pos_hint = PinoyChat_XY(.6,.45)
        LRank1.markup = True

        LLoc = Button()
        LLoc.background_normal = "Data/Colours/24.jpg"
        LLoc.background_down = LLoc.background_normal
        LLoc.text = Location
        LLoc.color = parse_color("#484848")
        LLoc.size_hint = .6,.07
        LLoc.pos_hint = PinoyChat_XY(.6,.37) # -7

        LLoc1 = Button()
        LLoc1.markup = True
        LLoc1.background_normal = "Data/Colours/17.jpg"
        LLoc1.background_down = LLoc1.background_normal
        LLoc1.text = "Location"
        LLoc1.color = parse_color("#ffffff")
        #LRank.font_name = "Data/fonts/ambrosia.ttf"
        LLoc1.size_hint = .3,.07
        #LRank.font_size = "20sp"
        LLoc1.pos_hint = PinoyChat_XY(.2,.37)

        LGen = Button()
        LGen.background_normal = "Data/Colours/24.jpg"
        LGen.background_down = LGen.background_normal
        LGen.text = Gender
        LGen.color = parse_color("#484848")
        LGen.size_hint = .6,.07
        LGen.pos_hint = PinoyChat_XY(.6,.29) # -7

        LGen1 = Button()
        LGen1.background_normal = "Data/Colours/17.jpg"
        LGen1.background_down = LGen1.background_normal
        LGen1.text = "Gender"
        LGen1.color = parse_color("#ffffff")
        #LRank.font_name = "Data/fonts/ambrosia.ttf"
        LGen1.size_hint = .3,.07
        #LRank.font_size = "20sp"
        LGen1.pos_hint = PinoyChat_XY(.2,.29)

        LAge = Button()
        LAge.background_normal = "Data/Colours/24.jpg"
        LAge.background_down = LAge.background_normal
        LAge.text = Age
        LAge.color = parse_color("#484848")
        LAge.size_hint = .6,.07
        LAge.pos_hint = PinoyChat_XY(.6,.21) # -7

        LAge1 = Button()
        LAge1.background_normal = "Data/Colours/17.jpg"
        LAge1.background_down = LAge1.background_normal
        LAge1.text = "Age"
        LAge1.color = parse_color("#ffffff")
        #LRank.font_name = "Data/fonts/ambrosia.ttf"
        LAge1.size_hint = .3,.07
        #LRank.font_size = "20sp"
        LAge1.pos_hint = PinoyChat_XY(.2,.21)

        LAM = Button()
        LAM.markup = True
        LAM.background_normal = "Data/Colours/17.jpg"
        LAM.background_down = LAge1.background_normal
        LAM.text = "About Me"
        LAM.color = parse_color("#ffffff")
        #LRank.font_name = "Data/fonts/ambrosia.ttf"
        LAM.size_hint = .3,.14
        #LRank.font_size = "20sp"
        LAM.pos_hint = PinoyChat_XY(.2,.09)

        LAM1 = Button()
        LAM1.background_normal = "Data/Colours/24.jpg"
        LAM1.background_down = LAge.background_normal
        LAM1.color = parse_color("#475577")
        LAM1.size_hint = .55,.14
        LAM1.pos_hint = PinoyChat_XY(.623,.09) # -7

        Stats = Button()
        State = ["Offline","Online"]
        Stats.markup = True
        try:
            self.send("list")

        except: pass

        if value in print_message_init:
            Stats.background_normal = "Data/Colours/1.jpg"
            Stats.text = State[1]

        else:
            Stats.background_normal = "Data/Colours/19.jpg"
            Stats.text = State[0]

        Stats.background_down = Stats.background_normal

        Stats.color = parse_color("#ffffff")
        Stats.font_name = "Data/fonts/ambrosia.ttf"
        Stats.size_hint = .2,.05
        #LRank.font_size = "20sp"
        Stats.pos_hint = PinoyChat_XY(.7,.825)

        def CallPM():
            _m.dismiss()
            self.UserOptions(instance=instance,value=value)

        MessMe = Button()
        MessMe.background_normal = "Data/Colours/msg.png"
        MessMe.background_down = MessMe.background_normal
        MessMe.size_hint = .17,.1
        MessMe.pos_hint = PinoyChat_XY(.89,.94)
        MessMe.on_press = CallPM


        _m = ModalView()
        _m.size_hint = .9,.9
        _layout = FloatLayout()
        add = _layout
        # Widgets

        add.add_widget(BG)
        add.add_widget(AvatView)
        add.add_widget(NameView)
        add.add_widget(Sep)

        add.add_widget(LRank1)
        add.add_widget(LRank)

        add.add_widget(LLoc)
        add.add_widget(LLoc1)

        add.add_widget(LGen)
        add.add_widget(LGen1)

        add.add_widget(LAge)
        add.add_widget(LAge1)

        add.add_widget(LAM)
        add.add_widget(LAM1)

        add.add_widget(Stats)
        add.add_widget(MessMe)
        ScrollAM = ScrollableAM()
        ScrollAM.text = AboutMe
        ScrollAM.size_hint = .5,.12
        ScrollAM.ids.AM.color = parse_color("#484848")
        ScrollAM.pos_hint = PinoyChat_XY(.623,.09) # -7
        add.add_widget(ScrollAM)

        # Widgets
        _m.add_widget(_layout)
        _m.open()


    def UserOptions(self,instance,value,msg=""):
        """
        Added: 1.5.4
        Calls when NAME clicked.
        Changed to UserPage as parent function

        - 2.0.0 (Only shows if PM preferred.)
        """
        global Users_Storage
        global AvatarView
        global username
        global BoolUserOption
        global print_message_init
        global MessageStorage
        global CurrentChat

        from kivy.uix.image import AsyncImage

        UserNamex = value
        CurrentChat = UserNamex
        def GetPChatAvatar(username):
            """
            Function for getting avatar via DB (CoreEngine)
            """
            try:
                RunGetter = pinoyChatClientApp().AccountValuesLoader(username=username)
                return RunGetter.Avatar

            except:
                pass

        def on_modalClosed(instance):
            """
            Event
            """
            global BoolUserOption
            BoolUserOption = "Closed"


        def on_modalOpen(instance):
            """
            Event
            """
            global BoolUserOption
            BoolUserOption = "Opened"

        M = ModalView(size_hint=(.9,.5),auto_dismiss=False)
        M.pos_hint = {"center_x":.5,"center_y":.6}
        M.bind(on_dismiss=on_modalClosed) # When modal closing <
        M.bind(on_open=on_modalOpen)
        Fl = FloatLayout()

        _Title = Label(text=value)
        _Title.pos_hint={'center_x': .5, 'center_y': .93}
        _Title.font_size = "24sp"
        _Title.color = parse_color("#ffffff")

        def CloseAndSave():
            M.dismiss()

        icon = Button(background_normal="Data/IOData/close.png")
        icon.background_down = icon.background_normal
        icon.pos_hint={'center_x': .92, 'center_y': .93}
        icon.size_hint = .15,.15
        icon.on_release = CloseAndSave

        TBG = Button()
        TBG.background_normal = "Data/Colours/12.jpg"
        TBG.background_down = TBG.background_normal
        TBG.pos_hint={'center_x': .5, 'center_y': .93}
        TBG.size_hint=(1, .14)

        AvatarBack = Button()
        AvatarBack.background_normal = TBG.background_normal
        AvatarBack.background_down = TBG.background_normal
        AvatarBack.pos_hint = PinoyChat_XY(.15,.99+.07)
        AvatarBack.size_hint = .3,.3

        def OpenUserPage():
            M.dismiss()
            self.UserPage(instance="",value=value)
        RunGetter = None


        UsrImgObj = ""

        try:
            _RunGetter = pinoyChatClientApp().AccountValuesLoader(username=value)
            UsrImgObj = str(_RunGetter.Avatar)

        except:
            UsrImgObj = "Data/Account/Avatars/guest.jpg"

        AvatarView = AsyncImage()
        AvatarView.source = UsrImgObj #Avatar

        try:
            AvatarView.reload()
        except:
            pass

        AvatarView.pos_hint = PinoyChat_XY(.15,.99+.07)
        AvatarView.size_hint = .27,.27
        AvatarView.allow_stretch = True
        AvatarView.keep_ratio = False

        self.cs_msgPMFeeds = cs_msgFeeds = PM()
        self.cs_msgPMFeeds.pos_hint = {"center_x":.5,"center_y":.5}
        self.cs_msgPMFeeds.size_hint = .97,.7

        try:
            self.cs_msgPMFeeds.text = MessageStorage[_Title.text]
        except:
            self.cs_msgPMFeeds.text = msg


        def on_enter(value):
            """
            Message Processor for PM
            """
            global print_message_init
            global MessageStorage

            self.send("list")

            if _Title.text in print_message_init:
                if len(MBox.text) <= 1:
                    pass

                else:
                    try:
                        Message = str(MBox.text)
                        CurrentTo = value

                        StringDict = {"from":username,
                                      "to":UserNamex,
                                      "message":Message}

                        self.send("#pm"+str(StringDict))

                    finally:
                        # version 2.0.0 (sender msg is not actually taken from server instead it stores into list.)
                        toMemory = "[b][color=#505050]"+str(username)+":[/color][/b] [color=#606060]"+str(Message)+"[/color]"+"\n"

                        try:    # if exist append the string from client
                            MessageStorage[_Title.text] += toMemory

                        except: # if fails make a new dict.
                            MessageStorage[_Title.text] = toMemory

                        MBox.text = ""

            else:
                OffString = "Sorry, "+_Title.text + " is offline :(\n" # Shows when user is offline. (chat will be disabled.)
                if OffString in self.cs_msgPMFeeds.text:
                    pass

                else:
                    self.cs_msgPMFeeds.text += OffString
                    MBox.text = ""


        BG = Button(background_normal="Data/Colours/13.jpg")
        BG.background_down = BG.background_normal
        BG.pos_hint = {"center_x":.5,
                       "center_y":.5}

        MBox = TextInput(multiline=False)
        MBox.hint_text = "Type a Message for "+str(UserNamex)
        MBox.size_hint = .97,.13
        MBox.font_size = "20sp"
        MBox.background_color = parse_color("#484848")
        MBox.keyboard_suggestions = False
        MBox.use_bubble = False
        MBox.allow_copy = False
        MBox.foreground_color = parse_color("#ffffff")
        MBox.hint_text_color = parse_color("#E0E0E0")
        MBox.pos_hint = {"center_x":.5,"center_y":.07}
        MBox.bind(on_text_validate=on_enter)

        _SpinAction = Spinner(
            text="Message Histories",
            size_hint=(.85, None),
            size=(500, 44),
            pos_hint={'center_x': .5, 'center_y': .1-0.4})

        IterUsers = []  # as a memory storage for users.

        for Senders in MessageStorage:
            if Senders == username:
                pass
            else:
                IterUsers.append(Senders)


        MessageConnect = self.cs_msgPMFeeds
        def ProcessMessage(instance,value):
            """
            Process and connects message from its key.
            """
            user = value
            caller = instance
            PhotoView = AvatarView
            NameView = _Title
            MEditor = MBox
            MemoryMessageStorage = MessageStorage
            global CurrentChat

            try:
                Getter = pinoyChatClientApp().AccountValuesLoader(username=user)
                UsrImgObj = str(_RunGetter.Avatar)
                AvatarView.source = UsrImgObj
                AvatarView.reload() # reload avatar

            except:
                UsrImgObj = "Data/Account/Avatars/guest.jpg"

            NameView.text = user    # change name
            MessageConnect.text = MemoryMessageStorage[user]    # insert the msg
            MBox.hint_text = "Type a Message for "+str(user)         # rename the for message.
            CurrentChat = user  # global.





        _SpinAction.background_normal = "Data/Colours/8.jpg"
        _SpinAction.background_down = _SpinAction.background_normal
        _SpinAction.values = IterUsers #[Users for Users in MessageStorage]   #Usernames <~ linked into a Dictionaries.
        _SpinAction.bind(text=ProcessMessage)

        try:
            HistoriesObj = MessageStorage[_Title.text]
            MessageConnect.text = HistoriesObj
        except:
            # user has no History with the sender.
            pass

        Fl.add_widget(BG)
        Fl.add_widget(TBG)
        Fl.add_widget(_Title)
        Fl.add_widget(icon)
        Fl.add_widget(MBox)
        Fl.add_widget(self.cs_msgPMFeeds)
        Fl.add_widget(AvatarBack)
        Fl.add_widget(AvatarView)
        Fl.add_widget(_SpinAction)

        M.add_widget(Fl)

        if username == value:
            pass
        else:
            M.open()

    def StartPMObj(self,instance,value):
        global username
        Command = "@"+str(value)+" "
        Dict = """{'to':'UNIT','fromf':'FNIT','message':'MNIT'}""".replace("UNIT")



        self.cs_msgBox.text = Command

    def EmoticonsObj(self):
        """
        Opens up Emoticon Chooser and a function glued.
        """

        PCEmoticonsMappings = {
                    # PinoyChat Key Mappings
                    "(spray)":"a",
                    "(laugh)":"b",
                    "(smile)":"c",
                    "(blush)":"d",
                    "(inlove)":"e",
                    "(bomb)":"f",
                    "(woot)":"g",
                    "(meow)":"h",
                    "(ninja)":"i",
                    "(huh)":"j",
                    "(mad)":"k",
                    "(fire)":"l",
                    "(sick)":"m",
                    "(angry)":"n",
                    "(boss)":"o",
                    "(bleed)":"p",
                    "(really)":"q",
                    "(hurt)":"r",
                    "(snob)":"s",
                    "(cry)":"t",
                    "(wow)":"u",
                    "(hate)":"v",
                    "(ows)":"w",
                    "(dead)":"x",
                    "(hihi)":"y",
                    "(ilikeyou)":"z",

                    # nums
                    "(ego)":"1",
                    "(sleep)":"2",
                    "(greedy)":"3",
                    "(adik)":"4",
                    "(slash)":"5",
                    "(brutal)":"6",
                    "(dizzy)":"7",
                    "(relax)":"8",
                    "(devil)":"9",
                    "(kiss)":"0",

                    #symbs
                    "(blind)":"[",
                    "(stupid)":"]",
                    "(fine)":"'",
                    "(badboy)":";",
                    }

        fname = "Data/fonts/font21.ttf"
        fsize = 50
        fcolor = parse_color("#FFFFFF")
        m = ModalView(auto_dismiss=True)

        bg = Button()
        bg.background_normal = "Data/Colours/6.jpg"
        bg.background_down = bg.background_normal


        layout = GridLayout(cols=3, spacing=10, size_hint_y=None)

        layout.bind(minimum_height=layout.setter('height'))

        mappings = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0","[","]","'",";"]

        def ProcessSmileys(value):
            """
            Emoticon Mapper
            """
            smile = value.text
            smileStruct = PCEmoticonsMappings
            messageBox = self.cs_msgBox.text
            Parse = ''
            for syntax, sval in smileStruct.items():
                if sval == smile:
                    break

            #messageBox += Parse

        for i in mappings:
            btn = Button(text=str(i), size_hint_y=None, height=40)
            btn.font_name = fname
            btn.background_normal = bg.background_normal
            btn.font_size = fsize
            btn.color = fcolor
            btn.bind(on_release = ProcessSmileys)

            layout.add_widget(btn)

        root = ScrollView(size_hint=(.9,.9))
        root.pos_hint = PinoyChat_XY(.5,.5)
        root.add_widget(layout)

        self.Main.add_widget(root)

    def ShowDocs(self):
        """
        Full Docs of PinoyChat
        """
        global username

        M = Popup(auto_dismiss=True)
        fullHelp = """

Kumusta {pcusr}!
================

`So What is this?`_::

    This File contains all Frequently Asked Questions
    About PinoyChat and related.


`Short Information`_::

    PinoyChat is an Open-Source (Non-Commercial) Mobile
    and Desktop Application Maintained by CoreSEC Software,CA
    And This app runs on different Platforms
    (We are still working to make it work with Microwaves)


Let's Start
===============================


`Disconnect Problem?`_::

    If you are experiencing such Disconnect problem,
    and Tarsier is the only friend you are seeing.
    Please note: This Application doesn't work out well with a slow
    Wireless connection (3-4 Mbps is recommended)


`Privacy`_::

    Yes, Your informations (Account Credential) are on our database
    and Do not worry everything that comes in and out are all
    encrypted and safe.




        """.format(pcusr=username)
        Doc = RstDocument()
        Doc.text = fullHelp
        M.add_widget(Doc)
        M.title = "PinoyChat Helper"
        M.separator_color = parse_color("#003366")
        M.size_hint = 1,.7
        M.open()

    def PinoyMail(self):
        """
        Function for Parallel like messaging (histories)
        """
        global MessageStorage
        global username
        IterUsers = []  # as a memory storage for users.
        BG = Button()
        BG.pos_hint = PinoyChat_XY(.5,.5)
        BG.background_normal = "Data/Colours/7.jpg"
        BG.background_down = "Data/Colours/7.jpg"

        def ShowMessages(instance,val):
            m.dismiss()
            self.UserOptions(instance=instance,value=val)

        for Senders in MessageStorage:
            if Senders == username:
                pass
            else:
                IterUsers.append(Senders)

        MList = Spinner(
            text="Message Histories",
            size_hint=(1,1),
            size=(500, 44),
            pos_hint={'center_x': .5, 'center_y': 5})

        MList.background_normal = "Data/Colours/7.jpg"
        MList.background_down = "Data/Colours/7.jpg"
        MList.values = IterUsers
        MList.bind(text=ShowMessages)


        m = ModalView()
        m.size_hint = .8,.12
        m.add_widget(BG)
        m.add_widget(MList)

        m.open()

    def ShowEmoticonsMenu(self):
        """
        Function for Showing Emoticons on view.
        """
        global initsend

        if self.cs_msgBox.focus == True:
            #self.cs_msgBox.pos_hint = {'center_x': 0.48, 'center_y': 0.05}
            #self.cs_Send.pos_hint = {'center_x': 0.93, 'center_y': 0.05}

            #if self.cs_msgBox.focus:
            #    self.cs_msgBox.focus = False    # controls moving up and down.

            self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
            self.cs_Send.text = u"\uf077"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.background_normal = "Data/Colours/8.jpg"
            self.cs_Send.background_down = "Data/IOData/send.png"

            try:
                self.Main.add_widget(self.EView)
                self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
                self.cs_Send.font_size = '19sp'
                self.cs_Send.text = u"\uf077"


            except:
                self.Main.remove_widget(self.EView) #pass # emo views doesnt exist.
                self.cs_Send.font_size = "19sp"
                self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
                self.cs_Send.text = u"\uf078"
        else:
            self.cs_msgBox.focus = True

    def pinoyChat(self,emoticons="Off"):
        """
        Main Chat Class
        """
        from kivy.animation import AnimationTransition,Animation
        from kivy.uix.image import AsyncImage
        from kivy.uix.behaviors import ButtonBehavior
        global Room
        global username,guest
        global SmileysLimit

        SmileysLimit = 0 # Stops on 10

        PCEmoticonsMappings = {
                        # PinoyChat Key Mappings
                        "(spray)":"a",
                        "(laugh)":"b",
                        "(smile)":"c",
                        "(blush)":"d",
                        "(inlove)":"e",
                        "(bomb)":"f",
                        "(woot)":"g",
                        "(meow)":"h",
                        "(ninja)":"i",
                        "(huh)":"j",
                        "(mad)":"k",
                        "(fire)":"l",
                        "(sick)":"m",
                        "(angry)":"n",
                        "(boss)":"o",
                        "(bleed)":"p",
                        "(really)":"q",
                        "(hurt)":"r",
                        "(snob)":"s",
                        "(cry)":"t",
                        "(wow)":"u",
                        "(hate)":"v",
                        "(ows)":"w",
                        "(dead)":"x",
                        "(hihi)":"y",
                        "(ilikeyou)":"z",

                        # nums
                        "(ego)":"1",
                        "(sleep)":"2",
                        "(greedy)":"3",
                        "(adik)":"4",
                        "(slash)":"5",
                        "(brutal)":"6",
                        "(dizzy)":"7",
                        "(relax)":"8",
                        "(devil)":"9",
                        "(kiss)":"0",

                        #symbs
                        "(blind)":"[",
                        "(stupid)":"]",
                        "(fine)":"'",
                        "(badboy)":";",
                        }

        fname = "Data/fonts/font21.ttf"
        fsize = 50
        fcolor = parse_color("#FFFFFF")

        #bg.background_normal = "Data/Colours/25.jpg"
        #bg.background_down = bg.background_normal


        layouts = GridLayout(cols=5, spacing=0, size_hint_y=None)

        layouts.bind(minimum_height=layouts.setter('height'))

        # Registered keys and is equivalent into an Emoticon.
        mappings = ["a","b","c","d","e","f","g","h","i",
                    "j","k","l","m","n","o","p","q","r",
                    "s","t","u","v","w","x","y","z","1",
                    "2","3","4","5","6","7","8","9","0",
                    "[","]","'",";"]

        def ProcessSmileys(value):
            """
            Emoticon Mapper
            """
            global SmileysLimit

            smile = value.text
            smileStruct = PCEmoticonsMappings
            messageBox = self.cs_msgBox.text
            Parse = ''
            for syntax, sval in smileStruct.items():
                if sval == smile:
                    if SmileysLimit <= 9:
                        self.cs_msgBox.text += " "+str(syntax)+" "
                        SmileysLimit += 1 # add every emo to count
                        break



        for i in mappings:
            btn = Button(text=str(i), size_hint_y=None, height=60)
            btn.font_name = fname
            btn.background_normal = "Data/Colours/25.jpg"
            btn.font_size = fsize
            btn.color = fcolor
            btn.bind(on_release = ProcessSmileys)

            layouts.add_widget(btn)

        self.EView = ScrollView(size_hint=(1.05,.23))
        self.EView.pos_hint = PinoyChat_XY(.5,.61)
        self.EView.add_widget(layouts)


        cache_err_msg = "None"

        def exception_func():
            pass

        self.cs_msgBox = ObjectProperty()
        self.Main = FloatLayout()

        # {{ Start
        AccessAvatar = None

        self.cs_BG = Button()
        self.cs_BG.background_normal = "Data/IOData/bg.jpg"
        self.cs_BG.background_down = "Data/IOData/bg.jpg"

        try:
            RemGet = pinoyChatClientApp().AccountValuesLoader(username=username)

            UsrImgObj = str(RemGet.Avatar)
        except: pass

        def callEditLoader():
            global guest

            if guest:
                self.Warning("You're a Guest, Please register!")
            else:
                self.EditAccountObj()

        self.EditAccountTop = Button(text="Edit Profile")
        self.EditAccountTop.color = parse_color("#424242")
        self.EditAccountTop.background_normal = "Data/Colours/13.jpg"
        self.EditAccountTop.background_down = self.EditAccountTop.background_normal
        self.EditAccountTop.size_hint = .3,.05
        self.EditAccountTop.pos_hint = PinoyChat_XY(.85,0.903)
        self.EditAccountTop.on_press = callEditLoader

        def callCameraLoader():
            global guest

            if guest:
                self.Warning("You're a Guest, Please register!")
            else:
                self.InstantPChatAvatarLocal()


        self.EditAvatarTop = Button(text=u"\uF1CD")
        self.EditAvatarTop.font_name = "Data/fonts/font22.ttf"
        self.EditAvatarTop.font_size = 36
        self.EditAvatarTop.background_normal = "Data/Colours/6.jpg"
        self.EditAvatarTop.background_down = self.EditAvatarTop.background_normal
        self.EditAvatarTop.size_hint = .15,.08
        self.EditAvatarTop.pos_hint = PinoyChat_XY(.78,0.954)
        self.EditAvatarTop.on_press = callCameraLoader

        def callImageLoader():
            """
            Too lazy to use lambda..
            """
            global guest

            if guest:
                self.Warning("You're a Guest, Please register!")
            else:
                self.InstantPChatAvatarLocal(onlyLoad=True)

        self.EditAvatarTopTwo = Button(text=u"\uF195")
        self.EditAvatarTopTwo.font_name = "Data/fonts/font22.ttf"
        self.EditAvatarTopTwo.font_size = 36
        self.EditAvatarTopTwo.background_normal = "Data/Colours/6.jpg"
        self.EditAvatarTopTwo.background_down = self.EditAvatarTopTwo.background_normal
        self.EditAvatarTopTwo.size_hint = .15,.08
        self.EditAvatarTopTwo.pos_hint = PinoyChat_XY(.93,0.954)
        self.EditAvatarTopTwo.on_press = callImageLoader


        def ShowAccountShortcuts(remove=False):
            """
            Functions for Showing Shortcuts of User
            """
            if remove:
                try:
                    self.Main.remove_widget(self.EditAccountTop)
                    self.Main.remove_widget(self.EditAvatarTopTwo)
                    self.Main.remove_widget(self.EditAvatarTop)
                except:
                    pass

            else:
                # show these Widgets
                try:
                    self.Main.add_widget(self.EditAvatarTop)
                    self.Main.add_widget(self.EditAvatarTopTwo)
                    self.Main.add_widget(self.EditAccountTop)

                except:
                    pass


        def AvatarPress(Close=False):
            """
            Callbacks when user(owner) hit the pic.
            """
            left = 0.6
            right = 0.89
            GetX = self.cs_Logo.pos_hint['center_x']
            if Close:
                ShowAccountShortcuts(remove=True)
                self.cs_LogoBG.pos_hint = {'center_x': right, 'center_y': 0.936}
                self.cs_Logo.pos_hint = {'center_x': right, 'center_y': 0.936}

            elif GetX == right: # Avatar to Left
                #self.cs_Logo.reload()

                self.cs_LogoBG.pos_hint = {'center_x': left, 'center_y': 0.936}
                self.cs_Logo.pos_hint = {'center_x': left, 'center_y': 0.936}
                ShowAccountShortcuts()

            elif GetX == left: # Avatar to Right
                #self.cs_Logo.reload()

                ShowAccountShortcuts(remove=True)
                self.cs_LogoBG.pos_hint = {'center_x': right, 'center_y': 0.936}
                self.cs_Logo.pos_hint = {'center_x': right, 'center_y': 0.936}



        class ImageButton(ButtonBehavior, AsyncImage):
            pass

        self.cs_Logo = ImageButton()

        try:
            Cache.remove('kv.image')
            Cache.remove('kv.texture')
            self.cs_Logo.source = UsrImgObj #"Data/IOData/title.png"

        except:
            Cache.remove('kv.image')
            Cache.remove('kv.texture')
            self.cs_Logo.source = "Data/Account/Avatars/guest.png"


        #self.cs_Logo.background_normal = UsrImgObj #"Data/IOData/title.png"
        self.cs_Logo.size_hint = .19,.11
        self.cs_Logo.pos_hint = {'center_x': 0.89, 'center_y': 0.936}
        self.cs_Logo.allow_stretch = True
        self.cs_Logo.keep_ratio = False
        self.cs_Logo.on_press = AvatarPress

        self.cs_LogoBG = Button()
        self.cs_LogoBG.background_normal = "Data/Colours/6.jpg"
        self.cs_LogoBG.size_hint = .21,.116
        self.cs_LogoBG.pos_hint = {'center_x': 0.89, 'center_y': 0.936}


        self.cs_emo = Button()
        #self.cs_emo.background_down = "Data/IOData/shout.png"
        #self.cs_emo.background_normal ="Data/IOData/shout.png"
        self.cs_emo.font_name = "Data/fonts/font21.ttf"
        self.cs_emo.text = "y"
        self.cs_emo.font_size = 56
        self.cs_emo.size_hint = .12,.12
        self.cs_emo.pos_hint = {'center_x': 0.67, 'center_y': 0.7}

        self.cs_preview = Scatter()
        self.cs_preview.size_hint = .5,.5
        self.cs_preview_label = Label()
        self.cs_preview_label.font_name = "Data/fonts/ambrosia.ttf"
        self.cs_preview_label.size_hint = .1,.2
        self.cs_preview_label.text = ""
        self.cs_preview_label.color = parse_color("#848484")
        self.cs_preview_label.font_size = "16sp"


        self.cs_preview_bg = Button()
        self.cs_preview_bg.background_normal = "Data/IOData/bgtwo.jpg"
        self.cs_preview_bg.background_down = "Data/IOData/bgtwo.jpg"
        self.cs_preview_bg.size_hint = 1,.3
        self.cs_preview.add_widget(self.cs_preview_label)


        self.cs_back = Button() # Option Button
        self.cs_back.background_normal = "Data/IOData/close.png"
        self.cs_back.background_down = "Data/IOData/close.png"
        self.cs_back.size_hint = .20,.12
        self.cs_back.pos_hint = {'center_x': 0.13, 'center_y': 0.93}
        self.cs_back.on_press = self.back_to_intro


        self.MessageButton = Button()
        self.MessageButton.background_normal = "Data/IOData/notifications.png"
        self.MessageButton.background_down = self.MessageButton.background_normal
        self.MessageButton.size_hint = .22,.12
        self.MessageButton.pos_hint = PinoyChat_XY(.92,.6) #Y.77
        self.MessageButton.on_release = self.PinoyMail

        def move_top_msgBox():  #MOVETOP
            """
            move box to top
            """
            try:
                AvatarPress(Close=True)
            except:
                pass

            self.cs_msgBox.pos_hint = {'center_x': 0.48, 'center_y': 0.77}
            self.cs_Send.pos_hint = {'center_x': 0.92, 'center_y': 0.77}
            self.cs_msgBox.size_hint = .98,.1

            self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
            self.cs_Send.text = u"\uf078"
            self.cs_Send.font_size = 20
            self.cs_Send.background_normal = "Data/IOData/send.png"
            self.cs_Send.background_down = "Data/Colours/25.jpg"

            # entrancenoti {
            self.IOMessage.size_hint = .42,.09
            self.IOMessage.pos_hint = PinoyChat_XY(.78,.355)

            self.IOMBG.size_hint = .43,.1
            self.IOMBG.pos_hint = PinoyChat_XY(.78,.355)
            # entrancenotif }

            self.Seper.pos_hint = PinoyChat_XY(.5,.766)

            #try: pass
                #self.Main.add_widget(self.EView)
            #except:
            #    pass # root already exists

        def move_default_msgBox(): #MOVEDOWN
            """
            Move to default Axis
            """
            try:
                AvatarPress(Close=True)
            except:
                pass


            self.cs_msgBox.pos_hint = {'center_x': 0.48, 'center_y': 0.05}
            self.cs_Send.pos_hint = {'center_x': 0.93, 'center_y': 0.05}

            self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
            self.cs_Send.text = u"\uf054"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.background_normal = "Data/Colours/8.jpg"
            self.cs_Send.background_down = "Data/IOData/send.png"
            # entrancenoti {

            self.IOMessage.size_hint = .42,.09
            self.IOMessage.pos_hint = PinoyChat_XY(.78,.755)
            self.IOMBG.size_hint = .43,.1
            self.IOMBG.pos_hint = PinoyChat_XY(.78,.755)

            self.Seper.pos_hint = PinoyChat_XY(.5,.054)
            # entrancenotif }

            try:
                self.Main.remove_widget(self.EView)
            except: pass # emo views doesnt exist.

        def on_focus(instance, value):
            """
            on focus event
            """
            #self.cs_msgFeeds.text = "123123123123123123123123123123123123123123123123123123123123\n"*32
            #if value:
            move_top_msgBox()     # move top
            #else:
            #    move_default_msgBox() # move below



        def on_text(instance,value):
            """
            Redirect text to preview label function
            """
            self.cs_preview_label.text = value

        def on_enter(value):
            """
            Call send_message() when Return
            """
            start_send()

        self.cs_msgBox = TextInput()
        self.cs_msgBox.size_hint = .97,.1
        self.cs_msgBox.allow_copy = False
        self.cs_msgBox.multiline = False # changed in 1.5.2
        self.cs_msgBox.font_size = "23sp"
        self.cs_msgBox.hint_text = "Enter your Message"
        self.cs_msgBox.hint_text_color = [0,0,0,0.3]
        self.cs_msgBox.background_color = [1,1,1,1]
        self.cs_msgBox.background_normal = "Data/IOData/mbox.jpg"
        self.cs_msgBox.background_active = "Data/IOData/mbox.jpg"
        self.cs_msgBox.padding = [15,15,60,6]
        self.cs_msgBox.pos_hint = {'center_x': 0.48, 'center_y': 0.05}
        self.cs_msgBox.use_bubble = False
        self.cs_msgBox.use_handles = True
        self.cs_msgBox.keyboard_suggestions = False
        self.cs_msgBox.bind(on_text_validate=on_enter)
        #self.cs_msgBox.bind(focus=on_focus)

        def FloatingBarCallback():
            "Modal for showing Social links"
            def shareOnFB():
                """
                Share on Facebook function
                """
                #_url_share = """https://m.facebook.com/dialog/feed?app_id=891081214237643&caption=PinoyChat is the New Filipino Application for Chatting within group (Chatrooms), Download #PinoyChat Now! Available at Google Playtore and Apple Appstore&link=https://play.google.com/store/apps/details?id=org.coresec.pinoychat&redirect_uri=https://play.google.com/store/apps/details?id=org.coresec.pinoychat """
                _url_share = "https://m.facebook.com/pinoychatapp"
                try:
                    webbrowser.open(_url_share)
                except:
                    self.pinoyChat_exception(errmsg="Sorry, something went wrong...")

            self.Main.remove_widget(self.FloatingBar)
            shareOnFB()

        self.FloatingBar = Button()
        self.FloatingBar.background_normal = "Data/IOData/bar.png"
        self.FloatingBar.background_down = self.FloatingBar.background_normal
        self.FloatingBar.pos_hint = PinoyChat_XY(.9,.4)
        self.FloatingBar.size_hint = .3,.3
        self.FloatingBar.on_press = FloatingBarCallback

        self.cs_msgFeeds = ScrollableLabel()
        self.cs_msgFeeds.size_hint = .99,.835
        self.cs_msgFeeds.pos_hint = {'center_x': 0.5, 'center_y': 0.525} # Default POS
        self.cs_msgFeeds.do_scroll_y = True
        self.cs_msgFeeds.scroll_y = 0


        self.EngineConnector = self.cs_msgFeeds.ids.PCHATFEEDENGINE # connected to kv lang with the id PCHAT...
        self.EngineConnector.font_size = int(PinoyChat_SOGet("PinoyChatCon","message_size"))

        if str(PinoyChat_SOGet("PinoyChatCon","message_font")) != "NONE":
            self.EngineConnector.font_name = str(PinoyChat_SOGet("PinoyChatCon","message_font"))

        self.EngineConnector.markup = True
        self.EngineConnector.color = parse_color("#2e2e2e")
        self.EngineConnector.bind(on_ref_press=self.UserPage)

        def start_send():
            """
            Start Send with Timer (Not Yet)
            """
            global SmileysLimit
            print "IN SEND!!!"
            try:
                self.Main.remove_widget(self.FloatingBar)

            except:
                pass

            SmileysLimit = 0

            self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
            self.cs_Send.text = u"\uf054"
            self.cs_Send.font_size = '19sp'
            self.cs_Send.background_normal = "Data/Colours/8.jpg"
            self.cs_Send.background_down = "Data/IOData/send.png"

            self.send_message()

        self.ShowINIT = 0

        self.cs_Send = Button() # Send Button
        self.cs_Send.background_normal = "Data/Colours/25.jpg"
        self.cs_Send.background_down = "Data/IOData/send.png"
        #self.cs_Send.text = "SEND"
        #self.cs_Send.color = parse_color("#424242")
        self.cs_Send.on_press = start_send

        self.cs_Send.text = "JOIN CHAT!"
        self.cs_Send.size_hint = 1,.16
        self.cs_Send.pos_hint = PinoyChat_XY(.5,0.08)  #{'center_x': .5, 'center_y': 0.08}



        self.chat_overlay = Label()
        self.chat_overlay.text = "Manila Room!"
        self.chat_overlay.color = parse_color("#E8E8E8")
        self.chat_overlay.font_size = "17sp"
        self.chat_overlay.font_name = "Data/fonts/ambrosia.ttf"
        self.chat_overlay.bold = True


        if "Manila" in self.chat_overlay.text:
            Room = "Manila"
            self.MessageGetFunc(type="Main",room="Manila") # creates bug for 2.0.2

        self.chat_obg = Button()
        self.chat_obg.background_normal = "Data/Colours/2.jpg"
        self.chat_obg.background_down = "Data/Colours/2.jpg"
        self.chat_obg.size_hint = 1,.06
        self.chat_obg.pos_hint = {"center_x":.5, "center_y":.97}

        self.change_colr = Button(text=u"\uF1A6")  # USERS ICON despite the name.
        #self.change_colr.background_normal = "Data/IOData/usr.png"
        self.change_colr.font_name = "Data/fonts/font22.ttf"
        self.change_colr.font_size = 48
        self.change_colr.background_color = 0,0,0,0
        self.change_colr.color = parse_color("#404040")
        #self.change_colr.background_down = "Data/IOData/usr.png"
        self.change_colr.size_hint = .17,.10
        self.change_colr.pos_hint = {"center_x":.82, "center_y":.97}
        self.change_colr.on_press = self.users_online

        def ShowInbox():
            """
            ShowInbox Function
            """
            global username
            self.UserOptions(instance="",value=username)

        self.Inbox = Button()
        self.Inbox.background_normal = "Data/messageData/mail.png"
        self.Inbox.background_down = "Data/messageData/mail.png"
        self.Inbox.size_hint = .17,.09
        self.Inbox.pos_hint = {"center_x":.7, "center_y":.97}
        self.Inbox.on_press = ShowInbox


        self.setinterface = Button(text=u"\uF214")
        #self.setinterface.background_down = "Data/IOData/set.png"
        #self.setinterface.background_normal = "Data/IOData/set.png"
        self.setinterface.size_hint = .09,.04
        self.setinterface.pos_hint = {"center_x":.93, "center_y":.97}
        self.setinterface.font_name = "Data/fonts/font22.ttf"
        self.setinterface.font_size = 36
        self.setinterface.color = parse_color("#404040")
        self.setinterface.background_color = 0,0,0,0
        self.setinterface.on_press = self.showOpt

        self.showrooms = Button(text=u"\uF103")
        #self.setinterface.background_down = "Data/IOData/set.png"
        #self.setinterface.background_normal = "Data/IOData/set.png"
        self.showrooms.size_hint = .09,.04
        self.showrooms.color = parse_color("#404040")
        self.showrooms.pos_hint = {"center_x":.72, "center_y":.97}
        self.showrooms.font_name = "Data/fonts/font22.ttf"
        self.showrooms.font_size = 36
        self.showrooms.background_color = 0,0,0,0
        self.showrooms.on_press = self.to_rooms_Window

        def open_pane():
            self.Sidebar.toggle_state()

        self.showpane = Button(text=u"\uF203")
        #self.setinterface.background_down = "Data/IOData/set.png"
        #self.setinterface.background_normal = "Data/IOData/set.png"
        self.showpane.size_hint = .09,.04
        self.showpane.color = parse_color("#404040")
        self.showpane.pos_hint = {"center_x":.06, "center_y":.97}
        self.showpane.font_name = "Data/fonts/font22.ttf"
        self.showpane.font_size = 36
        self.showpane.background_color = 0,0,0,0
        self.showpane.on_press = open_pane
        self.chat_overlay.pos_hint = {"center_x":.3, "center_y":.97}



        self.showinf = Button(text=u"\uF15E")
        #self.setinterface.background_down = "Data/IOData/set.png"
        #self.setinterface.background_normal = "Data/IOData/set.png"
        self.showinf.size_hint = .09,.04
        self.showinf.pos_hint = {"center_x":.63, "center_y":.97}
        self.showinf.font_name = "Data/fonts/font22.ttf"
        self.showinf.font_size = 36
        self.showinf.background_color = 0,0,0,0
        self.showinf.on_press = self.ShowDocs

        self.IOMessage = PCMIO()
        self.IOMessage.text = " Welcome "+username+"!\n"
        self.IOMessage.size_hint = .42,.09
        self.IOMessage.pos_hint = PinoyChat_XY(.78,.755)
        self.IOMessage.ids.PCIO.color = parse_color("#ffffff")
        self.IOMessage.font_size = "10sp"


        self.IOMBG = Button()
        self.IOMBG.background_normal = "Data/Colours/99.png"
        self.IOMBG.background_down = self.IOMBG.background_normal
        self.IOMBG.size_hint = .43,.1
        self.IOMBG.pos_hint = PinoyChat_XY(.78,.755)
        self.IOMBG.opacity = .6

        def showPanel():
            """
            Auto Load Admin Panel
            """
            #Controllers
            global guest

            if guest:
                return False

            else:
                if ModBool: # Moderator
                    self.Administration(rank="Moderator")

                if AdminBool:   # Administrator
                    self.Administration(rank="Administrator")

        self.control = Button(text=u"\uF1B3")
        #self.setinterface.background_down = "Data/IOData/set.png"
        #self.setinterface.background_normal = "Data/IOData/set.png"
        self.control.size_hint = .09,.04
        self.control.pos_hint = {"center_x":.63, "center_y":.97}
        self.control.font_name = "Data/fonts/font22.ttf"
        self.control.font_size = 36
        self.control.background_color = 0,0,0,0
        self.control.on_press = showPanel


        self.Seper = Button()
        self.Seper.size_hint = 1.1,.1
        self.Seper.pos_hint = PinoyChat_XY(.5,.054)
        self.Seper.background_normal = "Data/Colours/25.jpg"
        self.Seper.background_down = self.Seper.background_normal

        self.Main.add_widget(self.cs_BG)
        #self.Main.add_widget(self.cs_LogoBG)
        #self.Main.add_widget(self.cs_Logo)
        #self.Main.add_widget(self.cs_back)
        self.Main.add_widget(self.cs_msgFeeds)
        self.Main.add_widget(self.Seper)
        self.Main.add_widget(self.MessageButton)
        self.Main.add_widget(self.cs_msgBox)
        self.Main.add_widget(self.cs_Send)
        self.Main.add_widget(self.chat_obg)
        self.Main.add_widget(self.change_colr)
        self.Main.add_widget(self.setinterface)
        self.Main.add_widget(self.chat_overlay)
        self.Main.add_widget(self.showpane)
        self.Main.add_widget(self.showrooms)
        #self.Main.add_widget(self.showinf)
        #self.Main.add_widget(self.cs_emo)
        self.Main.add_widget(self.IOMBG) # emobg
        self.Main.add_widget(self.IOMessage)   #emolist

        #self.Main.add_widget(self.FloatingBar)



        return self.Main


    def connect_to_server(self):
        """
        Connect to Server (4321 is Manila Room)
        """
        global curr_h
        try:
            reactor.connectTCP(curr_h, 4321, EchoFactory(self))
        except:
            self.Warning("Server is currently Down...")




    def on_connection(self, connection, send_function):
        """
        When Connection starts ()
        """
        #self.print_message("connected succesfully!")
        self.connection = connection
        self.send = send_function
        print "Connected to Server! on_connection(self)):"


    def CommandEngineObject(self,command):
        """Command Engine Processor function."""
        _command = command
        commands = [
            "#whoami",            # 0
            "#changename",        # 1
            "#currentonline",     # 2
            "#online",            # 3
            "#pm",                # 4
            "#contacts",          # 5
            "#developer",         # 6
            "#help",              # 7
            "#commands",          # 8
            "#Connect"            # 9
                    ]

        if _command == str(commands[0]):
            _x = urllib.urlopen("http://myexternalip.com/raw") # get raw ip
            _y = _x.read()
            self.cs_msgFeeds.text = "You are "+str(username)+" and Saved in PinoyChat with the IP: " + str(_y)


    def send_message(self, *args):
        """
        Send Line to Echo Server / coreEngine Server @ PinoyChat tunnel
        """
        commands = ["#whoami","#changename","#currentonline","#online","#pm","#contacts","#developer","#help","#commands","#Connect"]
        global username, initsend,ShowAds
        global AdminBool,ModBool,DisableMessageBool

        self.cs_msgBox.focus = False

        if initsend == 0: # run on init send
            INITSEND = "IM LOGGED IN NOW!  version: "+str(__version__)
            if self.connection:
                self.send("<"+str(username)+"> "+str(INITSEND))
                self.cs_msgBox.text = ""
                self.cs_msgFeeds.scroll_y = 0

                try:
                    self.send("list")   # fixed in 2.0.0 (users list always delay bug)
                except:
                    pass

            # dbug

            initsend = 1

        #self.cs_Send.on_press = self.ShowEmoticonsMenu # bug caught here < 2.0.1

        msg = self.cs_msgBox.text

        # db: 2.0.1
        self.cs_Send.font_name = "Data/Resource/ico/icon2.ttf"
        self.cs_Send.text = u"\uf054"
        self.cs_Send.font_size = 20
        self.cs_Send.background_normal = "Data/Colours/8.jpg"
        self.cs_Send.background_down = "Data/IOData/send.png"
        # db: 2.0.1

        self.cs_Send.size_hint = .17,.1
        self.cs_Send.pos_hint = {'center_x': 0.93, 'center_y': 0.05}
        #self.cs_Send.text = "...."

        def PChatParserEngine(NCOLOR,MCOLOR,Bold,Italic):
            """
            Function for Processing Markup Syntax and inlines
            """
            global username, DisableMessageBool
            global Feed_Bold, Feed_Italic, Feed_Normal
            global Feed_UColor, Feed_MColor, Feed_Font, AdminBool, ModBool

            MessageString = self.cs_msgBox.text

            NameCOLR = Feed_UColor
            Name = username
            MessageCOLR = Feed_MColor

            ItalicObj = ""
            ItalicObjEnd = ""
            BoldObj = ""
            BoldObjEnd = ""

            if Feed_Bold:
                BoldObj = "[b]"
                BoldObjEnd = "[/b]"

            else:
                BoldObj = ""
                BoldObjEnd = ""

            if Feed_Italic:
                ItalicObj = "[i]"
                ItalicObjEnd = "[/i]"

            else:
                ItalicObj = ""
                ItalicObjEnd = ""

            Back = """
            MARK[sup][MODERATOR]:[/sup][color=#00FFFF]ASDASDASDASD[/color]
            [ref=world]World[/ref]"""

            if AdminBool:
                mString = "[color={nameColor}][ref={CUsername}]{CUsername}[sup][color=#FF0000][ADMIN][/color][/sup]:[/ref][/color] [color={messageColor}]{italicStart}{boldStart}{StrMessage}{boldEnd}{italicEnd}[/color]".format(
                                                 nameColor=NameCOLR,
                                                 CUsername=Name,
                                                 messageColor=MessageCOLR,
                                                 StrMessage = MessageString,
                                                 boldStart= BoldObj,
                                                 boldEnd = BoldObjEnd,
                                                 italicStart = ItalicObj,
                                                 italicEnd = ItalicObjEnd
                )

            elif ModBool:
                mString = "[color={nameColor}][ref={CUsername}]{CUsername}[sup][color=#FF3300][MOD][/color][/sup]:[/ref][/color] [color={messageColor}]{italicStart}{boldStart}{StrMessage}{boldEnd}{italicEnd}[/color]".format(
                                                 nameColor=NameCOLR,
                                                 CUsername=Name,
                                                 messageColor=MessageCOLR,
                                                 StrMessage = MessageString,
                                                 boldStart= BoldObj,
                                                 boldEnd = BoldObjEnd,
                                                 italicStart = ItalicObj,
                                                 italicEnd = ItalicObjEnd
                )

            else:
                mString = "[color={nameColor}][ref={CUsername}]{CUsername}:[/ref][/color] [color={messageColor}]{italicStart}{boldStart}{StrMessage}{boldEnd}{italicEnd}[/color]".format(
                                                 nameColor=NameCOLR,
                                                 CUsername=Name,
                                                 messageColor=MessageCOLR,
                                                 StrMessage = MessageString,
                                                 boldStart= BoldObj,
                                                 boldEnd = BoldObjEnd,
                                                 italicStart = ItalicObj,
                                                 italicEnd = ItalicObjEnd
                )


            return mString


        if msg and self.connection: # If connection is "Online" Continue...

            if str(self.cs_msgBox.text[0:1]) == "/":  # Capture Commands, usually starts with /

                # Activation of Administrative usage

                if "/activate" in str(self.cs_msgBox.text):
                    Via = pinoyChatClientApp().AccountValuesLoader(username=username)
                    if int(Via.Admin) == 1:
                        self.cs_msgFeeds.text = "Administrator Power Activated\n"
                        AdminBool = 1
                        try:
                            self.Main.add_widget(self.control)
                        except:
                            pass
                        self.Administration(rank="Administrator")

                    elif int(Via.Moderator) == 1:
                        self.cs_msgFeeds.text = "Moderator Power Activated\n"
                        ModBool = 1
                        try:
                            self.Main.add_widget(self.control)
                        except:
                            pass
                        self.Administration(rank="Moderator")



                #Controllers
                if "/control" in self.cs_msgBox.text:
                    if ModBool: # Moderator
                        self.Administration(rank="Moderator")

                    if AdminBool:   # Administrator
                        self.Administration(rank="Administrator")

                    else:
                        pass    # Normal

                if "/fuck" in self.cs_msgBox.text:
                    self.cs_msgFeeds.text += "YOU\n"

                if "/deactivate" in self.cs_msgBox.text:

                    if AdminBool:
                        AdminBool = 0
                        self.cs_msgFeeds.text += "Administrator Power Deactivated\n"
                        try:
                            self.Main.remove_widget(self.control)
                        except:
                            pass

                    if ModBool:
                        ModBool = 0
                        self.cs_msgFeeds.text += "Moderator Power Deactivated\n"
                        try:
                            self.Main.remove_widget(self.control)
                        except:
                            pass

                if AdminBool:
                    if "/power" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#003366]Power: ADMINISTRATOR[/color]"

                    elif "/whoami" in self.cs_msgBox.text:
                        _x = urllib.urlopen("http://ip.42.pl/raw") # get raw ip
                        _y = _x.read()
                        self.cs_msgFeeds.text += "\nName: "+str(username)+" IP: "+str(_y)+"\n"

                    elif "/developer" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#660000]Developer: "+__author__+"[/color]"+"\n"

                    elif "/clear" in self.cs_msgBox.text:
                        self.EngineConnector.text = ""

                    elif "/info" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#660000]Version: {v}\nUpdated: {ud}\nDeveloper: {csec}\nContact: {con}[/color]\n".format(v=__version__,
                                                                                                                                                 ud=__release__,
                                                                                                                                                 csec="CoreSEC Softwares Canada",
                                                                                                                                                 con="info@coresec.ca")
                    else:
                        if isinstance(self.cs_msgBox.text,unicode):
                            pass
                        else:
                            self.send(self.cs_msgBox.text)

                elif ModBool:

                    if "/power" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#003366]Power: MODERATOR[/color]"

                    elif "/whoami" in self.cs_msgBox.text:
                        _x = urllib.urlopen("http://ip.42.pl/raw") # get raw ip
                        _y = _x.read()
                        self.cs_msgFeeds.text += "\nName: "+str(username)+" IP: "+str(_y)+"\n"

                    elif "/clear" in self.cs_msgBox.text:
                        self.EngineConnector.text = ""

                    elif "/developer" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#660000]Developer: "+__author__+"[/color]"+"\n"

                    elif "/info" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#660000]Version: {v}\nUpdated: {ud}\nDeveloper: {csec}\nContact: {con}[/color]\n".format(v=__version__,
                                                                                                                                                 ud=__release__,
                                                                                                                                                 csec="CoreSEC Softwares Canada",
                                                                                                                                                 con="info@coresec.ca")

                    else:
                        if isinstance(self.cs_msgBox.text,unicode):
                            pass
                        else:
                            self.send(self.cs_msgBox.text)

                else:

                    if "/developer" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#660000]Developer: "+__author__+"[/color]"+"\n"

                    elif "/whoami" in self.cs_msgBox.text:
                        _x = urllib.urlopen("http://ip.42.pl/raw") # get raw ip
                        _y = _x.read()
                        self.cs_msgFeeds.text += "\nName: "+str(username)+" IP: "+str(_y)+"\n"

                    elif "/info" in self.cs_msgBox.text:
                        self.cs_msgFeeds.text += "[color=#660000]Version: {v}\nUpdated: {ud}\nDeveloper: {csec}\nContact: {con}[/color]\n".format(v=__version__,
                                                                                                                                                 ud=__release__,
                                                                                                                                                 csec="CoreSEC Softwares Canada",
                                                                                                                                                 con="info@coresec.ca")

                    elif "/clear" in self.cs_msgBox.text:
                        self.EngineConnector.text = ""

                    elif "/default" in self.cs_msgBox.text:
                        self.DefaultSettingsLoaderObj()


                self.cs_msgBox.text = ""

                return False

            if len(self.cs_msgBox.text) <= 1 or str(self.cs_msgBox.text).isspace():
                return False

            if DisableMessageBool:
                return False

            else:
                self.send(str(PChatParserEngine(NCOLOR="#FFFFFF",MCOLOR="#424242",Bold=1,Italic=1)))    # Parameters are useless/no effects
                #self.cs_msgBox.text = ""
                #self.cs_msgFeeds.text= PChatParserEngine(NCOLOR="#FFFFFF",MCOLOR="#424242",Bold=1,Italic=1)

                self.cs_msgBox.text = ""  # Clear the MessageBox

        if DEVELOPMENT_MODE == False and ShowAds != 0:
            self.show_pchat_ad()
            ShowAds -= 1

        if not self.connection:
            username = ""   # username + msg has entered bug fix.
            self.back_to_intro()



    def print_message(self, msg):
        """
        Works if coreEngine send through (transport) rather than (line)
        """
        global print_message_init

        if "PCHATLIST" in msg:
            print_message_init = msg
            return False
        else:

            self.cs_msgFeeds.text += str(msg).replace("<","").replace(">",": ") + "\n"
            self.play_chat()

    def SanitizeMessage(self,fullMess):
        """
        Clean All Dirty words...
        """
        from Engine import Filters
        dirts = Filters.Words

        mess = str(fullMess)
        sp = mess.splitlines()

        Cleaned = None
        for dirt,cencored in dirts.items():
            if dirt in mess:
                Cleaned = mess.replace(dirt,dirts[dirt])


        return Cleaned # could be NONE <~ bug and fixed.

    def Administration(self,rank):
        """
        Admin Command Controller
        """
        global username
        global print_message_init
        global AdminBool, ModBool

        Title = str(rank)
        FL = FloatLayout()


        BG = Button()
        BG.background_normal = "Data/Colours/6.jpg"
        BG.background_down = BG.background_normal
        BG.pos_hint = PinoyChat_XY(.5,.5)

        def Chose(instance,value):
            Chosen = value

            if Chosen == 'Kick User':
                CmdInfo.text = """
[b]Kick User[/b]
Kick the User out of the Room!"""

            if Chosen == 'Disconnect User':
                CmdInfo.text = """
[b]Disconnect User[/b]
This will make the User's PinoyChat App Crash!"""

            if Chosen == 'Mute User':
                CmdInfo.text = """
[b]Mute User[/b]
This will make the User unable to send a Message!"""

            if Chosen == 'Disable Mute':
                CmdInfo.text = """
[b]Disable Mute[/b]
This is useful if you have Muted the User!"""

            if Chosen == 'Warning User':
                CmdInfo.text = """
[b]Warning User[/b]
This will make a Prompt to the user with the message 'Inapproriate Message'!"""

            if Chosen == 'Get IP Address':
                CmdInfo.text = """
[b]Track User[/b]
This is useful if you have a Record and want to know His/Her IP Address!"""


        SpinAction = Spinner(
            text="Choose Action",
            values=('Kick User', 'Disconnect User', 'Mute User','Disable Mute','Warning User','Get IP'),
            size_hint=(.8, None),
            size=(300, 44),
            pos_hint={'center_x': .5, 'center_y': .9})


        CmdInfo = CMDHelp()
        CmdInfo.size_hint = .95,.24
        CmdInfo.text = ""
        CmdInfo.pos_hint = PinoyChat_XY(.5,.14)
        CmdInfo.font_size = "16sp"

        def Refresh():
            try:
                self.send("list")
            except:
                pass
            OList = []

            splitme = print_message_init.split("_")
            NoSym = []

            for x in splitme:
                NoSym.append(str(x).replace("\[Online\]",""))

            for Users in NoSym:
                if str(Users).isspace():
                    pass
                else:
                    OList.append(str(Users).replace("`","",2).strip())

            UserAction.values = OList

            CmdInfo.text = "Users List has been Reloaded\n{count} users is now in list".format(count=len(UserAction.values))

        UserAction = Spinner(
            text="Who?",
            size_hint=(.8, None),
            size=(300, 44),
            pos_hint={'center_x': .5, 'center_y': .78})
        Refresh()



        SpinAction.background_normal = "Data/Colours/8.jpg"
        SpinAction.background_down = SpinAction.background_normal
        SpinAction.bind(text=Chose)

        def Execute():
            """
            Command Executioner
            """

            Action = SpinAction.text
            Who = UserAction.text
            Def = ['Choose Action','Who?']
            defact = ['Kick User', 'Disconnect User', 'Mute User','Disable Mute','Warning User','Get IP']

            Struct = {
                      'Kick User':'kick',
                      'Disconnect User':'disconnect',
                      'Mute User':'disable',
                      'Disable Mute':'enable',
                      'Warning User':'warning',
                      'Get IP':'expose'
                    }
            if Action == Def[0] or Who == Def[1]:
                CmdInfo.text = "Please Select an Action and User"

            else: #Run now
                cmd = "/"

                if len(Who) < 3:
                    return False

                FullCom = cmd+str(Struct[Action])+" "+str(Who)
                try:
                    try:
                        self.send(str(FullCom))
                    finally:
                        CmdInfo.text = "[b]Success![/b]\n"+str(Action)+" to "+str(Who)
                except:
                    CmdInfo.text = "Could'nt Send the Commands! :("



        Go = Button(text="Run Command")
        Go.background_normal = "Data/Colours/16.jpg"
        Go.size_hint = .8,.1
        Go.pos_hint = PinoyChat_XY(.5,.4)
        Go.on_press = Execute

        Ref = Button(text="Reload Users List")
        Ref.background_normal = "Data/Colours/8.jpg"
        Ref.size_hint = .8,.1
        Ref.pos_hint = PinoyChat_XY(.5,.65)
        Ref.on_press = Refresh

        FL.add_widget(BG)
        FL.add_widget(CmdInfo)
        FL.add_widget(SpinAction)
        FL.add_widget(UserAction)
        FL.add_widget(Go)
        FL.add_widget(Ref)

        _m = Popup(title="Staff Control ("+Title+")",auto_dismiss=True)
        _m.add_widget(FL)
        _m.size_hint = .85,.8
        _m.pos_hint = PinoyChat_XY(.5,.55)
        _m.open()



    def HistoryLoader(self):
        """
        Histories loader fN,
        - Fixes/hack for the GPU exceeds.
        """
        global Room
        global DataServer
        RetRoom = str(Room)

        RType = None
        RRoom = str(Room)+".msg"
        TPattern = RetRoom[-5:]

        if "Other" == TPattern:
            RType = "Others/"

        else:
            RType = "Main/"

        URL = str(DataServer)+str(RType)+str(RRoom)

        try:
            if RType:
                _openner = urllib.FancyURLopener({})
                RemRead = _openner.open(URL).read()
                return Truncate.Truncate(RemRead,cut_max).Get()


                #return str(RemRead)

        except:
            return "Loading Logs..."  # this is Empty.

    def handle_message(self, msg):
        """
        Recieved Messages Flows inside...
        """
        from Engine import Filters
        import time

        global CurrentChat
        global print_message_init
        global username
        global users_init
        global Users_Storage
        global BoolUserOption, AdminBool,ModBool,DisableMessageBool
        global cacheEngine, Message
        global MessageStorage
        import ast

        if "\[Online\]" in msg:
            print_message_init = msg
            return False
        strMsg = str(msg)
        if strMsg[0:1] == "/":

            if AdminBool or ModBool:
                self.cs_msgFeeds.text += "[color=#FF9966]Command Success.[/color]\n"


            if "/kick" in strMsg: # Kick Packet
                _name = strMsg.replace("/kick ","")

                if _name == username:
                    self.pinoyChat_exception(errmsg="You are Disconnected.",title="Connection Lost")
                    AdminBool, ModBool = False,False
                    self.send(str(username)+ " has been Kicked.")
                    self.back_to_intro()

            elif "/warning" in strMsg: # Warning
                _name = strMsg.replace("/warning ","")

                if _name == username:
                    self.pinoyChat_exception(errmsg="Inappropriate Message\n",title="Warning!")
                    self.send("[color=#FF9966][WARNING to {x}][/color] Inapproriate Message".format(x=username))

            elif "/disconnect" in strMsg: # Crash / DC
                _name = strMsg.replace("/disconnect ","")

                if _name == username:
                    try:
                        self.send("[color=#FF9966][DISCONNECTED {s}][/color]".format(s=username))

                    finally:
                        self.stop()


            elif "/disable" in strMsg: # Crash / DC
                _name = strMsg.replace("/disable ","")

                if _name == username:
                    DisableMessageBool = True
                    self.cs_msgBox.text = ""

            elif "/enable" in strMsg: # back to active
                _name = strMsg.replace("/enable ","")
                if _name == username:
                    DisableMessageBool = False


            elif "/expose" in strMsg: # back to active
                _name = strMsg.replace("/expose ","")
                if _name == username:

                    try:
                        ExIP = urllib.urlopen("http://myexternalip.com/raw")
                        Clean = ExIP.read()
                        formx = "Username: {uname} \nIP: {ip}\n".format(uname=username,
                                                                        ip=Clean)
                        self.send(str(formx))

                    except:
                        pass

            return False


        # If message is a PMPacket (unpack)
        if strMsg[0:3] == "#pm":
            StrM = str(msg).replace("#pm","")
            try:
                DictEval = ast.literal_eval(StrM)
            except:
                return False

            Message = str(DictEval["message"]).replace("[","").replace("]","").replace("'","") # message
            SenderUser = str(DictEval["from"]).replace("[","").replace("]","").replace("'","") # Sender
            RecieverUser = str(DictEval["to"]).replace("[","").replace("]","").replace("'","") # Sender

            sanitized = "[b][color=#505050]"+str(SenderUser)+":[/color][/b] [color=#606060]"+str(Message)+"[/color]"    #Main Message for PM
            forMain_sanitized = "[ref="+str(SenderUser)+"][color=#FF0099][from "+str(SenderUser)+"]:[/color][/b] [color=#333366]"+str(Message)+"[/color][/ref]"
            forMain_sanitized_same = "[color=#FF0099][to "+str(RecieverUser)+"]:[/color][/b] [color=#333366]"+str(Message)+"[/color][/ref]"


            if RecieverUser == username or SenderUser == username:

                self.play_PMChat()

                if BoolUserOption == "Opened": # if the modal is opened (active with someone)

                    if CurrentChat == SenderUser or CurrentChat == RecieverUser:
                        self.cs_msgPMFeeds.ids.PMOBJ.text += sanitized +"\n"  # Disable inserting if Opened.
                        self.cs_msgPMFeeds.do_scroll_y = True
                        self.cs_msgPMFeeds.scroll_y = 0
                    else:
                        pass

                    senderMsg = sanitized+"\n"

                    try:
                        MessageStorage[SenderUser]
                        MessageStorage[str(SenderUser)] += str(sanitized) +"\n"
                        self.cs_msgPMFeeds.do_scroll_y = True
                        self.cs_msgPMFeeds.scroll_y = 0
                    except:
                        MessageStorage[SenderUser] = senderMsg



                if BoolUserOption == "Closed":  # if the modal is closed.


                    senderMsg = sanitized+"\n"
                    try:
                        MessageStorage[SenderUser]
                        MessageStorage[str(SenderUser)] += str(sanitized) +"\n"
                        self.cs_msgPMFeeds.do_scroll_y = True
                        self.cs_msgPMFeeds.scroll_y = 0
                    except:
                        MessageStorage[SenderUser] = senderMsg

                    self.UserOptions(instance="",value=SenderUser,msg=sanitized+"\n")
                    self.cs_msgPMFeeds.do_scroll_y = True
                    self.cs_msgPMFeeds.scroll_y = 0






            else:
                pass # Not for you

            return False

        if "has entered the room..." in msg or "has left the Room" in msg:
            joined = "has entered the room..."
            left = "has left the Room"

            try:

                try:

                    msgs = ""
                    PCCurr_time = time.strftime("%I:%M")

                    if "has entered the room..." in msg:
                        msgs1 = str(msg).replace("has entered the room...","")
                        cleanjoin = msgs1.strip()
                        msgs = "[color=00FF66]("+PCCurr_time+ ") "+cleanjoin + " joined...[/color]\n"

                    elif "has left the Room" in msg:
                        msgs2 = str(msg).replace("has left the Room","")
                        cleanleft = msgs2.strip()
                        msgs = "[color=FF0000]("+PCCurr_time+ ") "+cleanleft + " left...[/color]\n"

                    EntranceNotif = msgs

                finally:

                    self.IOMessage.text += " "+ EntranceNotif
                    self.IOMessage.do_scroll_y = True
                    self.IOMessage.scroll_y = 0


            except:
                pass

            return False


        else:   # Actual Messages
            count = str(self.cs_msgFeeds.text).splitlines()

            if len(count) >= 40:
                try:
                    self.cs_msgFeeds.text = ""
                finally:
                    #print "Loaded"

                    RemoteHistory = self.HistoryLoader()
                    self.cs_msgFeeds.text = RemoteHistory
                    self.cs_msgFeeds.do_scroll_y = 1
                    self.cs_msgFeeds.scroll_y = 0
                    self.play_chat()

            else:
                
                if "IM LOGGED IN NOW!  version:" in msg:
                    self.cs_msgFeeds.do_scroll_y = 1
                    self.cs_msgFeeds.scroll_y = 0

                else:
                    # Fix black bug
                    #Truncat = Truncate
                    #_cleared = Truncat.Truncate(self.cs_msgFeeds.text,cut_max).Get()
                    # Fix black bug

                        
                    ParseIt = self.PCEmoticonEngine(msg)    # Call the function to Parse everything (only replaces syntax -> Emo) + Sanitize
                    cacheEngine += str(ParseIt) + "\n"
                    self.cs_msgFeeds.text += str(ParseIt) + "\n"
                    self.cs_msgFeeds.do_scroll_y = 1
                    self.cs_msgFeeds.scroll_y = 0
                    self.play_chat()


    def handle_command(self, msg):
        """
        Useless Func, Useful for future use.
        """
        if 'SET' in msg:
            try:
                #self.indicator.text = "SYSTEM:" + msg.split()[2]
                return "200"

            except:
                self.cs_msgFeeds.text += "Malformed message received." + "\n"
                return "PChat Exception: Couldn't Parse the strings!"

    # Function below is use for loading SO Files (only mac)
    #def MessageGetFunc(self,type,room):
    #    """
    #    Get and Load Histories
    #    """
    #
    #    from LoaderEngine import MessageGetFunc # < Cython Compiled

    #    try:
    #        Loader = MessageGetFunc(type,room)
    #        self.cs_msgFeeds.text = ""
    #        self.cs_msgFeeds.text = str(Loader).replace("<","").replace(">",":")
    #    except:
    #        Catch = MessageGetFunc(type,room)
    #        if str(Catch) == "ERROR":
    #            self.pinoyChat_exception(errmsg="Error Loading Histories!",title="Network Error")

    def MessageGetFunc(self,type,room):
        """
        Get and Load Histories
        """
        from Engine.MessageEngine import LabelManager
        global DataServer


        _URL = DataServer
        self.type = str(type)+"/"           # <~~ Type (Main or Others)
        self.room = str(room)+".msg"           # <~~ Str Room Name
        URL = _URL + self.type + self.room

        try:
            _openner = urllib.FancyURLopener({})
            _Openned = _openner.open(URL).read()
            self.cs_msgFeeds.text = ""
            Cached = str(_Openned).replace("<","").replace(">",":")
            SplittedCached = Cached.splitlines()
    
            for Caches in SplittedCached:
                self.cs_msgFeeds.text += self.PCEmoticonEngine(Caches) +"\n"

        # Unknown exception.
        except:
            self.pinoyChat_exception(errmsg="Error Loading Histories!",title="Server Error")

if __name__ == '__main__':
    pinoyChatClientApp().run() # Start PinoyChat Application