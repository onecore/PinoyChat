__author__ = ['Mark Anthony Pequeras']
__language__ = ['Python', 'Kivy', 'C']
__version__ = [5.5]
__distributor__ = ['CoreSEC Software Development']
__license__ = ['PSF', 'MIT']
__website__ = 'http://www.marp.me/'

# PinoyChat Module
# {

class Core:
    """
    Main CoreEngine Class
    """
    # Main Strings Variable{

    mone = "Metro Manila Chat"
    mtwo = "Ilocos Chat"
    mthree = "Laguna Chat"
    mfour = "Quezon Province Chat"
    mfive = "Bicol Chat"
    msix = "Cebu Chat"
    mseven = "Bacolod Chat"
    meight = "Tacloban Chat"
    mnine = "Mindanao Area Chat"
    # Main }

    # Others Strings Variable{
    oone = "Teens Chat"
    otwo = "Barkada Chat"
    othree = "Music Chat"
    ofour = "Gamers Chat"
    ofive = "Nerds Chat"
    osix = "Mig Chat"
    oseven = "Lovers Chat"
    oeight = "Students Chat"
    onine = "Overseas Chat"
    # }


    def __init__(self): #from 0 to End
        self.ports = [4321, 4322,
                      4323, 4324,
                      4325, 4326, 4327,
                      4328, 4329, 4330, 4331,
                      4332, 4333, 4334, 4335, 4336,
                      4337, 4338, 4339, 4340, 4341, 4342,
                      4343, 4344, 4345, 4346, 4347, 4348, 4349,
                      4350, 4351, 4352, 4353, 4354, 4355, 4355, 4356
                 ] # Integers list for the protocol.

    def join_main(self,room):
        """
        join in the "Main" rooms
        """
        _priv_room = str(room)
        if _priv_room == "Manila":
            return "Manila"

    def join_others(self,room):
        """
        join in the "Others" rooms
        """
        _priv_room = str(room)









# }   