__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'

import Functions

__host__ = str(Functions.Core.PChatParserNext(True))
__port__ = 9999

import itertools
import mimetools
import mimetypes
from cStringIO import StringIO
import urllib
import urllib2


class MultiPartForm(object):
    """
    Accumulate the data to be used when posting a form.
    Copyright: PyOMTW Python News for this awesome Class.
    """

    def __init__(self):
        self.form_fields = []
        self.files = []
        self.boundary = mimetools.choose_boundary()
        return

    def get_content_type(self):
        return 'multipart/form-data; boundary=%s' % self.boundary

    def add_field(self, name, value):
        """Add a simple field to the form data."""
        self.form_fields.append((name, value))
        return

    def add_file(self, fieldname, filename, fileHandle, mimetype=None):
        """Add a file to be uploaded."""
        body = fileHandle.read()
        if mimetype is None:
            mimetype = mimetypes.guess_type(filename)[0] or 'application/octet-stream'
        self.files.append((fieldname, filename, mimetype, body))
        return

    def __str__(self):
        """Return a string representing the form data, including attached files."""
        # Build a list of lists, each containing "lines" of the
        # request.  Each part is separated by a boundary string.
        # Once the list is built, return a string where each
        # line is separated by '\r\n'.
        parts = []
        part_boundary = '--' + self.boundary

        # Add the form fields
        parts.extend(
            [ part_boundary,
              'Content-Disposition: form-data; name="%s"' % name,
              '',
              value,
            ]
            for name, value in self.form_fields
            )

        # Add the files to upload
        parts.extend(
            [ part_boundary,
              'Content-Disposition: file; name="%s"; filename="%s"' % \
                 (field_name, filename),
              'Content-Type: %s' % content_type,
              '',
              body,
            ]
            for field_name, filename, content_type, body in self.files
            )

        # Flatten the list and add closing boundary marker,
        # then return CR+LF separated data
        flattened = list(itertools.chain(*parts))
        flattened.append('--' + self.boundary + '--')
        flattened.append('')
        return '\r\n'.join(flattened)



class Core(object):

    def __init__(self,registration=False,userlogin=False, guest=False):
        self.registration = registration
        self.userlogin = userlogin
        self.guest = guest


    def GetUsersInformation(self,u):
        """
        Method for getting informations from database
        """
        if u:
            import socket

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                host =  __host__

                port = __port__ # PinoyChat Server uses this port as default for database and file function

                s.connect((host, port))

                Packet = "I,"+str(u)

                s.send(Packet) # I Pattern for Information

                Response = s.recv(1024)  # bytes from server

                s.close()

                return Response


            finally:
                pass
    def UserRegistration(self,u,p,a,g):
        """
        This makes a tunnel for server and creates a Secure connection with some security functions (Back-End)
        """

        if self.registration and not self.userlogin:
            Username, Password, Avatar, Gender = str(u), str(p), str(a), str(g)

            import socket

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                host =  __host__

                port = __port__ # PinoyChat Server uses this port as default for database and file function

                s.connect((host, port))

                Packet = "R,{U},{P},{Avat},{Gen}".format(U=Username, P=Password, Avat=Avatar,Gen=Gender)

                s.send(Packet) # G Pattern for Guest Login

                Response = s.recv(1024)  # bytes from server

                s.close()

                return Response


            finally:
                pass


    def UploadAndUpdate(self,u,p,binaryp):
        """
        Function for transfering custom avatars to coreEngine (PinoyChat Engine)
        """

        import requests

        parsed = {

            "upload":binaryp
        }

        dparsed = {
            "pchatid" : str(u),
            "pchatpwd" : str(p)

        }
        request = requests.post("http://pchattunnel.coresec.ca:5000/upload", data=dparsed,files=parsed)
        #print request.text for debug

    def UpdateUser(self, usr,what,val):
        """
        For editing user (send via "u" packet)
        """
        pack = "U,"

        import Functions

        databaseStringsRows = ["Username",
                               "Password",
                               "Avatar",
                               "Gender",
                               "Age",
                               "Location",
                               "AboutMe",
                               "Status",
                               "Admin",
                               "Moderator"]


        if usr and what:
            # make sure to pass 7 as 0 string. ang 3 primes as values.
            import socket

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                host =  __host__

                port = __port__ # PinoyChat Server uses this port as default for database and file function

                s.connect((host, port))

                s.send(pack +usr+","+ what+","+val) # L Pattern for Log-in


                Response = s.recv(1024)

                s.close()

                return Response


            finally:
                pass

    def UserProcessCred(self,u,p):
        """
        Creates Connection with the Server and Map database with the Info.
        - PinoyChat uses socket as seperate library for communicating to Server (Twisted are used only for Messaging etc.)
        """
        import Functions
        NonePattern = "0"
        TruePattern = "1"
        FalsePattern = "0"

        databaseStringsRows = ["Username",
                               "Password",
                               "Avatar",
                               "Gender",
                               "Age",
                               "Location",
                               "AboutMe",
                               "Status",
                               "Admin",
                               "Moderator"]


        if self.userlogin and not self.registration:
            # make sure to pass 7 as 0 string. ang 3 primes as values.
            Username, Password = str(u), str(p)
            import socket

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                host =  __host__

                port = __port__ # PinoyChat Server uses this port as default for database and file function

                s.connect((host, port))

                s.send("L,"+Username+","+Password) # L Pattern for Log-in

                Response = s.recv(1024)

                s.close()

                return Response


            finally:
                pass
    def UserProcessGuest(self,n):
        """
        Function Processor for Guest User
        """
        if self.guest:
            nickname = str(n)
            import socket

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                host = __host__

                port = __port__ # PinoyChat Server uses this port as default for database and file function

                s.connect((host, port))

                s.send("G, "+nickname) # G Pattern for Guest Login

                Response = s.recv(1024)

                s.close()

                return Response


            finally:
                pass