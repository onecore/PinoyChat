__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'


# Engine Folder contains functions that helps the main app functionality.
# It's a Ferrari Engine brand, yet not that noise.