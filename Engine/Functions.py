__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'

# Functions below are all Statics.

class Core(object):

    @staticmethod
    def PChatGPUMemoryCheck():
        """
        Checks Graphics Memory Usage,
        - PinoyChat Messages (sometimes 40+ lines creates a black message, which is a bug.)
        """
        # PinoyChat GPUMemory Checker, This is a very big problem so have to monitor its usage.
        pass

    @staticmethod
    def PChatParser(bool): #/ DataServer / CoreEngine
        import base64
        if bool:
            parse = "http://127.0.0.1:8000/" #base64.b64decode('cGN0dW5uZWwuY29yZXNlYy5jYQ==')
            return parse

    @staticmethod
    def PChatParserNext(bool):
        import base64
        if bool:
            parse = '127.0.0.1' #base64.b64decode('cGN0dW5uZWwuY29yZXNlYy5jYQ==') #('MC4wLjAuMA==')
            return parse

    @staticmethod
    def PChatMarkPequeras(bool):
        import base64
        if bool:
            parse = base64.b64decode("TWFyaw==")
            return parse

    @staticmethod
    def PChatDBEngine(forLogin=False,forRegister=False,query=None):
        pass

    @staticmethod
    def PChatCheckpoint():
        pass