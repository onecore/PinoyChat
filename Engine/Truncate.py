class Truncate(object):
    def __init__(self,msg,cut):
        self.msg = msg
        self.cut = cut

    
    def Get(self):
        global lines

        messages = self.msg
        lines = []
        lines_number = 0

        for lin in messages.splitlines():
            lines.append(lin)
        lines_number = len(lines)

        def Cut(msg):
            global lines
            final = str(msg)
            
            to_cut = lines[-10:] # end to index
            clean = ""

            for l in to_cut:
                clean += l +"\n"

            return clean

        

        if lines_number >= self.cut: # Process Starts
            Cut(messages)

        else:
            return messages
        
