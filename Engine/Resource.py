__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'

class PCResources(object):

    def __init__(self,sig):
        self.checkSignature = sig

coreData = "Data/"

imageData = { "IOData" : coreData+"IOData/",
                           "Resource" : coreData+"Resource/",
                           "Tone" : coreData+"Tone/",
                           "fonts" : coreData+"fonts/",
                           "introData" : coreData+"introData/",
                           "messageData" : coreData+"messageData/",
                           "optionsData" : coreData+"optionsData/",
                           "roomsData" : coreData+"roomsData/",
                           "Account" : coreData+"Account/"
                           }
