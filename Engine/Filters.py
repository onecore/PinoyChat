__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__version__ = '2.0.1' # Version of Main
__distributor__ = ['CoreSEC Softwares, CA']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'

# These are the arrays of fowl words, and will be filtered out of the msgs.

Filters = {
    # Parse all keys into capitals.

    "puta":"****",
    "bitch":"****",
    "gago":"g***",
    "demonyo":"dem****",
    "fuck":"****",
    "panget":"pa*g**",
    "pangit":"p*****",
    "shet":"****",
    "shit":"****",
    "kantot":"******",
    "tae":"***",
    "tangin":"******",
    "tang i":"*****",
    "tingil":"******",
    "tamod":"****",
    "sex":"s*x",
    "cum":"***",
    "kantut":"******",
    "susu":"s***",
    "titi":"****",
    "tite":"****",
    "namo":"n***",
    "wechat":"*****"

           }

Words = Filters