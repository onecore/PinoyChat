__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'

from kivy.lang import Builder
from kivy.uix.scrollview import ScrollView
from kivy.properties import StringProperty

Builder.load_string("""
<LabelManager>:

    Label:
        id: PCHATFEEDENGINE
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text1


    Label:
        id: PCHATFEEDENGINE2
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text2


    Label:
        id: PCHATFEEDENGINE3
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text3


    Label:
        id: PCHATFEEDENGINE4
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text4


    Label:
        id: PCHATFEEDENGINE5
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text5


    Label:
        id: PCHATFEEDENGINE6
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text6


    Label:
        id: PCHATFEEDENGINE7
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text7


    Label:
        id: PCHATFEEDENGINE8
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text8


    Label:
        id: PCHATFEEDENGINE9
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text9

    Label:
        id: PCHATFEEDENGINE10
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text10

""")


class LabelManager(ScrollView):
    """
    Scrollview with Label inside (as the self.cs_msgFeeds connected to)
    """
    text1 = StringProperty('')   # First label, trying to implement remove and add (GPU Memory Exceeds Debug)
    text2 = StringProperty('')
    text3 = StringProperty('')
    text4 = StringProperty('')
    text5 = StringProperty('')
    text6 = StringProperty('')
    text7 = StringProperty('')
    text8 = StringProperty('')
    text9 = StringProperty('')
    text10 = StringProperty('')